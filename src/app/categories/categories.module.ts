import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import {CategoryListComponent} from './category-list/category-list.component';
import {AppWidgetsModule} from '../common/app-widgets/app-widgets.module';
import {ProductDetailComponent} from '../product/product-detail/product-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProductService} from '../product/product-detail/product.service';
import { ProductPreviewComponent } from './product-preview/product-preview.component';
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { CategoriesEditorComponent } from './categories-editor/categories-editor.component';
import { CategoriesEditorItemComponent } from './categories-editor/categories-editor-item/categories-editor-item.component';
import {AutosizeInputModule} from 'ngx-input-autosize';

@NgModule({
  imports: [
    AppWidgetsModule,
    CommonModule,
    FormsModule,
    AutosizeInputModule,
    NgbTooltipModule,
    ReactiveFormsModule,
    CategoriesRoutingModule
  ],
  declarations: [
    CategoryListComponent,
    ProductDetailComponent,
    ProductPreviewComponent,
    CategoriesEditorComponent,
    CategoriesEditorItemComponent
  ],
  providers: [ProductService]
})
export class CategoriesModule { }
