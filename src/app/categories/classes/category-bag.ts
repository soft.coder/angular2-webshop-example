import {Category} from './category';
import {CategoryTree} from './category-tree';

export interface CategoryBag {
  category: Category;
  tree: CategoryTree;
}
