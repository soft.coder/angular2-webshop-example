import {Category, CategoryList} from './category';


describe('CategoryList', () => {

  let category1: Category, category2: Category, category3: Category;
  let sortedCategories: Category[];
  beforeEach(() => {
    category1 = new Category('category1', 1);
    category2 = new Category('category2', 2, category1);
    category3 = new Category('category3', 3, category2);
    sortedCategories = [category1, category2, category3];
  });
  it('should be created', () => {
    let categoryList: CategoryList;
    categoryList = new CategoryList();
    expect(categoryList).toBeDefined();
    categoryList = new CategoryList(sortedCategories);
    expect(categoryList).toBeDefined();

  });

  describe('methods and accessors', () => {
    function compareOrder(categoryList: CategoryList, rightOrdered: Category[]) {
      function compareRecursive(lastCategory: Category, index = rightOrdered.length - 1) {
        expect(lastCategory).toBe(rightOrdered[index]);
        if (index - 1 >= 0) {
          if (lastCategory.prev === undefined) {
            fail('must have previous element');
          }
          compareRecursive(lastCategory.prev, index - 1);
        }
      }
      expect(categoryList.length).toBe(rightOrdered.length);
      compareRecursive(categoryList.last);
    }
    let categoryListEmpty: CategoryList;
    let categoryListFilled: CategoryList;
    beforeEach(() => {
      categoryListEmpty = new CategoryList();
      categoryListFilled = new CategoryList(sortedCategories);
    });
    it('should return length', () => {
      expect(categoryListEmpty.length).toBe(0);
      expect(categoryListFilled.length).toBe(sortedCategories.length);
    });
    it('should return last element', () => {
      expect(categoryListEmpty.last).toBeUndefined();
      expect(categoryListFilled.last).toBe(category3);
    });
    it('should get element by index', () => {
      function expectedError(categoryList: CategoryList, errorIndexes) {
        for (const index of errorIndexes) {
          try {
            categoryList.get(index);
            fail('expected exception');
          } catch (error) {
            expect(error instanceof RangeError).toBeTruthy();
          }
        }
      }
      expect(categoryListFilled.get(0)).toBe(category1);
      expect(categoryListFilled.get(1)).toBe(category2);
      expect(categoryListFilled.get(2)).toBe(category3);
      expectedError(categoryListFilled, [-1, 3, 5]);
      expectedError(categoryListEmpty, [0, 1, -1]);
    });
    it('should iterate through for ... of', () => { // IE >= 9
      let index = 0;
      for (const category of categoryListFilled) {
        expect(category).toBe(sortedCategories[index++]);
      }
    });
    it('should iterate through for', () => {
      for (let i = 0; i < categoryListFilled.length; i++) {
        const category = categoryListFilled.get(i);
        expect(category).toBe(sortedCategories[i]);
      }
    });
    it('should findById', () => {
      expect(categoryListEmpty.findById(2)).toBeUndefined();
      const foundedCategory = categoryListFilled.findById(category2.id);
      expect(foundedCategory.id).toBe(category2.id);
    });
    it('should push', () => {
      const singleCategory = new Category('singleCategory', 1);
      categoryListEmpty.push(singleCategory);
      expect(categoryListEmpty.length).toBe(1);
      expect(categoryListEmpty.last.id).toBe(singleCategory.id);
      const category4 = new Category('category4', 4, category3);
      categoryListFilled.push(category4);
      compareOrder(categoryListFilled, [category1, category2, category3, category4]);
      const categoryInsert = new Category('categoryInsert', 5, category2);
      categoryListFilled.push(categoryInsert);
      compareOrder(categoryListFilled, [category1, category2, categoryInsert, category3, category4]);
    });
    describe('remove', () => {
      let initialLength: number;
      beforeEach(() => initialLength = categoryListFilled.length);
      it('should not remove', () => {
        expect(categoryListEmpty.remove(category1.id)).toBeFalsy();
        expect(categoryListEmpty.length).toBe(0);
        expect(categoryListFilled.remove(1024)).toBeFalsy();
        expect(categoryListFilled.length).toBe(initialLength);
      });
      it('should remove at start', () => {
        expect(categoryListFilled.remove(category1.id)).toBeTruthy();
        expect(categoryListFilled.length).toBe(initialLength - 1);
        compareOrder(categoryListFilled, [category2, category3]);
        expect(category1.prev).toBeUndefined();
      });
      it('should remove at end', () => {
        expect(categoryListFilled.remove(category3.id)).toBeTruthy();
        expect(categoryListFilled.length).toBe(initialLength - 1);
        compareOrder(categoryListFilled, [category1, category2]);
        expect(category3.prev).toBeUndefined();
      });
      it('should remove in the middle', () => {
        expect(categoryListFilled.remove(category2.id)).toBeTruthy();
        expect(categoryListFilled.length).toBe(initialLength - 1);
        compareOrder(categoryListFilled, [category1, category3]);
        expect(category2.prev).toBeUndefined();
      });
    });
  });
});

