import {Category} from './category';


describe('Category', () => {

  it('should be created', () => {
    const category = new Category('test-category');
    expect(category).toBeDefined();
  });
  describe('sort by order id', () => {
    function compareOrder(sorted: Category[], right: Category[]) {
      for (let i = 0; i < sorted.length; i++) {
        if (sorted[i].id !== right[i].id) {
          return false;
        }
      }
      return true;
    }
    const category1 = new Category('category1', 1);
    const category2 = new Category('category2', 2, category1);
    const category3 = new Category('category3', 3, category2);
    const rightOrder = [category1, category2, category3];

    it('should handle empty', () => {
      expect(Category.sortByOrderId().length).toBe(0);
    });
    it('should right sort', () => {
      const successOrders = [
        [category3, category2, category1],
        [category2, category3, category1],
        [category1, category3, category2],
        [category1]
      ];
      for (const order of successOrders) {
        const sortedByPrev = Category.sortByOrderId(order);
        expect(compareOrder(sortedByPrev, rightOrder)).toBeTruthy(sortedByPrev);
      }
    });
    it('should throw error', () => {
      const errorOrders = [
        [category2, category3],
        [category1, category3],
        [category3]
      ];
      for (const order of errorOrders) {
        try {
          Category.sortByOrderId(order);
          fail('expected exception');
        } catch (error) {
          expect(error.message).toContain('sortByOrderId');
        }
      }
    });
  });
  it('should return parentId', () => {
    const parentCategory = new Category('parent-category', 1);
    const category = new Category('test-category', 2);
    expect(category.parentId).toBeUndefined();
    category.parent = parentCategory;
    expect(category.parentId).toBe(parentCategory.id);
  });
  it('should convert to CategoryDB', () => {
    const parentCategory = new Category('parent-category', 1);
    const prevCategory = new Category('prev-category', 2);
    const category = new Category('test-category', 3);
    let categoryDb = category.toCategoryDB();
    expect(categoryDb.name).toBe(category.name);
    expect(categoryDb.id).toBe(category.id);
    expect(categoryDb.parentId).toBeUndefined();
    expect(categoryDb.orderId).toBeUndefined();
    category.parent = parentCategory;
    categoryDb = category.toCategoryDB();
    expect(categoryDb.parentId).toBe(parentCategory.id);
    category.prev = prevCategory;
    categoryDb = category.toCategoryDB();
    expect(categoryDb.orderId).toBe(prevCategory.id);
  });
});
