export class CategoryDB {
  /**
   * Category
   * @param name
   * @param id
   * @param parentId
   * @param orderId
   */
  constructor(
    public name: string,
    readonly id?: number,
    public parentId?: number,
    public orderId?: number,
  ) {
  }
}
