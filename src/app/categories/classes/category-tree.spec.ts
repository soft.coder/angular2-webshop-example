import {Category, CategoryList} from './category';
import {CategoryTree} from './category-tree';
import {CategoryDB} from './category-db';
import {randomRange} from '../../utils';


describe('CategoryTree', () => {
  let sortedCategories: Category[];
  beforeEach(() => {
    let category1: Category, category2: Category, category3: Category;
    category1 = new Category('category1', 1);
    category2 = new Category('category2', 2, category1);
    category3 = new Category('category3', 3, category2);
    sortedCategories = [category1, category2, category3];
  });
  it('should create', () => {
    const categoryTreeEmpty = new CategoryTree();
    expect(categoryTreeEmpty).toBeDefined();
    const categoryTreeFilled = new CategoryTree(sortedCategories);
    expect(categoryTreeFilled).toBeDefined();
  });
  describe('methods and accessors', () => {
    let categoryTreeEmpty: CategoryTree, categoryTreeFilled: CategoryTree;
    beforeEach(() => {
      categoryTreeEmpty = new CategoryTree();
      categoryTreeFilled = new CategoryTree(sortedCategories);
    });
    it('should return children', () => {
      expect(categoryTreeEmpty.children).toBeDefined();
      expect(categoryTreeEmpty.children.length).toBe(1); // other category
      expect(categoryTreeFilled.children).toBeDefined();
      expect(categoryTreeFilled.children instanceof CategoryList).toBeTruthy();
      expect(categoryTreeFilled.children.length).toBe(sortedCategories.length + 1);
      const length = sortedCategories.length;
      for (let i = 0; i < length; i++) {
        expect(categoryTreeFilled.children.get(i)).toBe(sortedCategories[i]);
      }
    });
    it('should return other category', () => {
      expect(categoryTreeEmpty.children.last).toBeDefined();
      expect(categoryTreeFilled.children.last).toBeDefined();
      const otherCategories = [categoryTreeEmpty.children.last, categoryTreeFilled.children.last];
      for (const other of otherCategories) {
        expect(other.id).toBe(CategoryTree.OTHER_ID);
        expect(other.name).toBe(CategoryTree.OTHER_CATEGORY_NAME);
      }
    });
    it('should return root category', () => {
      expect(categoryTreeEmpty.root).toBeDefined();
      expect(categoryTreeFilled.root).toBeDefined();
      const rootCategories = [categoryTreeEmpty.root, categoryTreeFilled.root];
      for (const root of rootCategories) {
        expect(root.name).toBe(CategoryTree.ROOT_CATEGORY_NAME);
        expect(root.id).toBe(CategoryTree.ROOT_ID);
        expect(root.prev).toBeUndefined();
        expect(root.parent).toBeUndefined();
      }
    });
    describe('tree methods', () => {
      function checkTreeBranch(categoryTreeLevel: CategoryList, expectedTreeLevel: Object, categoriesDB?: CategoryDB[]) {
        function checkTreeRecursive(treeLevel: CategoryList, expectedLevel: Object) {
          const length = treeLevel.length;
          const expectedNames = Object.keys(expectedLevel);
          // exclude other category: "Product without category"
          const expectedLength = treeLevel.last.id === CategoryTree.OTHER_ID ? expectedNames.length + 1 : expectedNames.length;
          expect(length).toBe(expectedLength);
          for (let i = 0; i < expectedNames.length; i++) {
            const categoryName = expectedNames[i];
            const expectedChildren = expectedLevel[categoryName];
            const category = treeLevel.get(i);
            expect(category.name).toBe(categoryName);
            if (categoriesDB) {
              const categoryDB = categoriesDB.find(c => c.id === category.id);
              expect(categoryDB).toBeDefined();
              expect(category.name).toBe(categoryDB.name);
              expect(category.orderId).toBe(categoryDB.orderId);
              if (categoryDB.parentId !== undefined) {
                expect(category.parentId).toBe(categoryDB.parentId);
              } else {
                expect(category.parentId).toBe(CategoryTree.ROOT_ID);
              }
            }
            if (expectedChildren) {
              checkTreeRecursive(category.children, expectedChildren);
            }
          }
        }
        return checkTreeRecursive(categoryTreeLevel, expectedTreeLevel);
      }
      function checkTree(categoryTree: CategoryTree, expectedTree: Object, categoriesDB?: CategoryDB[]) {
        return checkTreeBranch(categoryTree.children, expectedTree, categoriesDB);
      }
      function shuffle(categoriesDB: CategoryDB[]) {
        categoriesDB = JSON.parse(JSON.stringify(categoriesDB));
        const max = categoriesDB.length - 1;
        function getSwapIndexes(deep = 0) {
          const first = randomRange(max);
          const second = randomRange(max);
          if (first !== second) {
            return {first, second};
          } else {
            return getSwapIndexes(deep + 1);
          }
        }
        const count = randomRange(max);
        for (let i = 0; i < count; i++) {
          const {first, second} = getSwapIndexes();
          const swapItem = categoriesDB[first];
          categoriesDB[first] = categoriesDB[second];
          categoriesDB[second] = swapItem;
        }
        return categoriesDB;
      }

      let singleLevel: CategoryDB[], twoLevel: CategoryDB[], threeLevel: CategoryDB[];
      let correctSingleLevel: Object, correctTwoLevel: Object, correctThreeLevel;
      beforeEach(() => {
        singleLevel = [
          new CategoryDB('category1', 1),
          new CategoryDB('category2', 2, undefined, 1),
          new CategoryDB('category3', 3, undefined, 2),
          new CategoryDB('category4', 4, undefined, 3)
        ];
        correctSingleLevel = {
          category1: undefined,
          category2: undefined,
          category3: undefined,
          category4: undefined
        };
        twoLevel = [
          ...singleLevel,
          new CategoryDB('category3_1', 5, 3),
          new CategoryDB('category3_2', 6, 3, 5),
          new CategoryDB( 'category3_3', 7, 3, 6),
          new CategoryDB('category2_1', 8, 2)
        ];
        correctTwoLevel = Object.assign(
          {},
          correctSingleLevel,
          {
            category3: {
              category3_1: undefined,
              category3_2: undefined,
              category3_3: undefined
            }
          },
          {
            category2: {
              category2_1: undefined
            }
          }
        );
        threeLevel = [
          ...twoLevel,
          new CategoryDB('category3_2_1', 9, 6),
          new CategoryDB('category3_2_2', 10, 6, 9),
          new CategoryDB('category3_1_1', 11, 5)
        ];
        correctThreeLevel = Object.assign(
          {},
          correctTwoLevel,
          {category3: {
              category3_1: {
                category3_1_1: undefined
              },
              category3_2: {
                category3_2_1: undefined,
                category3_2_2: undefined
              },
              category3_3: undefined
            }
          }
        );
      });

      describe('buildTreeOfCategories', () => {
        it('should create empty', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories([]);
          expect(categoryTree.children.length).toBe(1);
        });
        it('should create single category', () => {
          const categoryDB = new CategoryDB('category', 1);
          const categoryTree = CategoryTree.buildTreeOfCategories([categoryDB]);
          expect(categoryTree.children.length).toBe(2);
          const category = categoryTree.children.get(0);
          expect(category.id).toBe(categoryDB.id);
          expect(category.name).toBe(categoryDB.name);
        });
        it('should build single level tree', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories(singleLevel);
          checkTree(categoryTree, correctSingleLevel, singleLevel);
        });
        it('should build two level tree', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories(twoLevel);
          checkTree(categoryTree, correctTwoLevel, twoLevel);
        });
        it('should build three tree', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories(threeLevel);
          checkTree(categoryTree, correctThreeLevel, threeLevel);
        });
        it('should build tree from shuffle categoriesDB', () => {
          const categoriesInfo = [
            {
              categoriesDB: shuffle(singleLevel),
              correct: correctSingleLevel,
            },
            {
              categoriesDB: shuffle(twoLevel),
              correct: correctTwoLevel,
            },
            {
              categoriesDB: shuffle(threeLevel),
              correct: correctThreeLevel,
            }
          ];
          for (const info of categoriesInfo) {
            const categoryTree = CategoryTree.buildTreeOfCategories(info.categoriesDB);
            checkTree(categoryTree, info.correct, info.categoriesDB);
          }
        });
      });
      it('should findCategoryById', () => {
        const categoryTree = CategoryTree.buildTreeOfCategories(threeLevel);
        for (const categoryDB of threeLevel) {
          const category = categoryTree.findCategoryById(categoryDB.id);
          expect(category).toBeDefined();
          expect(category.id).toBe(categoryDB.id);
          expect(category.name).toBe(categoryDB.name);
          expect(category.orderId).toBe(categoryDB.orderId);
          if (categoryDB.parentId) {
            expect(category.parentId).toBe(categoryDB.parentId);
          } else {
            expect(category.parentId).toBe(CategoryTree.ROOT_ID);
          }
        }
        const rootCategory = categoryTree.findCategoryById(CategoryTree.ROOT_ID);
        expect(rootCategory).toBeDefined();
        expect(rootCategory.id).toBe(CategoryTree.ROOT_ID);
        expect(rootCategory.name).toBe(CategoryTree.ROOT_CATEGORY_NAME);
      });
      describe('moveCategory', () => {
        it('should move in singleLevel tree', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories(singleLevel);
          categoryTree.moveCategory(CategoryTree.ROOT_ID, undefined, 4);
          checkTree(categoryTree, {
            category4: undefined,
            category1: undefined,
            category2: undefined,
            category3: undefined
          });
          categoryTree.moveCategory(CategoryTree.ROOT_ID, 4, 3);
          checkTree(categoryTree, {
            category4: undefined,
            category3: undefined,
            category1: undefined,
            category2: undefined
          });
          categoryTree.moveCategory(CategoryTree.ROOT_ID, 4, 1);
          checkTree(categoryTree, {
            category4: undefined,
            category1: undefined,
            category3: undefined,
            category2: undefined
          });
        });
        it('should move in twoLevel tree', () => {
          const categoryTree = CategoryTree.buildTreeOfCategories(twoLevel);
          const category2 = categoryTree.findCategoryByName('category2');
          const category3 = categoryTree.findCategoryByName('category3');
          expect(category3.children.length).toBe(3);
          expect(category2.children.length).toBe(1);
          checkTreeBranch(category3.children, {
            category3_1: undefined,
            category3_2: undefined,
            category3_3: undefined
          });
          checkTreeBranch(category2.children, {
            category2_1: undefined
          });
          categoryTree.moveCategory(CategoryTree.ROOT_ID, undefined, category3.id);
          checkTree(categoryTree, {
            category3: {
              category3_1: undefined,
              category3_2: undefined,
              category3_3: undefined
            },
            category1: undefined,
            category2: {
              category2_1: undefined
            },
            category4: undefined
          });
          categoryTree.moveCategory(CategoryTree.ROOT_ID, 3, category2.id);
          checkTree(categoryTree, {
            category3: {
              category3_1: undefined,
              category3_2: undefined,
              category3_3: undefined
            },
            category2: {
              category2_1: undefined
            },
            category1: undefined,
            category4: undefined
          });
          const category2_1 = categoryTree.findCategoryByName('category2_1');
          categoryTree.moveCategory(CategoryTree.ROOT_ID, category3.id, category2_1.id);
          checkTree(categoryTree, {
            category3: {
              category3_1: undefined,
              category3_2: undefined,
              category3_3: undefined
            },
            category2_1: undefined,
            category2: undefined,
            category1: undefined,
            category4: undefined
          });
          categoryTree.moveCategory(category2_1.id, undefined, category2.id);
          checkTree(categoryTree, {
            category3: {
              category3_1: undefined,
              category3_2: undefined,
              category3_3: undefined
            },
            category2_1: {
              category2: undefined,
            },
            category1: undefined,
            category4: undefined
          });
          const category3_2 = categoryTree.findCategoryByName('category3_2');
          categoryTree.moveCategory(category2_1.id, category2.id, category3_2.id);
          checkTree(categoryTree, {
            category3: {
              category3_1: undefined,
              category3_3: undefined
            },
            category2_1: {
              category2: undefined,
              category3_2: undefined,
            },
            category1: undefined,
            category4: undefined
          });
          categoryTree.moveCategory(category3_2.id, undefined, category3.id);
          checkTree(categoryTree, {
            category2_1: {
              category2: undefined,
              category3_2: {
                category3: {
                  category3_1: undefined,
                  category3_3: undefined
                }
            }
            },
            category1: undefined,
            category4: undefined
          });
        });
      });
    });
  });
});
