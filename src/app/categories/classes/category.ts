import {Product} from '../../product/product-detail/product';
import {CategoryDB} from './category-db';
import {PropBag} from '../../common/classes/prop-bag';

export class Category {
  get orderId() {
    return this.prev && this.prev.id;
  }
  get parentId() {
    return this.parent && this.parent.id;
  }
  /**
   * Category
   * @param name // DB
   * @param id // DB
   * @param prev // Frontend
   * @param parent // Frontend
   * @param children // Frontend
   * @param products // Frontend
   */
  constructor(
    public name: string,
    readonly id?: number,
    public prev ?: Category,
    public parent?: Category,
    public children: CategoryList = new CategoryList(),
    public products: Product[] = []) {
  }
  static sortByOrderId(children: Category[] = []): Category[] {
    const orderedChildren: Category[] = [];
    for (let i = 0; i < children.length; i++) {
      for (let j = 0; j < children.length; j++) {
        if (!orderedChildren.length && children[j].prev === undefined) {
          orderedChildren.push(children[j]);
        } else if (orderedChildren.length && children[j].prev &&
          children[j].prev.id === orderedChildren[orderedChildren.length - 1].id) {
          children[j].prev = orderedChildren[orderedChildren.length - 1];
          orderedChildren.push(children[j]);
        }
      }
    }
    if (orderedChildren.length !== children.length) {
      throw Error(`sortByOrderId length unequal; ordered:${orderedChildren.length}, source:${children.length}`);
    }
    return orderedChildren;
  }
  toCategoryDB() {

    return new CategoryDB(this.name, this.id,
      this.parent && this.parent.id,
      this.prev && this.prev.id);
  }
}

export class CategoryList implements Iterable<Category> {
  private categories: Category[];

  constructor(categories: Category[] = []) {
    if (categories.length) {
      this.categories = Category.sortByOrderId(categories);
    } else {
      this.categories = categories;
    }
  }

  get length() {
    return this.categories.length;
  }

  get last(): Category {
    const lastCategory = this.categories[this.categories.length - 1];
    if (this.categories.every(c => c.prev === undefined || c.prev.id !== lastCategory.id)) {
      return lastCategory;
    } else {
      throw new Error('CategoryList: broken order of internal array');
    }
  }
  get(index: number) {
    if (index < 0 || index >= this.categories.length) {
      throw RangeError(`category with index:${index} not existing in categoryList`);
    } else {
      return this.categories[index];
    }
  }
  findById(id: number) {
    return this.find([{name: 'id', value: id}]);
  }
  findByOrderId(orderId: number) {
    return this.find([{name: 'orderId', value: orderId}]);
  }

  push(category: Category) {
    let index: number;
    const next = this.categories.find((c, i) => {
      if (c.orderId === category.orderId) {
        index = i;
        return true;
      }
    });
    if (next && index !== undefined) {
      next.prev = category;
      this.categories = [...this.categories.slice(0, index), category, ...this.categories.slice(index)];
    } else {
      this.categories.push(category);
    }
  }

  remove(categoryId: number) {
    let index: number;
    const category = this.categories.find((c, i) => {
      if (c.id === categoryId) {
        index = i;
        return true;
      }
    });
    if (category !== undefined && index !== undefined) {
      const nextCategory = this.categories.find(c => c.orderId === category.id);
      if (nextCategory) {
        if (category.orderId !== undefined) {
          nextCategory.prev = this.categories.find(c => c.id === category.orderId);
        } else {
          nextCategory.prev = undefined;
        }
      }
      category.prev = undefined;
      this.categories.splice(index, 1);
      return true;
    } else {
      return false;
    }
  }
  private find(props: PropBag<any>[]) {
    return this.categories.find(c => props.every( p => c[p.name] === p.value));
  }

  [Symbol.iterator](): Iterator<Category> {
    let index = 0;
    return {
      next: () => {
        if (index < this.categories.length) {
          return {
            value: this.categories[index++],
            done: false
          };
        } else {
          return {
            value: undefined,
            done: true
          };
        }
      },
      return: () => {
        if (index < this.categories.length) {
          return {
            value: this.categories[index++],
            done: false
          };
        } else {
          return {
            value: undefined,
            done: true
          };
        }
      }
    };
  }
}

