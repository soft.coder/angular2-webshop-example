import {Category, CategoryList} from './category';
import {PropBag} from '../../common/classes/prop-bag';
import {CategoryDB} from './category-db';



export class CategoryTree {
  static readonly ROOT_ID: number = -1;
  static readonly OTHER_ID: number = -2;
  static readonly OTHER_CATEGORY_NAME = 'Products without categories';
  static readonly ROOT_CATEGORY_NAME = 'All categories';
  private readonly rootCategory: Category;

  get children() {
    return this.rootCategory.children;
  }

  get other() {
    for (const category of this.rootCategory.children) {
      if (category.id === CategoryTree.OTHER_ID) {
        return category;
      }
    }
  }

  get root() {
    return this.rootCategory;
  }

  constructor(categoires: Category[] = []) {
    this.rootCategory = new Category(
      CategoryTree.ROOT_CATEGORY_NAME,
      CategoryTree.ROOT_ID,
      undefined,
      undefined,
      new CategoryList(categoires));
    this.rootCategory.children.push(new Category(
      CategoryTree.OTHER_CATEGORY_NAME,
      CategoryTree.OTHER_ID,
      this.children.last,
    ));
  }

  static buildTreeOfCategories(categoriesDB: CategoryDB[]) {
    function setParent(parent: Category) {
      if (parent.children) {
        for (const child of parent.children) {
          child.parent = parent;
          setParent(child); // recursion
        }
      }
    }
    function setPrev(children: Category[]) {
      if (children) {
        for (const child of children) {
          const childDB = categoriesDB.find(cdb => cdb.id === child.id);
          if (childDB.orderId) {
            child.prev = children.find( c => c.id === childDB.orderId);
          }
        }
      }
      return children;
    }

    function findChild(parentDB: CategoryDB, deep: number = 1) {
      const children: Category[] = [];
      categoriesDB.forEach(categoryDB => {
        if (categoryDB.parentId === parentDB.id) {
          const child = new Category(categoryDB.name, categoryDB.id);
          // child.parent = parent;
          child.children = findChild(categoryDB, deep + 1); // recursion
          children.push(child);
        }
      });
      return new CategoryList(setPrev(children));
    }

    const categories: Category[] = [];
    categoriesDB.forEach(categoryDB => {
      if (!categoryDB.parentId) {
        const category = new Category(categoryDB.name, categoryDB.id);
        category.children = findChild(categoryDB);
        setParent(category);
        categories.push(category);
      }
    });
    const categoryTree =  new CategoryTree(Category.sortByOrderId(setPrev(categories)));
    const count = categoryTree.children.length;
    for (let i = 0; i < count; i++) {
      const category = categoryTree.children.get(i);
      category.parent = categoryTree.rootCategory;
    }
    return categoryTree;
  }

  public findCategoryById(id: number) {
    return this.findCategory([{name: 'id', value: id}]);
  }
  public findCategoryByName(name: string) {
    return this.findCategory([{name: 'name', value: name}]);
  }

  public moveCategory(parentId: number, orderId: number, categoryId: number) {
    const newPlaceParent = this.findCategoryById(parentId);
    const movedCategory = this.findCategoryById(categoryId);
    const oldPlaceParent = this.findCategoryById(movedCategory.parentId);
    oldPlaceParent.children.remove(movedCategory.id);
    if (orderId !== undefined) {
      movedCategory.prev = this.findCategory([{name: 'parentId', value: parentId}, {name: 'id', value: orderId}]);
    } else {
      movedCategory.prev = undefined;
    }
    newPlaceParent.children.push(movedCategory);
    movedCategory.parent = newPlaceParent;
  }

  private findCategory(props: PropBag<any>[]): Category {
    function propMatch(category: Category) {
      return props.every(prop => category[prop.name] === prop.value);
    }
    function findCategory(category: Category, deep: number = 1) { // recursion
      for (const childCategory of category.children) {
        if (propMatch(childCategory)) {
          return childCategory;
        } else {
          const result = findCategory(childCategory, deep + 1);
          if (result) {
            return result;
          }
        }
      }
    }
    if (propMatch(this.rootCategory)) {
      return this.rootCategory;
    } else {
      return findCategory(this.rootCategory);
    }
  }
}
