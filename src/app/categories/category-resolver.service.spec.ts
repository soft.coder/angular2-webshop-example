import { TestBed, inject } from '@angular/core/testing';

import { CategoryResolverService } from './category-resolver.service';
import {CategoryService} from './category.service';
import {Category} from './classes/category';
import {of} from 'rxjs/internal/observable/of';

describe('CategoryResolverService', () => {
  beforeEach(() => {
    const categoryServiceStub: Partial<CategoryService> = {
      get(id: number) {
        return of(new Category('Test category', id));
      }
    };
    TestBed.configureTestingModule({
      providers: [
        CategoryResolverService,
        {provide: CategoryService, useValue: categoryServiceStub}
      ]
    });
  });

  it('should be created', inject([CategoryResolverService], (service: CategoryResolverService) => {
    expect(service).toBeTruthy();
  }));
});
