import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CategoryService} from './category.service';
import {tap} from 'rxjs/internal/operators/tap';
import {CategoryTree} from './classes/category-tree';

@Injectable({
  providedIn: 'root'
})
export class RootCategoriesResolverService implements Resolve<CategoryTree> {

  constructor(private categoryService: CategoryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CategoryTree> {
    return this.categoryService.getAll();
  }
}
