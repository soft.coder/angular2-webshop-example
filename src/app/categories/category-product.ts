export class CategoryProduct {
  constructor(public id: number, public categoryId: number, public productId: number) {}
}
