import { TestBed, inject } from '@angular/core/testing';

import { RootCategoriesResolverService } from './root-categories-resolver.service';
import {CategoryService} from './category.service';
import {CategoryTree} from './classes/category-tree';
import {of} from 'rxjs/internal/observable/of';

describe('RootCategoriesResolverService', () => {
  beforeEach(() => {
    const categoryServiceStub: Partial<CategoryService> = {
      getAll() {
        return of(new CategoryTree([]));
      }
    };
    TestBed.configureTestingModule({
      providers: [
        RootCategoriesResolverService,
        {provide: CategoryService , useValue: categoryServiceStub}
      ]
    });
  });

  it('should be created', inject([RootCategoriesResolverService], (service: RootCategoriesResolverService) => {
    expect(service).toBeTruthy();
  }));
});
