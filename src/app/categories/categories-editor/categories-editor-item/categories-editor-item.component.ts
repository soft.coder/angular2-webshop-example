import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Category} from '../../classes/category';
import {of, Subject, timer} from 'rxjs';
import {catchError, debounce, filter, switchMap} from 'rxjs/operators';
import {CategoryService} from '../../category.service';
import {ComputedStyleService} from '../../../common/app-services/computed-style.service';
import {DragDataService} from '../../../common/app-services/drag-data.service';
import {NgModel} from '@angular/forms';
import {getErrorMsg} from '../../../common/functions';
@Component({
  selector: 'app-categories-editor-item',
  templateUrl: './categories-editor-item.component.html',
  styleUrls: ['./categories-editor-item.component.less']
})
export class CategoriesEditorItemComponent implements OnInit {
  @Input()
  category: Category;
  @ViewChild('item')
  item: ElementRef;
  @ViewChild('categoryName')
  ngModelCategoryName: NgModel;
  private categoryName = new Subject<string>();
  public status = {
    updating: false,
    updated: false,
    'drop-before': false,
    'drop-inner': false,
    'drop-after': false
  };
  isFocus: boolean;
  allowDrop = false;
  public countDragEnter = 0;
  private savedCategoryName;
  readonly categoryNameChangeInterval = 2000;
  readonly categoryNameUpdatedTimeout = 2000;

  constructor(private categoryService: CategoryService, private computedStyle: ComputedStyleService,
              private dragDataService: DragDataService<DragData>) { }

  ngOnInit() {
    this.savedCategoryName = this.category.name;
    this.categoryName.next(this.category.name);
    this.initSubjectPipe();
  }

  changeName(categoryName: string) {
    if (categoryName.trim()) {
      this.categoryName.next(categoryName.trim());
    }
  }

  blur(categoryName: string) {
    if (categoryName.trim()) {
      this.categoryName.next(categoryName.trim());
    }
  }

  dragOver($event: DragEvent) {
    const dragData = this.dragDataService.getData();
    dragData.dropTarget = this.getDropTarget($event);
    if (this.isAvailDrop(dragData)) {
      this.setDropStyles(dragData.dropTarget);
      $event.preventDefault();
    }
  }

  dragLeave() {
    this.countDragEnter--;
    if (this.countDragEnter <= 0) { // prevent leave to descedants nodes
      this.countDragEnter = 0;
      this.resetDropStyles();
    }

  }

  dragEnter($event: DragEvent) {
    const dragData = this.dragDataService.getData();
    dragData.dropTarget = this.getDropTarget($event);
    if (this.isAvailDrop(dragData)) {
      this.setDropStyles(dragData.dropTarget);
      this.countDragEnter++;
      $event.preventDefault();
    }
  }
  dragDrop($event: DragEvent) {
    const dragData = this.dragDataService.getData();
    let parentId: number;
    switch (dragData.dropTarget) {
      case DropTarget.before:
        parentId = this.category.parentId;
        this.categoryService.moveCategory(parentId, dragData.movedCategory.id, this.category.orderId).subscribe();
        break;
      case DropTarget.inner:
        parentId = this.category.id;
        const lastId = this.category.children.length ? this.category.children.last.id : undefined;
        this.categoryService.moveCategory(parentId, dragData.movedCategory.id, lastId).subscribe();
        break;
      case DropTarget.after:
        parentId = this.category.parentId;
        this.categoryService.moveCategory(parentId, dragData.movedCategory.id, this.category.id).subscribe();
        break;
    }
    // this.categoryService.moveCategory(this.category.parentId, dragData.movedCategory.id, this.category.id).subscribe();
    this.resetDropStyles();
    this.countDragEnter = 0;
    $event.preventDefault();
  }

  dragEnd() {
    this.countDragEnter = 0;
    this.resetDropStyles();
  }

  dragStart($event: DragEvent) {
    // $event.preventDefault();
    try {
      $event.dataTransfer.setData('text/plain', this.category.id.toString()); // FF
    } catch (error) {}
    this.dragDataService.setData(new DragData(this.category));
    $event.dataTransfer.dropEffect = 'move';
    $event.dataTransfer.effectAllowed = 'move';
    if ($event.dataTransfer.setDragImage) {
      $event.dataTransfer.setDragImage(this.item.nativeElement, 0, 0);
    }
  }

  dragCanceled() {
    // this.countDragEnter = 0;
    // this.resetDropStyles();
  }
  getCategoryNameErrorMsg() {
    if (this.ngModelCategoryName.errors) {
      return getErrorMsg(this.ngModelCategoryName.errors, {
        'required': 'Required',
        'serverError': () => this.ngModelCategoryName.errors.serverError,
        'unexpectedError': () => this.ngModelCategoryName.errors.unexpectedError
      });
    }
  }

  private isAvailDrop(dragData: DragData): boolean {
    function isChild(category: Category) {
      if (category && category.parentId) {
        if (category.parentId === dragData.movedCategory.id ) {
          return true;
        } else {
          return isChild(category.parent);
        }
      } else {

        return false;
      }
    }
    if (isChild(this.category)) {
      return false;
    }
    switch (dragData.dropTarget) {
      case DropTarget.before:
        return this.category.orderId !== dragData.movedCategory.id;
      case DropTarget.inner:
        return this.category.id !== dragData.movedCategory.id && this.category.id !== dragData.movedCategory.parentId;
      case DropTarget.after:
        if (!this.category.parent) {
          throw new Error('undefined parent');
        }
        const nextCategory = this.category.parent.children.findByOrderId(this.category.id);
        if (nextCategory) {
          return nextCategory.id !== dragData.movedCategory.id;
        } else {
          return true;
        }
    }
  }
  private getDropTarget($event: DragEvent): DropTarget {
    const itemHeight = this.computedStyle.height(this.item);
    const offsetParentY = this.computedStyle.offsetTop((<HTMLElement>$event.target), this.item);
    const offset = offsetParentY + $event.offsetY;
    if (offset < itemHeight / 4) {
      return DropTarget.before;
    } else if (offset >= itemHeight / 4 && offset < itemHeight - itemHeight / 4) {
      return DropTarget.inner;
    } else {
      return DropTarget.after;
    }
  }
  private setDropStyles(dropTarget: DropTarget) {
    this.resetDropStyles();
    this.status[dropTarget] = true;
  }
  private resetDropStyles() {
    this.status['drop-before'] = this.status['drop-inner'] = this.status['drop-after'] = false;
  }
  private initSubjectPipe() {
    this.categoryName.pipe(
      debounce<string>((name: string) => {
        if (this.isFocus) {
          return timer(this.categoryNameChangeInterval).pipe(switchMap(() => of(name)));
        } else {
          return of(name);
        }
      }),
      filter(name => this.savedCategoryName !== name),
      switchMap((categoryName: string) => {
        this.status.updated = false;
        this.status.updating = true;
        this.savedCategoryName = categoryName;
        return this.categoryService.changeName(this.category.id, categoryName).pipe(
          catchError(error => {
            this.ngModelCategoryName.control.setErrors({serverError: error.message});
            this.status.updating = false;
            return of(error);
          })
        );
      }),
      switchMap((result: Category | Error) => {
        this.status.updating = false;
        if (result instanceof Error) {
          return of(undefined);
        } else {
          if (this.ngModelCategoryName.control.hasError('serverError')) {
            this.ngModelCategoryName.control.setErrors(null);
          }
          this.status.updated = true;
          return timer(this.categoryNameUpdatedTimeout);
        }
      })
    ).subscribe(
      () => this.status.updated = false,
      error => {
        this.ngModelCategoryName.control.setErrors({unexpectedError: error.message ? error.message : 'unexpected error, try again'});
        this.categoryName = new Subject<string>();
        this.initSubjectPipe();
      });
    // timer().subscribe(() => this.itemHeight = this.computedStyle.height(this.item));
  }
}

export enum DropTarget {
  before = 'drop-before', inner = 'drop-inner', after = 'drop-after'
}

export class DragData {
  constructor(readonly movedCategory: Category, public dropTarget?: DropTarget) { }
}
