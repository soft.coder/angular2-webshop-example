import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {CategoriesEditorItemComponent, DragData, DropTarget} from './categories-editor-item.component';
import {CategoryService} from '../../category.service';
import {ComputedStyleService} from '../../../common/app-services/computed-style.service';
import {DragDataService} from '../../../common/app-services/drag-data.service';
import {Category, CategoryList} from '../../classes/category';
import {FormsModule} from '@angular/forms';
import {Component, DebugElement, Directive, Input} from '@angular/core';
import {RouterLinkStubDirective} from '../../../common/tests/router/router-link-stub.directive';
import {By} from '@angular/platform-browser';
import {asyncData, asyncError, FieldDataLazy, inputText, TestPage} from '../../../common/tests/common.spec';
import {Observable, timer} from 'rxjs';
import {CategoryDB} from '../../classes/category-db';
import {CategoryTree} from '../../classes/category-tree';
import {CategoriesEditorComponent} from '../categories-editor.component';
import {ActivatedRoute} from '@angular/router';
import {ActivatedRouteStubService} from '../../../common/tests/router/activated-route-stub.service';
import {BreadcrumbsService} from '../../../common/app-services/breadcrumbs.service';
import SpyObj = jasmine.SpyObj;

describe('CategoriesEditorItemComponent', () => {

  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[ngbTooltip]',
    exportAs: 'ngbTooltip'
  })
  class NgbTooltipDirectiveStub {
    // tslint:disable-next-line:no-input-rename
    @Input('ngbTooltip')
    text: string;
    @Input()
    disableTooltip: boolean;
    @Input()
    openDelay: number;
    @Input()
    placement: string;
  }
  describe('standalone', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-zone',
      template: '<app-categories-editor-item [category]="category"></app-categories-editor-item>'
    })
    class TestZoneComponent {
      category?: Category;
    }
    let fixture: ComponentFixture<TestZoneComponent>;
    let componentTestZone: TestZoneComponent;
    let component: CategoriesEditorItemComponent;
    let page: CategoriesEditorItemPage;


    beforeEach(async(() => {
      const categoryServiceSpy = jasmine.createSpyObj('CategoryService', ['changeName']);

      TestBed.configureTestingModule({
        imports: [FormsModule],
        declarations: [TestZoneComponent, CategoriesEditorItemComponent, NgbTooltipDirectiveStub, RouterLinkStubDirective],
        providers: [
          {provide: CategoryService, useValue: categoryServiceSpy},
          {provide: ComputedStyleService, useValue: {}},
          {provide: DragDataService, useValue: {}}
        ]
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(TestZoneComponent);
      componentTestZone = fixture.componentInstance;
      const debugElement = fixture.debugElement.query(By.directive(CategoriesEditorItemComponent));
      component = debugElement.injector.get(CategoriesEditorItemComponent);
      page = new CategoriesEditorItemPage(debugElement);
    });
    describe('init', () => {
      const testCategory = new Category('Test category');
      beforeEach(fakeAsync(() => {
        componentTestZone.category = testCategory;
        fixture.detectChanges();
        tick();
      }));
      it('should create', () => {
        expect(componentTestZone).toBeTruthy();
        expect(component).toBeTruthy();
      });
      it('should binding', () => {
        expect(component.category).toBe(testCategory);
      });
    });
    describe('template binding', () => {
      const testCategory = new Category('Test category', 1);
      beforeEach(fakeAsync(() => {
        componentTestZone.category = testCategory;
        fixture.detectChanges();
        tick();
      }));
      it('should show input with category name', () => {
        expect(page.categoryName.debugElement).not.toBeNull();
        expect(page.categoryName.domElement.value).toBe(testCategory.name);
      });
      it('should have clickable delete link', () => {
        const deleteUrl = `${testCategory.id}/delete`;
        expect(page.delete).not.toBeNull();
        expect(page.deleteRouteLink.strLinkParam).toBe(deleteUrl);
        expect(page.deleteRouteLink.navigatedTo).toBeUndefined();
        page.delete.triggerEventHandler('click', null);
        expect(page.deleteRouteLink.strNavigatedTo).toBe(deleteUrl);
      });
      it('should have spinner', () => {
        expect(page.spinner).not.toBeNull();
      });
      it('should have move', () => {
        expect(page.move).not.toBeNull();
      });
      it('should children categories', fakeAsync(() => {
        const child1 = new Category('child1', 2);
        const child2 = new Category('child2', 3, child1, testCategory);
        const child3 = new Category('child3', 4, child2, testCategory);
        testCategory.children = new CategoryList([child1, child2, child3]);
        fixture.detectChanges();
        tick();
        const children = page.children;
        expect(children).not.toBeNull();
        expect(children.length).toBeGreaterThan(0);
        for (let i = 0; i < children.length; i++) {
          const childCategory = testCategory.children.get(i);
          const childComponent = children[i].injector.get(CategoriesEditorItemComponent);
          expect(childComponent.category).toBe(childCategory);
        }
      }));
    });
    describe('change category name handlers', () => {
      function setup(categoryName: string = newCategoryName, categoryServiceReturn: Observable<any> = asyncData(undefined) ) {
        categoryServiceSpy.changeName.and.returnValue(categoryServiceReturn);
        debugElement.triggerEventHandler('focus', null);
        inputText(inputElement, categoryName);
        fixture.detectChanges();
      }
      function shouldShowError(message: string) {
        expect(page.categoryName.domElement.classList.contains('ng-invalid')).toBeTruthy();
        expect(page.categoryName.domElement.classList.contains('ng-dirty')).toBeTruthy();
        expect(page.categoryName.invalidFeedback.textContent).toMatch(new RegExp(message, 'i'));
      }
      const newCategoryName = 'changed test category name';
      let testCategory: Category, debugElement: DebugElement, inputElement: HTMLInputElement;
      let categoryServiceSpy: SpyObj<CategoryService>;
      beforeEach(fakeAsync(() => {
        testCategory = new Category('Test category', 1);
        componentTestZone.category = testCategory;
        fixture.detectChanges();
        tick();
        debugElement = page.categoryName.debugElement;
        inputElement = page.categoryName.domElement as HTMLInputElement;
        categoryServiceSpy = TestBed.get(CategoryService) as SpyObj<CategoryService>;
      }));
      it('should focus', () => {
        expect(component.isFocus).toBeFalsy();
        debugElement.triggerEventHandler('focus', null);
        expect(component.isFocus).toBeTruthy();
      });
      it('should wait debounce time', fakeAsync(() => {
        setup();
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
        tick(component.categoryNameChangeInterval);
        expect(categoryServiceSpy.changeName).toHaveBeenCalledWith(testCategory.id, newCategoryName);
        tick(component.categoryNameUpdatedTimeout);
      }));
      it('should skip debounce time when input losing focus', fakeAsync(() => {
        setup();
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
        debugElement.triggerEventHandler('blur', null);
        tick();
        expect(categoryServiceSpy.changeName).toHaveBeenCalledWith(testCategory.id, newCategoryName);
        tick(component.categoryNameUpdatedTimeout);
      }));
      it('should set updating and updated classes on root div', fakeAsync(() => {
        function expectedStatus(updating: boolean, updated: boolean) {
          expect(component.status.updating).toBe(updating);
          expect(component.status.updated).toBe(updated);
          fixture.detectChanges();
          expect(rootDiv.classList.contains('updating')).toBe(updating);
          expect(rootDiv.classList.contains('updated')).toBe(updated);
        }
        const fakeInterval = 1000;
        setup(newCategoryName, timer(fakeInterval));
        const rootDiv = component.item.nativeElement as HTMLDivElement;
        tick(component.categoryNameChangeInterval - 1);
        // before debounce
        expectedStatus(false, false);
        tick(1);
        // after debounce
        expectedStatus(true, false);
        tick(fakeInterval);
        // after stub response from server
        expectedStatus(false, true);
        tick(component.categoryNameUpdatedTimeout);
        expectedStatus(false, false);
      }));
      it('should disable input during waiting response form server', fakeAsync(() => {
        const fakeInterval = 1000;
        setup(newCategoryName, timer(fakeInterval));
        tick(component.categoryNameChangeInterval - 1);
        // before debounce
        expect(inputElement.disabled).toBeFalsy();
        tick(1);
        // after debounce
        fixture.detectChanges();
        tick();
        expect(inputElement.disabled).toBeTruthy();
        tick(fakeInterval);
        // after stub response from server
        fixture.detectChanges();
        tick();
        expect(inputElement.disabled).toBeFalsy();
        tick(component.categoryNameUpdatedTimeout);
      }));
      it('should don\`t send request with already saved category name', fakeAsync(() => {
        const savedCategoryName = testCategory.name;
        const spyChangeName = spyOn(component, 'changeName').and.callThrough();
        setup();
        expect(spyChangeName).toHaveBeenCalledWith(newCategoryName);
        inputText(inputElement, savedCategoryName);
        // fixture.detectChanges();
        expect(spyChangeName).toHaveBeenCalledWith(savedCategoryName);
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
        tick(component.categoryNameChangeInterval);
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
      }));
      it('should ignore string of whitespaces', fakeAsync(() => {
        const spyChangeName = spyOn(component, 'changeName').and.callThrough();
        const categoryNameWhitespaces = '   ';
        setup(categoryNameWhitespaces);
        expect(spyChangeName).toHaveBeenCalledWith(categoryNameWhitespaces);
        tick(component.categoryNameChangeInterval);
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
      }));
      it('should trim string', fakeAsync(() => {
        const spyChangeName = spyOn(component, 'changeName').and.callThrough();
        const categoryNameNew = ' name ';
        setup(categoryNameNew);
        expect(spyChangeName).toHaveBeenCalledWith(categoryNameNew);
        tick(component.categoryNameChangeInterval);
        expect(categoryServiceSpy.changeName).toHaveBeenCalledWith(testCategory.id, categoryNameNew.trim());
        tick(component.categoryNameUpdatedTimeout);
      }));
      it('should handle server error response', fakeAsync(() => {
        const serverError = new Error('server-error-msg');
        setup(newCategoryName, asyncError(serverError));
        tick(component.categoryNameUpdatedTimeout);
        expect(categoryServiceSpy.changeName).toHaveBeenCalled();
        expect(component.ngModelCategoryName.control.hasError('serverError')).toBeTruthy();
        expect(component.ngModelCategoryName.control.errors.serverError).toBe(serverError.message);
        expect(component.status.updated).toBeFalsy();
        fixture.detectChanges();
        shouldShowError(serverError.message);
      }));
      it('should hide server error after success saving changes on server', fakeAsync(() => {
        const serverError = new Error('server-error-msg');
        const componentChangeNameSpy = spyOn(component, 'changeName').and.callThrough();
        setup(newCategoryName, asyncError(serverError));
        tick(component.categoryNameUpdatedTimeout);
        expect(categoryServiceSpy.changeName).toHaveBeenCalled();
        expect(component.status.updated).toBeFalsy();
        expect(component.ngModelCategoryName.control.hasError('serverError')).toBeTruthy();
        setup('hide server error', asyncData(undefined));
        expect(componentChangeNameSpy).toHaveBeenCalled();
        tick(component.categoryNameUpdatedTimeout + 1);
        expect(categoryServiceSpy.changeName).toHaveBeenCalledTimes(2);
        expect(component.status.updated).toBeTruthy();
        expect(component.ngModelCategoryName.control.hasError('serverError')).toBeFalsy();
        tick(component.categoryNameUpdatedTimeout);
        expect(component.status.updated).toBeFalsy();
      }));
      it('should handle unexpected error', fakeAsync(() => {
        const errorMsg = 'test unexpected error';
        categoryServiceSpy.changeName.and.throwError(errorMsg);
        debugElement.triggerEventHandler('focus', null);
        inputText(inputElement, newCategoryName);
        fixture.detectChanges();
        tick(component.categoryNameUpdatedTimeout);
        expect(categoryServiceSpy.changeName).toHaveBeenCalled();
        fixture.detectChanges();
        shouldShowError(errorMsg);
        const categoryName = 'after unexpected error category name';
        setup(categoryName);
        tick(component.categoryNameUpdatedTimeout);
        expect(categoryServiceSpy.changeName).toHaveBeenCalledTimes(2);
        expect(page.categoryName.domElement.classList.contains('ng-invalid')).toBeFalsy();
        expect(page.categoryName.invalidFeedback.textContent).toBeFalsy();
        tick(component.categoryNameUpdatedTimeout);
      }));
      it('should handle required validation', fakeAsync(() => {
        setup('');
        tick(component.categoryNameUpdatedTimeout);
        expect(categoryServiceSpy.changeName).not.toHaveBeenCalled();
        shouldShowError('required');
      }));
    });
  });
  describe('in categoriesEditorComponent', () => {
    function getDebugElement(categoryId: number) {
      return fixture.debugElement.queryAll(By.directive(CategoriesEditorItemComponent)).find(
        de => de.injector.get(CategoriesEditorItemComponent).category.id === categoryId
      );
    }
    function setup(categoryId: number) {

      activatedRouteStub.data = asyncData({
        categoryTree: categoryTree
      });
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
      const debugElement = getDebugElement(categoryId);
      page = new CategoriesEditorItemPage(debugElement);
      return debugElement.injector.get(CategoriesEditorItemComponent);
    }
    interface CategoryTreeData {
      categoryTree: CategoryTree;
    }
    let fixture: ComponentFixture<CategoriesEditorComponent>;
    let componentCategoriesEditor: CategoriesEditorComponent;
    let page: CategoriesEditorItemPage;
    let activatedRouteStub: ActivatedRouteStubService<CategoryTreeData>;

    let categoryTree: CategoryTree;
    let dragEvent: SpyObj<DragEvent>;
    let dragDataServiceSpy: SpyObj<DragDataService<Category>>;
    let computedStyleServiceSpy: SpyObj<ComputedStyleService>;
    const categoriesDB = [
      new CategoryDB('category1', 1),
      new CategoryDB('category2', 2, undefined, 1),
      new CategoryDB('category3', 3, undefined, 2),
      new CategoryDB('category4', 4, undefined, 3),
      new CategoryDB('category3_1', 5, 3),
      new CategoryDB('category3_2', 6, 3, 5),
      new CategoryDB('category3_3', 7, 3, 6),
      new CategoryDB('category2_1', 8, 2),
      new CategoryDB('category3_2_1', 9, 6),
      new CategoryDB('category3_2_2', 10, 6, 9),
      new CategoryDB('category3_1_1', 11, 5)
    ];

    beforeEach(async(() => {
      const categorySpy = jasmine.createSpyObj('CategoryService', ['moveCategory']);
      const dragDataSpy = jasmine.createSpyObj('DragDataService', ['getData', 'setData']);
      const computedStyleSpy = jasmine.createSpyObj('ComputedStyleService', ['height', 'offsetTop']);
      TestBed.configureTestingModule({
        imports: [FormsModule],
        declarations: [CategoriesEditorComponent, CategoriesEditorItemComponent, NgbTooltipDirectiveStub, RouterLinkStubDirective],
        providers: [
          {provide: CategoryService, useValue: categorySpy},
          {provide: ComputedStyleService, useValue: computedStyleSpy},
          {provide: ActivatedRoute, useClass: ActivatedRouteStubService},
          {provide: BreadcrumbsService, useValue: {}}
        ]
      })
        .overrideComponent(CategoriesEditorComponent, {
          set: {
            providers: [
              {provide: DragDataService, useValue: dragDataSpy},
            ]
          }
        })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(CategoriesEditorComponent);
      componentCategoriesEditor = fixture.componentInstance;
      activatedRouteStub = fixture.debugElement.injector.get(ActivatedRoute) as unknown as ActivatedRouteStubService<CategoryTreeData>;
      dragDataServiceSpy = fixture.debugElement.injector.get(DragDataService) as SpyObj<DragDataService<Category>>;
      computedStyleServiceSpy = fixture.debugElement.injector.get(ComputedStyleService) as SpyObj<ComputedStyleService>;
      dragEvent = jasmine.createSpyObj('DragEvent', ['preventDefault']);
      categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
    });
    describe('drag and drop operations', () => {
      it('should dragStart', fakeAsync(() => {
        const categoryId = 2;
        const component = setup(categoryId);
        expect(component.category).toBeDefined();
        // @ts-ignore
        dragEvent.dataTransfer = jasmine.createSpyObj('DataTransfer', ['setData', 'setDragImage']);
        fixture.detectChanges();
        page.move.triggerEventHandler('dragstart', dragEvent);
        expect(dragEvent.dataTransfer.setData).toHaveBeenCalledWith('text/plain', categoryId.toString());
        expect(dragDataServiceSpy.setData).toHaveBeenCalled();
        expect(dragEvent.dataTransfer.dropEffect).toBe('move');
        expect(dragEvent.dataTransfer.effectAllowed).toBe('move');
        expect(dragEvent.dataTransfer.setDragImage).toHaveBeenCalledWith(component.item.nativeElement, 0, 0);
      }));
      const commonDragOperations = ['dragEnter', 'dragOver']; // BDD
      for (const dragOperation of commonDragOperations) {
        describe(dragOperation, () => {
          function setupDragEnter(movedCategoryId: number, droppedCategoryId: number, offsetY: number, parentOffsetY = 0) {
            const component = setup(droppedCategoryId);
            const movedCategory = getDebugElement(movedCategoryId).injector.get(CategoriesEditorItemComponent).category;
            dragDataServiceSpy.getData.and.returnValue(new DragData(movedCategory));
            computedStyleServiceSpy.height.and.returnValue(itemHeight);
            computedStyleServiceSpy.offsetTop.and.returnValue(parentOffsetY);
            // @ts-ignore
            dragEvent.offsetY = offsetY;
            page.root.triggerEventHandler(dragOperation.toLowerCase(), dragEvent);
            fixture.detectChanges();
            return component;
          }
          function checkStatus(component, expectedStatus: 'drop-before' | 'drop-inner' | 'drop-after') {
            const dropStatusNames = ['drop-before', 'drop-inner', 'drop-after'];
            dropStatusNames.forEach( statusName => {
              if (statusName === expectedStatus) {
                expect(page.rootEl.classList.contains(statusName)).toBeTruthy();
              } else {
                expect(page.rootEl.classList.contains(statusName)).toBeFalsy();
              }
            });
          }

          const itemHeight = 40;
          const offsetBefore = itemHeight / 4 - itemHeight / 10;
          const offsetInner = itemHeight / 4 + itemHeight / 10;
          const offsetAfter = itemHeight - itemHeight / 4 + itemHeight / 10;
          it('should set class drop-before', fakeAsync(() => {
            const movedCategoryId = 2, droppedCategoryId = 1;
            const component = setupDragEnter(movedCategoryId, droppedCategoryId, offsetBefore);
            checkStatus(component, 'drop-before');
          }));
          it('should set class drop-inner', fakeAsync(() => {
            const movedCategoryId = 2, droppedCategoryId = 1;
            const component = setupDragEnter(movedCategoryId, droppedCategoryId, offsetInner);
            checkStatus(component, 'drop-inner');
          }));
          it('should set class drop-after', fakeAsync(() => {
            const movedCategoryId = 4, droppedCategoryId = 1;
            const component = setupDragEnter(movedCategoryId, droppedCategoryId, offsetAfter);
            checkStatus(component, 'drop-after');
          }));
          it('should allow drop after last element', fakeAsync(() => {
            const movedCategoryId = 1, droppedCategoryId = 4;
            const component = setupDragEnter(movedCategoryId, droppedCategoryId, offsetAfter);
            checkStatus(component, 'drop-after');
          }));
          it('should allow drop before first element', fakeAsync(() => {
            const movedCategoryId = 4, droppedCategoryId = 1;
            const component = setupDragEnter(movedCategoryId, droppedCategoryId, offsetBefore);
            checkStatus(component, 'drop-before');
          }));
          if (dragOperation === 'dragEnter') {
            it('should increase countDragEnter when available drop operation', fakeAsync(() => {
              const component = setupDragEnter(2, 1, offsetBefore);
              expect(component.countDragEnter).toBe(1);
            }));
            it('should prevent increase countDragEnter when forbidden drop operation', fakeAsync(() => {
              const component = setupDragEnter(2, 1, offsetAfter);
              expect(component.countDragEnter).toBe(0);
            }));
          }
          it('should prevent default', fakeAsync(() => {
            setupDragEnter(2, 1, offsetBefore);
            expect(dragEvent.preventDefault).toHaveBeenCalled();
          }));
          it('should prevent move to child', fakeAsync(() => {
            const category3 = categoryTree.findCategoryByName('category3');
            const category3_1 = categoryTree.findCategoryByName('category3_1');
            setupDragEnter(category3.id, category3_1.id, 1);
            expect(dragEvent.preventDefault).not.toHaveBeenCalled();
          }));
          it('should prevent move inner to same category', fakeAsync(() => {
            setupDragEnter(2, 2, offsetInner);
            expect(dragEvent.preventDefault).not.toHaveBeenCalled();
          }));
          it('should prevent move to same parent category', fakeAsync(() => {
            const category3 = categoryTree.findCategoryByName('category3');
            const category3_1 = categoryTree.findCategoryByName('category3_1');
            setupDragEnter(category3_1.id, category3.id, offsetInner);
            expect(dragEvent.preventDefault).not.toHaveBeenCalled();
          }));
          it('should prevent move before next category', fakeAsync(() => {
            setupDragEnter(2, 3, offsetBefore);
            expect(dragEvent.preventDefault).not.toHaveBeenCalled();
          }));
          it('should prevent move after previous category', fakeAsync(() => {
            setupDragEnter(2, 1, offsetAfter);
            expect(dragEvent.preventDefault).not.toHaveBeenCalled();
          }));
        });
      }
      describe('dragLeave', () => {
        let component;
        beforeEach(fakeAsync(() => {
          component = setup(1);
        }));
        it('should decrease countDragEnter', () => {
          expect(component.countDragEnter).toBe(0);
          component.countDragEnter = 1;
          page.root.triggerEventHandler('dragleave', null);
          expect(component.countDragEnter).toBe(0);
        });
        it('should reset drop styles when countDragEnter equal zero', () => {
          for (const dropProp in DropTarget) {
            if (DropTarget.hasOwnProperty(dropProp)) {
              const dropClassName = DropTarget[dropProp];
              component.countDragEnter = 1;
              component.status[dropClassName] = true;
              page.root.triggerEventHandler('dragleave', null);
              expect(component.status[dropClassName]).toBeFalsy(dropClassName);
            }
          }
        });
      });
      it('should dragEnd reset drop styles', fakeAsync(() => {
        const component = setup(1);
        for (const dropProp in DropTarget) {
          if (DropTarget.hasOwnProperty(dropProp)) {
            const dropClassName = DropTarget[dropProp];
            component.status[dropClassName] = true;
            page.move.triggerEventHandler('dragend', null);
            expect(component.status[dropClassName]).toBeFalsy(dropClassName);
          }

        }
      }));
      describe('dragDrop', () => {
        function  setupDragDrop(categoryId: number, dropTarget: DropTarget, movedCategoryId: number, triggerDrop: boolean = true) {
          const dropAreaComponent = setup(categoryId);
          const movedComponent = getDebugElement(movedCategoryId).injector.get(CategoriesEditorItemComponent);
          const dragData = new DragData(movedComponent.category, dropTarget);
          categoryServiceSpy.moveCategory.and.returnValue(asyncData(undefined));
          dragDataServiceSpy.getData.and.returnValue(dragData);
          if (triggerDrop) {
            page.root.triggerEventHandler('drop', dragEvent);
          }
          return {
            dropAreaComponent,
            movedComponent
          };
        }
        let categoryServiceSpy: SpyObj<CategoryService>;
        beforeEach(() => {
          categoryServiceSpy = fixture.debugElement.injector.get(CategoryService) as SpyObj<CategoryService>;
        });
        it('should drop before', fakeAsync(() => {
          const dropAreaId = 2, movedId = 3;
          const {dropAreaComponent: {category: dropCategory}} = setupDragDrop(dropAreaId, DropTarget.before, movedId);
          expect(categoryServiceSpy.moveCategory).toHaveBeenCalledWith(CategoryTree.ROOT_ID, movedId, dropCategory.orderId);
        }));
        it('should drop inner', fakeAsync(() => {
          const dropAreaId = 2, movedId = 3;
          const {dropAreaComponent: {
            category: {children: children}
          }} = setupDragDrop(dropAreaId, DropTarget.inner, movedId);
          const lastId = children.length ? children.last.id : undefined;
          expect(categoryServiceSpy.moveCategory).toHaveBeenCalledWith(dropAreaId, movedId, lastId);
        }));
        it('should drop after', fakeAsync(() => {
          const dropAreaId = 2, movedId = 1;
          setupDragDrop(dropAreaId, DropTarget.after, movedId);
          expect(categoryServiceSpy.moveCategory).toHaveBeenCalledWith(CategoryTree.ROOT_ID, movedId, dropAreaId);
        }));
        it('should drop in no root categories', fakeAsync(() => {
          const dropAreaCategory = categoryTree.findCategoryByName('category3_1');
          const movedId = 1;
          const lastId = dropAreaCategory.children.length ? dropAreaCategory.children.last.id : undefined;
          setupDragDrop(dropAreaCategory.id, DropTarget.inner, movedId);
          expect(categoryServiceSpy.moveCategory).toHaveBeenCalledWith(dropAreaCategory.id, movedId, lastId);
        }));
        it('should reset drop styles', fakeAsync(() => {
          const {dropAreaComponent} = setupDragDrop(2, DropTarget.inner, 1, false);
          dropAreaComponent.status[DropTarget.inner] = true;
          fixture.detectChanges();
          expect(page.rootEl.classList.contains(DropTarget.inner)).toBeTruthy();
          page.root.triggerEventHandler('drop', dragEvent);
          expect(dropAreaComponent.status[DropTarget.inner]).toBeFalsy();
          fixture.detectChanges();
          expect(page.rootEl.classList.contains(DropTarget.inner)).toBeFalsy();
        }));
        it('should call preventDefault', fakeAsync(() => {
          setupDragDrop(2, DropTarget.inner, 1);
          expect(dragEvent.preventDefault).toHaveBeenCalled();
        }));
      });
    });
  });
});

class CategoriesEditorItemPage extends TestPage {
  readonly selectors = {
    root: '.item',
    inputs:  {
      categoryName: 'input[name="categoryName"]',
    },
    delete: '.actions .delete',
    spinner: '.actions .spinner',
    move: '.actions .move',
  };
  get root(): DebugElement {
    return this.queryByCss(this.selectors.root);
  }
  get rootEl(): HTMLDivElement {
    return this.root.nativeElement;
  }
  get categoryName(): FieldDataLazy<HTMLInputElement | HTMLTextAreaElement> {
    return this.inputFields['categoryName'];
  }
  get delete(): DebugElement {
    return this.queryByCss(this.selectors.delete);
  }
  get deleteRouteLink(): RouterLinkStubDirective {
    return this.delete.injector.get(RouterLinkStubDirective);
  }
  get spinner(): DebugElement {
    return this.queryByCss(this.selectors.spinner);
  }
  get move(): DebugElement {
    return this.queryByCss(this.selectors.move);
  }
  get children(): DebugElement[] {
    return this.queryAllByDirective(CategoriesEditorItemComponent);
  }
  constructor(protected debugElement: DebugElement) {
    super(debugElement);
    const component = debugElement.injector.get(CategoriesEditorItemComponent);
    this.initInputFields(this.selectors.inputs, component);
  }
}
