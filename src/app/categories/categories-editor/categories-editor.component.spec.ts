import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { CategoriesEditorComponent } from './categories-editor.component';
import {Component, DebugElement, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DragDataService} from '../../common/app-services/drag-data.service';
import {ActivatedRouteStubService} from '../../common/tests/router/activated-route-stub.service';
import {Category} from '../classes/category';
import {asyncData, TestPage} from '../../common/tests/common.spec';
import {CategoryTree} from '../classes/category-tree';
import {CategoryDB} from '../classes/category-db';

describe('CategoriesEditorComponent', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-categories-editor-item',
    template: ''
  })
  class CategoriesEditorItemStubComponent {
    @Input()
    category: Category;
  }
  let component: CategoriesEditorComponent;
  let debugElement: DebugElement;
  let page: CategoriesEditorPage;
  let fixture: ComponentFixture<CategoriesEditorComponent>;
  let activatedRouteStubService: ActivatedRouteStubService<{categoryTree?: CategoryTree}>;

  beforeEach(async(() => {
    const dragDataServiceStub: Partial<DragDataService<never>> = {};
    TestBed.configureTestingModule({
      declarations: [ CategoriesEditorComponent, CategoriesEditorItemStubComponent ],
      providers: [
        {provide: ActivatedRoute, useClass: ActivatedRouteStubService}
      ]
    })
    .overrideComponent(CategoriesEditorComponent, {
      set: {
        providers: [{provide: DragDataService, useValue: dragDataServiceStub}]
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesEditorComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    page = new CategoriesEditorPage(debugElement);
    activatedRouteStubService = debugElement.injector.get(ActivatedRoute) as ActivatedRouteStubService<{categoryTree?: CategoryTree}>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should show title', () => {
    fixture.detectChanges();
    expect(page.title).not.toBeNull();
    expect((<HTMLElement>page.title.nativeElement).textContent.trim()).toBeTruthy();
  });
  it('should show message about empty list of categories', fakeAsync(() => {
    activatedRouteStubService.data = asyncData({
      categoryTree: new CategoryTree()
    });
    fixture.detectChanges();
    expect(page.emptyMsg).toBeNull();
    tick();
    fixture.detectChanges();
    expect(page.emptyMsg).not.toBeNull();
    expect((<HTMLElement>page.emptyMsg.nativeElement).textContent.trim().length).toBeGreaterThan(0);
  }));
  it('should show children of root category', fakeAsync(() => {
    const categoriesDB = [
      new CategoryDB('category-1-1', 1, undefined, undefined),
      new CategoryDB('category-1-2', 2, undefined, 1),
      new CategoryDB('category-1-3', 3, undefined, 2),
      new CategoryDB('category-2-1', 4, 2, undefined),
      new CategoryDB('category-2-2', 5, 2, 4)
    ];
    const categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
    activatedRouteStubService.data = asyncData({
      categoryTree: categoryTree
    });
    fixture.detectChanges();
    expect(page.categoriesEditorItems.length).toBe(0);
    expect(page.emptyMsg).toBeNull();
    tick();
    fixture.detectChanges();
    expect(page.emptyMsg).toBeNull();
    expect(component.categoryTree).toBe(categoryTree);
    const categoriesEditorItems = page.categoriesEditorItems;
    expect(categoriesEditorItems.length).toBe(categoryTree.children.length - 1);
    for (let i = 0; i < categoriesEditorItems.length; i++) {
      const item = categoriesEditorItems[i];
      const category = categoryTree.children.get(i);
      const itemComponent = item.injector.get(CategoriesEditorItemStubComponent);
      expect(category).toBe(itemComponent.category);
      expect(itemComponent.category.id).not.toBe(CategoryTree.OTHER_ID);
    }
  }));
});

class CategoriesEditorPage extends TestPage {
  readonly selectors = {
    title: 'h1',
    categoriesEditorItem: '.categories-editor-item',
    emptyMsg: '.empty-msg',
  };
  get title(): DebugElement {
    return this.queryByCss(this.selectors.title);
  }
  get categoriesEditorItems(): DebugElement[] {
    return this.queryAllByCss(this.selectors.categoriesEditorItem);
  }
  get emptyMsg(): DebugElement {
    return this.queryByCss(this.selectors.emptyMsg);
  }
}
