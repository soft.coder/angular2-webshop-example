import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CategoryTree} from '../classes/category-tree';
import {DragDataService} from '../../common/app-services/drag-data.service';

@Component({
  selector: 'app-categories-editor',
  templateUrl: './categories-editor.component.html',
  styleUrls: ['./categories-editor.component.less'],
  providers: [DragDataService]
})
export class CategoriesEditorComponent implements OnInit {

  public categoryTree: CategoryTree;

  get OTHER_ID() {
    return CategoryTree.OTHER_ID;
  }
  constructor(private  activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(
      (data: {categoryTree?: CategoryTree}) => {
        if (data.categoryTree) { // root categories
          this.categoryTree = data.categoryTree;
        }
      }
    );
  }

}
