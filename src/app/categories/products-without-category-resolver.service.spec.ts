import { TestBed, inject } from '@angular/core/testing';

import { ProductsWithoutCategoryResolverService } from './products-without-category-resolver.service';
import {CategoryService} from './category.service';
import {of} from 'rxjs/internal/observable/of';
import {Product} from '../product/product-detail/product';

describe('ProductsWithoutCategoryResolverService', () => {
  beforeEach(() => {
    const categoryServiceStub: Partial<CategoryService> = {
      getProductsWithoutCategories() {
        return of([
          new Product('Test product 1', 'description', 100, 1),
          new Product('Test product 2', 'description', 200, 2)
        ]);
      }
    };
    TestBed.configureTestingModule({
      providers: [
        ProductsWithoutCategoryResolverService,
        {provide: CategoryService, useValue: categoryServiceStub}
      ]
    });
  });

  it('should be created', inject([ProductsWithoutCategoryResolverService], (service: ProductsWithoutCategoryResolverService) => {
    expect(service).toBeTruthy();
  }));
});
