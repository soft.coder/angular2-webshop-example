import {Component, OnInit} from '@angular/core';
import {Category} from '../classes/category';
import {ActivatedRoute} from '@angular/router';
import {Breadcrumb} from '../../common/app-widgets/breadcrumbs/breadcrumb';
import {Product} from '../../product/product-detail/product';
import {BreadcrumbsService} from '../../common/app-services/breadcrumbs.service';
import {CategoryTree} from '../classes/category-tree';
import {CategoryBag} from '../classes/category-bag';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.less']
})
export class CategoryListComponent implements OnInit {
  readonly otherCategoryName = 'Products without categories';
  categoryTree: CategoryTree;
  currentCategory: Category;
  get OTHER_ID() {
    return CategoryTree.OTHER_ID;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private breadcrumbsService: BreadcrumbsService) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(
      (data: {categoryBag?: CategoryBag, categoryTree?: CategoryTree, products?: Product[]}) => {
        if (data.categoryBag) { // not root category
          this.categoryTree = data.categoryBag.tree;
          this.currentCategory = data.categoryBag.category;
        }  else if (data.products) { // products without categories
          this.categoryTree = new CategoryTree([]);
          this.categoryTree.other.products = data.products;
          this.currentCategory = this.categoryTree.other;
        } else if (data.categoryTree) { // root categories
          this.categoryTree = data.categoryTree;
          this.currentCategory = data.categoryTree.root;
        }
        this.breadcrumbsService.breadcrumbs = this.getBreakcrumb();
      }
    );


  }
  private getBreakcrumb() {
    const breadcrumb: Breadcrumb[] = [];
    const URL = '/categories';
    const walkUp = (category: Category) => {
      const id = category.id !== this.OTHER_ID ? category.id : 'other';
      breadcrumb.unshift(
        new Breadcrumb(`${URL}/${id}`, category.name)
      );
      if (category.parent && category.parentId !== CategoryTree.ROOT_ID) {
        walkUp(category.parent);
      }
    };
    if (this.currentCategory !== this.categoryTree.root) {
      walkUp(this.currentCategory);
    }
    breadcrumb.unshift(
      new Breadcrumb(URL,  this.categoryTree.root.name)
    );
    return breadcrumb;
  }
}
