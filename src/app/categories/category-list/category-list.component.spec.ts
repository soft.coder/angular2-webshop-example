import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { CategoryListComponent } from './category-list.component';
import {Component, DebugElement, Input} from '@angular/core';
import {Product} from '../../product/product-detail/product';
import {RouterLinkStubDirective} from '../../common/tests/router/router-link-stub.directive';
import {UppercaseFirstLetterStubPipe} from '../../common/tests/uppercase-first-letter-stub.pipe';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute} from '@angular/router';
import {ActivatedRouteStubService} from '../../common/tests/router/activated-route-stub.service';
import {BreadcrumbsService} from '../../common/app-services/breadcrumbs.service';
import {Category} from '../classes/category';
import {CategoryTree} from '../classes/category-tree';
import {CategoryDB} from '../classes/category-db';
import {CategoryBag} from '../classes/category-bag';
import {asyncData, TestPage} from '../../common/tests/common.spec';
import {Breadcrumb} from '../../common/app-widgets/breadcrumbs/breadcrumb';



describe('CategoryListComponent', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-product-preview',
    template: ''
  })
  class ProductPreviewStubComponent {
    @Input()
    product: Product;
    @Input()
    categoryUrl: string;
  }
  let component: CategoryListComponent;
  let fixture: ComponentFixture<CategoryListComponent>;
  let activatedRouteStub: ActivatedRouteStubService<Object>;
  let breadcrumbsServiceStub: Partial<BreadcrumbsService>;
  const testProducts: Product[] = [
    new Product('root-product-name-1', 'root-product-description-1', 100, 10, 1),
    new Product('root-product-name-1', 'root-product-description-2', 101, 11, 2),
    new Product('root-product-name-1', 'root-product-description-3', 102, 12, 3),
  ];
  const categoriesDB = [
    new CategoryDB('category-level-1-1', 1, undefined, undefined),
    new CategoryDB('category-level-1-2', 2, undefined, 1),
    new CategoryDB('category-level-1-3', 3, undefined, 2),
    new CategoryDB('category-level-2-1', 4, 2, undefined),
    new CategoryDB('category-level-2-2', 5, 2, 4)
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ CategoryListComponent, ProductPreviewStubComponent, RouterLinkStubDirective, UppercaseFirstLetterStubPipe ],
      providers: [
        {provide: ActivatedRoute, useClass: ActivatedRouteStubService},
        {provide: BreadcrumbsService, useValue: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryListComponent);
    component = fixture.componentInstance;
    activatedRouteStub = fixture.debugElement.injector.get(ActivatedRoute) as ActivatedRouteStubService<Object>;
    breadcrumbsServiceStub = fixture.debugElement.injector.get(BreadcrumbsService) as Partial<BreadcrumbsService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('view modes', () => {
    let categoryTree: CategoryTree, noRootCategoryWithChildren: Category;
    beforeEach(() => {
      categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
      noRootCategoryWithChildren = categoryTree.findCategoryById(2);
    });
    it('should show root category', fakeAsync(() => {
      categoryTree.root.products = testProducts;
      activatedRouteStub.data = asyncData({
        categoryTree: categoryTree
      });
      fixture.detectChanges();
      tick();
      expect(component.categoryTree).toBe(categoryTree);
      expect(component.currentCategory).toBe(categoryTree.root);
      categoryTree.root.products = undefined;
    }));
    it('should show no root category', fakeAsync(() => {
      let categoryBag: CategoryBag;
      categoryBag = {
        category: noRootCategoryWithChildren,
        tree: categoryTree
      };
      activatedRouteStub.data = asyncData({categoryBag: categoryBag});
      fixture.detectChanges();
      tick();
      expect(component.currentCategory).toBe(noRootCategoryWithChildren);
      expect(component.categoryTree).toBe(categoryTree);
    }));
    it('should show products without categories', fakeAsync(() => {
      activatedRouteStub.data = asyncData({
        products: testProducts
      });
      fixture.detectChanges();
      tick();
      expect(component.currentCategory.id).toBe(CategoryTree.OTHER_ID);

      expect(component.currentCategory.products).toBe(testProducts);
      expect(component.categoryTree.children.length).toBe(1, 'only other category');
    }));
  });
  describe('template binding', () => {
    describe('no root category with child categories', () => {
      let categoryTree: CategoryTree, noRootCategoryWithChildren: Category;
      let page: CategoryListPage;
      beforeEach(() => {
        categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
        noRootCategoryWithChildren = categoryTree.findCategoryById(2);
        page = new CategoryListPage(fixture.debugElement);
      });
      beforeEach(fakeAsync(() => {
        const categoryBag: CategoryBag = {
          category: noRootCategoryWithChildren,
          tree: categoryTree
        };
        activatedRouteStub.data = asyncData({categoryBag: categoryBag});
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
      }));
      it('should show current category name', () => {
        expect(component.currentCategory).toBeDefined();
        expect(component.currentCategory.name).toBeDefined();
        expect(page.currentCategoryName).not.toBeNull();
        expect(page.currentCategoryNameEl.textContent).toBe(noRootCategoryWithChildren.name);
      });
      it('should show products', () => {
        component.currentCategory.products = testProducts;
        fixture.detectChanges();
        const length = noRootCategoryWithChildren.products.length;
        const productPreviews = page.productPreviews;
        for (let i = 0; i < length; i++) {
          const product = noRootCategoryWithChildren.products[i];
          const categoryId = noRootCategoryWithChildren.id;
          const productComponent = productPreviews[i].injector.get(ProductPreviewStubComponent);
          expect(productComponent.product).toBe(product);
          expect(productComponent.categoryUrl).toBe(`/categories/${categoryId}`);
        }
      });
      it('should show message about empty products and category list in current category', () => {
        const children = component.currentCategory.children;
        while (children.length > 0) {
          const child = children.get(0);
          children.remove(child.id);
        }
        fixture.detectChanges();
        expect(component.currentCategory.products.length).toBe(0);
        expect(children.length).toBe(0);
        expect(page.productListEmptyMessage).not.toBeNull();
        const el = page.productListEmptyMessage.nativeElement as HTMLElement;
        expect(el.textContent.length).toBeGreaterThan(0);
      });
      it('should show clickable edit link', () => {
        const editUrl = '/categories/edit';
        const editCategoryLink = page.editCategoryLink;
        expect(editCategoryLink).toBeDefined();
        // @ts-ignore
        const routeLinkStub = editCategoryLink.injector.get(RouterLinkStubDirective);
        expect(routeLinkStub.strLinkParam).toBe('/categories/edit');
        expect(routeLinkStub.strNavigatedTo).toBeUndefined();
        editCategoryLink.triggerEventHandler('click', null);
        expect(routeLinkStub.strNavigatedTo).toBe(editUrl);
      });
      it('should show child categories', () => {
        const childCategories = page.childCategories;
        expect(childCategories.length).toBe(noRootCategoryWithChildren.children.length);
        for (let i = 0; i < childCategories.length; i++) {
          const child = childCategories[i];
          const childEl = child.nativeElement as HTMLElement;
          const routerLink = child.injector.get(RouterLinkStubDirective);
          const category = noRootCategoryWithChildren.children.get(i);
          const categoryUrl = `/categories/${category.id}`;
          expect(childEl.textContent.trim()).toBe(category.name);
          expect(routerLink.strLinkParam).toBe(`/categories/${category.id}`);
          expect(routerLink.strNavigatedTo).toBeUndefined();
          child.triggerEventHandler('click', null);
          expect(routerLink.strNavigatedTo).toBe(categoryUrl);
        }
      });
      it('should set breadcrumbs', () => {
        const url = '/categories';
        const breadcrumbs: Breadcrumb[] = [];
        function initBreadcrumbs(category: Category) {
          breadcrumbs.unshift(new Breadcrumb(`${url}/${category.id}`, category.name));
        }
        initBreadcrumbs(noRootCategoryWithChildren);
        breadcrumbs.unshift(new Breadcrumb(url, CategoryTree.ROOT_CATEGORY_NAME));
        expect(breadcrumbsServiceStub.breadcrumbs).toBeDefined();
        expect(breadcrumbsServiceStub.breadcrumbs.length).toBeDefined(breadcrumbs.length);
        for (let i = 0; i < breadcrumbs.length; i++) {
          expect(breadcrumbsServiceStub.breadcrumbs[i].name).toBe(breadcrumbs[i].name);
          expect(breadcrumbsServiceStub.breadcrumbs[i].url).toBe(breadcrumbs[i].url);
        }
      });
    });
    describe('no root category without child categories', () => {
      let categoryTree: CategoryTree, noRootCategoryWithoutChildren: Category;
      let page: CategoryListPage;
      beforeEach(() => {
        categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
        noRootCategoryWithoutChildren = categoryTree.findCategoryById(1);
        page = new CategoryListPage(fixture.debugElement);
      });
      beforeEach(fakeAsync(() => {
        const categoryBag: CategoryBag = {
          category: noRootCategoryWithoutChildren,
          tree: categoryTree
        };
        activatedRouteStub.data = asyncData({categoryBag: categoryBag});
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
      }));
      it('should show message about empty products and category list in current category', () => {
        expect(component.currentCategory.products.length).toBe(0);
        expect(component.currentCategory.children.length).toBe(0);
        expect(page.productListEmptyMessage).not.toBeNull();
        const el = page.productListEmptyMessage.nativeElement as HTMLElement;
        expect(el.textContent.length).toBeGreaterThan(0);
      });
      it('should hide list of child categories', () => {
        expect(page.childCategoryList).toBeNull();
      });

    });
    describe('root category', () => {
      let categoryTree: CategoryTree, page: CategoryListPage;
      beforeEach(() => {
        categoryTree = CategoryTree.buildTreeOfCategories(categoriesDB);
        page = new CategoryListPage(fixture.debugElement);
      });
      beforeEach(fakeAsync(() => {
        activatedRouteStub.data = asyncData({categoryTree: categoryTree});
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
      }));
      it('should show other category link', () => {
        const otherUrl = 'other';
        const other = page.otherCategory;
        expect(other).not.toBeNull();
        const routeLink = other.injector.get(RouterLinkStubDirective);
        const otherEl = other.nativeElement as HTMLElement;
        expect(otherEl.textContent.trim()).toBe(CategoryTree.OTHER_CATEGORY_NAME);
        expect(routeLink.strLinkParam).toBe(otherUrl);
        expect(routeLink.strNavigatedTo).toBeUndefined();
        other.triggerEventHandler('click', null);
        expect(routeLink.strNavigatedTo).toBe(otherUrl);
      });
      it('should show root category name', () => {
        expect(page.currentCategoryNameEl.textContent.trim()).toBe(CategoryTree.ROOT_CATEGORY_NAME);
      });
      it('should return id of category which contains products without category', () => {
        expect(component.OTHER_ID).toBe(CategoryTree.OTHER_ID);
      });

      it('should show child categories', () => {
        const children = page.childCategories;
        expect(children.length).toBe(categoryTree.children.length - 1);
        for (let i = 0; i < children.length; i++) {
          const child = children[i].nativeElement as HTMLElement;
          const childCategory = categoryTree.children.get(i);
          if (childCategory.id !== CategoryTree.OTHER_ID) {
            expect(child.textContent.trim()).toBe(childCategory.name);
          }
        }
      });
    });
    describe('other category', () => {
      let page: CategoryListPage;
      beforeEach(() => {
        page = new CategoryListPage(fixture.debugElement);
      });
      beforeEach(fakeAsync(() => {
        activatedRouteStub.data = asyncData({products: testProducts});
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
      }));
      it('should show product previews which set correct categoryUrl property', () => {
        const otherUrl = '/categories/other';
        const productPreviews = page.productPreviews;
        expect(productPreviews.length).toBe(testProducts.length);
        expect(component.currentCategory.id).toBe(CategoryTree.OTHER_ID);
        for (let i = 0; i < productPreviews.length; i++) {
          const productPreviewComponent = productPreviews[i].injector.get(ProductPreviewStubComponent);
          expect(productPreviewComponent.categoryUrl).toBe(otherUrl);
        }
      });
      it('should show other category name', () => {
        expect(page.currentCategoryNameEl.textContent).toBe(CategoryTree.OTHER_CATEGORY_NAME);
      });
    });
  });
});

class CategoryListPage extends TestPage {
  readonly selectors = {
    currentCategoryName: '.current-category-name',
    productPreview: '.product-list .product',
    editCategoryLink: '.edit-category-link',
    productListEmpty: '.product-list-empty',
    childCategory: '.child-category',
    otherCategory: '.other-category',
    childCategoryList: '.child-category-list'
  };
  get currentCategoryName(): DebugElement {
    return this.queryByCss(this.selectors.currentCategoryName);
  }
  get currentCategoryNameEl(): HTMLElement {
    return this.currentCategoryName.nativeElement;
  }
  get productPreviews(): DebugElement[] {
    return this.queryAllByCss(this.selectors.productPreview);
  }
  get editCategoryLink(): DebugElement {
    return this.queryByCss(this.selectors.editCategoryLink);
  }
  get productListEmptyMessage(): DebugElement {
    return this.queryByCss((this.selectors.productListEmpty));
  }
  get childCategories(): DebugElement[] {
    return this.queryAllByCss(this.selectors.childCategory);
  }
  get otherCategory(): DebugElement {
    return this.queryByCss(this.selectors.otherCategory);
  }
  get childCategoryList(): DebugElement {
    return this.queryByCss(this.selectors.childCategoryList);

  }
}
