import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Category, CategoryList} from './classes/category';
import {Product} from '../product/product-detail/product';
import {CategoryProduct} from './category-product';
import {forkJoin, Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';
import {tap} from 'rxjs/internal/operators/tap';
import {ApiUrlService} from '../api-url.service';
import {CategoryBag} from './classes/category-bag';
import {CategoryTree} from './classes/category-tree';
import {CategoryDB} from './classes/category-db';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public static readonly OTHER_ID: number = -1;
  private categoryTree: CategoryTree;
  private isUpdatedCategory: boolean;

  constructor(private http: HttpClient, private url: ApiUrlService) { }

  public getAllWithProducts() { // may be faster when catalog small
    return forkJoin(
      this.http.get<Category[]>(this.url.categories),
      this.http.get<Product[]>(this.url.products),
      this.http.get<CategoryProduct[]>(this.url.categoryProduct)
    ).pipe(
      map(([categories, products, categoryProducts]: [Category[], Product[], CategoryProduct[]])  => {
        return this.getCategoriesWithProducts(categories, products, categoryProducts);
      })
    );
  }
  public getAll() {
    return this.http.get<CategoryDB[]>(this.url.categories).pipe(
      map((categories: CategoryDB[]) => {
        categories = categories.map(c => new CategoryDB(c.name, c.id, c.parentId, c.orderId)); // Object
        this.categoryTree = CategoryTree.buildTreeOfCategories(categories);
        return this.categoryTree;
      })
    );
  }
  public getProductsWithoutCategories(): Observable<Product[]> {
    return forkJoin(
      this.http.get<Product[]>(this.url.products),
      this.http.get<CategoryProduct[]>(this.url.categoryProduct)
    ).pipe(
      switchMap(([products, categoryProducts]) => {
        const productsWithoutCategories =  products.filter(p => {
            return !categoryProducts.some(cp => cp.productId === p.id);
        });
        return of(productsWithoutCategories);
      })
    );
  }
  public get(id: number) {
    if (!this.categoryTree || this.isUpdatedCategory) {
      this.isUpdatedCategory = false;
      return this.getAll().pipe(switchMap(() => this.get(id)));
    }
    const category = this.categoryTree.findCategoryById(id);
    return this.http.get<CategoryProduct[]>(
      `${this.url.categoryProduct}?categoryId=^${id}$`
    ).pipe(
      switchMap((categoryProducts: CategoryProduct[]) => {
        if (categoryProducts.length) {
          const products$: Array<Observable<Product>> = categoryProducts.map(cp => {
            return this.http.get<Product>(`${this.url.products}/${cp.productId}`);
          });
          return forkJoin(products$).pipe(
            switchMap((products: Product[]) => {
              category.products = products;
              const categoryBag: CategoryBag = {
                category: category,
                tree: this.categoryTree,
              };
              return of(categoryBag);
            })
          );
        } else {
          const categoryBag: CategoryBag = {
            category: category,
            tree: this.categoryTree,
          };
          return of(categoryBag);
        }
      })
    );
  }
  public findCategoryInTree(id: number) {
    function findCategory(category: Category, deep: number = 1) { // recursion
      for (const childCategory of category.children) {
        if (childCategory.id === id) {
          return childCategory;
        } else {
          const result = findCategory(childCategory, deep + 1 );
          if (result) {
            return result;
          }
        }
      }
    }
    for (const rootCategory of this.categoryTree.children) {
      if (rootCategory.id === id) {
        return rootCategory;
      } else {
        const result = findCategory(rootCategory);
        if (result) {
          return result;
        }
      }
    }
  }
  public changeName(id: number, name: string) {
    const category = this.categoryTree.findCategoryById(id);
    return this.update(category.toCategoryDB());
  }
  moveCategory(parentId: number, categoryId: number, orderId: number) {
    const convertToDbParentId = (id: number) => {
      return id !== CategoryTree.ROOT_ID ? id : undefined;
    };
    const moved = this.categoryTree.findCategoryById(categoryId);
    const movedDB = new CategoryDB(moved.name, moved.id, convertToDbParentId(parentId), orderId);
    const dbCommands: Observable<any>[] = [this.update(movedDB)];
    const oldPlaceNext = moved.parent.children.findByOrderId(moved.id);
    if (oldPlaceNext) {
      const oldPlaceNextDB = new CategoryDB(oldPlaceNext.name, oldPlaceNext.id,
        convertToDbParentId(oldPlaceNext.parentId), moved.orderId
      );
      dbCommands.push(this.update(oldPlaceNextDB));
    }
    const newParent = this.categoryTree.findCategoryById(parentId);
    const newPlaceNext = newParent.children.findByOrderId(orderId);
    if (newPlaceNext) {
      const newPlaceNextDB = new CategoryDB(newPlaceNext.name, newPlaceNext.id,
        convertToDbParentId(newPlaceNext.parentId), movedDB.id
      );
      dbCommands.push(this.update(newPlaceNextDB));
    }
    return forkJoin(dbCommands).pipe(tap(() => this.categoryTree.moveCategory(parentId, orderId, categoryId)));
  }
  private update(categoryDB: CategoryDB) {
    return this.http.put<null>(`${this.url.categories}/${categoryDB.id}`, categoryDB).pipe(
      tap(() => {
        this.isUpdatedCategory = true;
      })
    );
  }
  private getCategoriesWithProducts(categories: Category[], products: Product[], categoryProductMap: CategoryProduct[]) {
    function deepClone(obj: any) {
      return JSON.parse(JSON.stringify(obj));
    }
    function findProducts(categoryId: number) {
      return categoryProductMap
        .filter(cp => cp.categoryId === categoryId)
        .map(categoryProduct => {
          return products.find(p => p.id === categoryProduct.productId);
        });
    }
    function findProductsWithoutCategories() {
      return products.filter(p => {
        return !categoryProductMap.some(cp => cp.productId === p.id);
      });
    }
    function findChild(parent: Category, deep: number = 1) {
      return categories.filter(category => {
        if (category.parent.id === parent.id) {
          category.parent = parent;
          category.children = new CategoryList(findChild(category, deep + 1)); // recursion
          category.products = findProducts(category.id);
          return true;
        }
      });
    }
    const result = categories.filter(category => {
      if (!category.parent.id) {
        category.children = new CategoryList(findChild(category));
        category.products = findProducts(category.id);
        if (category.children.length || category.products.length) {
          return true;
        }
      }
    });
    result.push(
      new Category(
        'Other',
        undefined,
        result[result.length - 1],
        undefined,
        undefined,
        /*otherProducts*/
      )
    );
    return result;
  }

/*
  private buildTreeOfCategories(categoriesDB: CategoryDB[]) {
    function findChild(parentDB: CategoryDB, deep: number = 1) {
      const childCategories: Category[] = [];
      categoriesDB.forEach(categoryDB => {
        if (categoryDB.parentId === parentDB.id) {
          const children = findChild(categoryDB, deep + 1); // recursion
          childCategories.push(categoryDB.toCategory(parentDB, children));
        }
      });
      return childCategories;
    }
    const categories: Category[] = [];
    CategoryDB.sortByOrderId(categoriesDB).forEach((categoryDB, index, sortedCategories) => {
      if (!categoryDB.parentId) {
        const children = findChild(categoryDB);
        const category = new Category(categoryDB.name, categoryDB.id,
          index - 1 > 0 ? categories[index - 1] : undefined
        );
        categories.push(categoryDB.toCategory(undefined, children));
        return true;
      }
    });
    return categories;
  }
*/
}
