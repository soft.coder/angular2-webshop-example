import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CategoryService} from './category.service';
import {tap} from 'rxjs/internal/operators/tap';
import {CategoryBag} from './classes/category-bag';

@Injectable({
  providedIn: 'root'
})
export class CategoryResolverService implements Resolve<CategoryBag> {

  constructor(private categoryService: CategoryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CategoryBag> {
    const id = parseInt(route.paramMap.get('id'), 10);
    if (!isNaN(id) && id > 0) {
      return this.categoryService.get(id);
    }
  }
}
