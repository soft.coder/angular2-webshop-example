import { Injectable } from '@angular/core';
import {Category} from './classes/category';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {CategoryService} from './category.service';
import {Product} from '../product/product-detail/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsWithoutCategoryResolverService implements Resolve<Product[]> {

  constructor(private categoryService: CategoryService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product[]>  {
    return this.categoryService.getProductsWithoutCategories();

  }
}
