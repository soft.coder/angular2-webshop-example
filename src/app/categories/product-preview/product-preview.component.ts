import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../product/product-detail/product';
import {UserService} from '../../common/app-services/user.service';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.less']
})
export class ProductPreviewComponent implements OnInit {
  @Input()
  product: Product;
  @Input()
  categoryUrl: string;
  get isAdmin() {
    return this.userService.isAdmin;
  }

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  delete() {

  }
}
