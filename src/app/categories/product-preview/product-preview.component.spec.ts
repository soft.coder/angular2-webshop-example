import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductPreviewComponent } from './product-preview.component';
import {RouterLinkStubDirective} from '../../common/tests/router/router-link-stub.directive';
import {BackUrlStubDirective} from '../../common/tests/back-url-stub.directive';
import {UppercaseFirstLetterStubPipe} from '../../common/tests/uppercase-first-letter-stub.pipe';
import {UserService} from '../../common/app-services/user.service';
import {TextOverflowStubPipe} from '../../common/tests/text-overflow-stub.pipe';
import {Component, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {Product} from '../../product/product-detail/product';
import {TestPage} from '../../common/tests/common.spec';
import {CurrencyStub} from '../../common/tests/currency-stub.pipe';

describe('ProductPreviewComponent', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-component-test-zone',
    template: '<app-product-preview [product]="product" [categoryUrl]="categoryUrl"></app-product-preview>'
  })
  class TestZoneComponent {
    product: Product;
    categoryUrl: string;
  }
  let cTestZone: TestZoneComponent;
  let cProductPreview: ProductPreviewComponent;
  let deProductPreview: DebugElement;
  let fixture: ComponentFixture<TestZoneComponent>;
  let userService: Partial<UserService>;
  let page: ProductPreviewPage;
  const product = new Product('name', 'description', 100, 10, 1);

  beforeEach(async(() => {
    const userServiceStub: Partial<UserService> = {
      isAdmin: true
    };
    TestBed.configureTestingModule({
      declarations: [
        TestZoneComponent,
        ProductPreviewComponent,
        BackUrlStubDirective,
        CurrencyStub,
        UppercaseFirstLetterStubPipe,
        RouterLinkStubDirective,
        TextOverflowStubPipe],
      providers: [
        {provide: UserService, useValue: userServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestZoneComponent);
    cTestZone = fixture.componentInstance;
    deProductPreview = fixture.debugElement.query(By.directive(ProductPreviewComponent));
    cProductPreview = deProductPreview.injector.get(ProductPreviewComponent);
    page = new ProductPreviewPage(deProductPreview);
    userService = deProductPreview.injector.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(cProductPreview).toBeTruthy();
  });
  it('should hide when product input property is undefined', () => {
    expect(page.root).toBeNull();
  });
  it('should binding product', () => {

    cTestZone.product = product;
    fixture.detectChanges();
    expect(cProductPreview.product).toBe(product);
    expect(page.root).not.toBeNull();
    expect(page.nameEl.textContent.trim()).toBe(product.name);
    expect(page.costEl.textContent.trim()).toBe(product.cost.toString());
    expect(page.countEl.textContent.trim()).toBe(product.count.toString());
    expect(page.descriptionEl.textContent.trim()).toBe(product.description);
  });
  it('should show and hide controls', () => {
    cTestZone.product = product;
    fixture.detectChanges();
    expect(userService.isAdmin).toBeTruthy();
    expect(cProductPreview.isAdmin).toBeTruthy();
    expect(page.edit).not.toBeNull();
    expect(page.delete).not.toBeNull();
    userService.isAdmin = false;
    fixture.detectChanges();
    expect(cProductPreview.isAdmin).toBeFalsy();
    expect(page.edit).toBeNull();
    expect(page.delete).toBeNull();
  });
  it('should have clickable routerLink to product detail page', () => {
    const productUrl = `/products/${product.id}`;
    cTestZone.product = product;
    fixture.detectChanges();
    expect(page.name).not.toBeNull();
    expect(page.nameRouterLink.strLinkParam).toBe(productUrl);
    expect(page.nameRouterLink.navigatedTo).toBeUndefined();
    page.name.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(page.nameRouterLink.strNavigatedTo).toBe(productUrl);
  });
  it('should set current category url to appBackUrl directive', () => {
    const categoryUrl = '/category/1';
    cTestZone.product = product;
    fixture.detectChanges();
    expect(page.nameAppBackUrl.appBackUrl).toBeUndefined();
    cTestZone.categoryUrl = categoryUrl;
    fixture.detectChanges();
    expect(page.nameAppBackUrl.appBackUrl).toBe(categoryUrl);
  });
  it('should have clickable routerLink to edit product page for admin users', () => {
    const productEditUrl = `/products/${product.id}/edit`;
    cTestZone.product = product;
    fixture.detectChanges();
    expect(page.editRouterLink.strLinkParam).toBe(productEditUrl);
    expect(page.editRouterLink.navigatedTo).toBeUndefined();
    page.edit.triggerEventHandler('click', null);
    expect(page.editRouterLink.strNavigatedTo).toBe(productEditUrl);
  });
  it('should have clickable delete button for admin users', () => {
    cTestZone.product = product;
    fixture.detectChanges();
    expect(page.delete).not.toBeNull();
    const spyDelete = spyOn(cProductPreview, 'delete');
    expect(spyDelete).not.toHaveBeenCalled();
    page.delete.triggerEventHandler('click', null);
    expect(spyDelete).toHaveBeenCalled();
  });
});

class ProductPreviewPage extends TestPage {
  readonly selectors = {
    root: '.product-preview',
    anchorName: '.name',
    cost: '.cost',
    count: '.count',
    description: '.description',
    edit: '.edit',
    delete: '.delete'
  };
  get root() { return this.queryByCss(this.selectors.root); }
  get name() { return this.queryByCss(this.selectors.anchorName); }
  get nameEl() { return this.name.nativeElement as HTMLAnchorElement; }
  get nameAppBackUrl() { return this.name.injector.get(BackUrlStubDirective); }
  get costEl() { return this.queryByCss(this.selectors.cost).nativeElement as HTMLElement; }
  get countEl() { return this.queryByCss(this.selectors.count).nativeElement as HTMLElement; }
  get descriptionEl() { return this.queryByCss(this.selectors.description).nativeElement as HTMLElement; }
  get edit() { return this.queryByCss(this.selectors.edit); }
  get editBtn() { return this.edit.query(By.css('button')).nativeElement as HTMLButtonElement; }
  get delete() { return this.queryByCss(this.selectors.delete); }
  get deleteEl() { return this.delete.nativeElement as HTMLButtonElement; }
  get nameRouterLink() { return this.name.injector.get(RouterLinkStubDirective); }
  get editRouterLink() { return this.edit.injector.get(RouterLinkStubDirective); }
}
