import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CategoryListComponent} from './category-list/category-list.component';
import {CategoryResolverService} from './category-resolver.service';
import {RootCategoriesResolverService} from './root-categories-resolver.service';
import {ProductsWithoutCategoryResolverService} from './products-without-category-resolver.service';
import {CategoriesEditorComponent} from './categories-editor/categories-editor.component';

const routes: Routes = [
  {
    path: 'categories',
    children: [
      {
        path: 'edit',
        component: CategoriesEditorComponent,
        resolve: {
          categoryTree: RootCategoriesResolverService
        }
      },
      {
        path: 'other', component: CategoryListComponent,
        resolve: {
          products: ProductsWithoutCategoryResolverService
        }
      },
      {
        path: ':id',
        component: CategoryListComponent,
        resolve: {
          categoryBag: CategoryResolverService
        }
      },
      {
        path: '',
        component: CategoryListComponent,
        resolve: {
          categoryTree: RootCategoriesResolverService
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
