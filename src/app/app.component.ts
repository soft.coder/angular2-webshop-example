import {Component, OnInit} from '@angular/core';
import {UserService} from './common/app-services/user.service';
import {BreadcrumbsService} from './common/app-services/breadcrumbs.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit {
  get breadcrumbs() {
    return this.breadcrumbsService.breadcrumbs;
  }
  get isLogin (): boolean {
    return this.userService.isLogin;
  }
  constructor(private userService: UserService, private breadcrumbsService: BreadcrumbsService) {
  }

  ngOnInit() {
  }
}
