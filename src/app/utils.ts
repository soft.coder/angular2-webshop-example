import {concat, Observable, of, throwError} from 'rxjs';
import {delay} from 'rxjs/operators';

export function randomRange(max: number, min: number = 0) {
  return Math.floor(Math.random() * max) + min;
}
export function deepCopy(obj: any) {
  return JSON.parse(JSON.stringify(obj));
}
export function wrapObservable<T>(val: T, error?: Error, timeoutRange = {min: 1000, max: 3000}): Observable<T> {
  return new Observable(subscriber => {
    const timeoutId = setTimeout(() => {
      if (error) {
        subscriber.error(error);
      } else {
        subscriber.next(val);
      }
    }, randomRange(randomRange(timeoutRange.max, timeoutRange.min)));
    return () => {
      clearTimeout(timeoutId);
    };
  });
}
