import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NotFoundComponent} from './not-found/not-found.component';
import {RouterModule} from '@angular/router';

const appRoutes = [
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {path: '', pathMatch: 'full', redirectTo: 'categories'},
  {path: '**', component: NotFoundComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, {enableTracing: false})
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
