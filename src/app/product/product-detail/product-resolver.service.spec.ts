import { TestBed, inject } from '@angular/core/testing';

import { ProductResolverService } from './product-resolver.service';
import {ProductService} from './product.service';
import {Observable} from 'rxjs';
import {Product} from './product';
import {of} from 'rxjs/internal/observable/of';
import {RouterStubService} from '../../common/tests/router/router-stub.service';
import {Router} from '@angular/router';
import {LogService} from '../../common/app-services/log.service';

describe('ProductResolverService', () => {
  beforeEach(() => {
    const productServiceStub: Partial<ProductService> = {
      get(id: number): Observable<Product> {
        return of(new Product('Test product', 'description', 100, 1, id));
      }
    };
    const logSpy = jasmine.createSpyObj(['error', 'log']);
    TestBed.configureTestingModule({
      providers: [
        ProductResolverService,
        {provide: ProductService, useValue: productServiceStub},
        {provide: Router, useValue: RouterStubService},
        {provide: LogService, useValue: logSpy}
      ]
    });
  });

  it('should be created', inject([ProductResolverService], (service: ProductResolverService) => {
    expect(service).toBeTruthy();
  }));
});
