import {Category} from '../../categories/classes/category';

export class Product {
  constructor(public name: string,
              public description: string,
              public cost: number,
              public count: number,
              readonly id?: number,
              public categories?: Category[]) { }
}
