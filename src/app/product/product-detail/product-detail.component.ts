import {Component, OnInit, Input} from '@angular/core';
import {Product} from './product';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductService} from './product.service';
import {DialogService} from '../../common/app-services/dialog.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {DataService} from '../../common/app-services/data.service';
import {getErrorMsg} from '../../common/functions';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  @Input()
  product?: Product;
  @Input()
  isPreview: boolean;
  productForm: FormGroup;

  isAddNew: boolean;
  isEdit: boolean;
  isPending: boolean;
  serverErrorMsg: string;

  get isAdmin() {
    return true; // @TODO after add admin users
  }
  get name() { return this.productForm.get('name'); }
  get description() { return this.productForm.get('description'); }
  get cost() { return this.productForm.get('cost'); }
  get count() { return this.productForm.get('count'); }
  constructor(private fb: FormBuilder,
              private productService: ProductService,
              private dialog: DialogService,
              private locationService: Location,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe({
      next: (data: {add?: boolean, edit?: boolean, product?: Product}) => {
        if (data.add) {
          this.initNewProduct();
        } else if (data.edit) {
          this.isEdit = true;
        }
        if (data.product) {
          this.product = data.product;
        }
        this.initProductForm();
      }
    });
  }


  private initNewProduct() {
    this.isAddNew = true;
    this.product = new Product('', '', 0, 0);
    this.isEdit = true;
  }
  private initProductForm() {
    this.productForm = this.fb.group({
      name: [this.product.name, [Validators.required, Validators.minLength(3)]],
      description: [this.product.description],
      cost: [this.product.cost, [Validators.required, Validators.pattern('(-)?\\d+'), Validators.min(0)]],
      count: [this.product.count, [Validators.required, Validators.pattern('(-)?\\d+'), Validators.min(0)]]
    });
  }
  edit() {
    this.router.navigate(['edit']);
  }

  delete() {
    if (this.dialog.confirm('Are you really want to delete this product')) {
      this.isPending = true;
      this.productService.delete(this.product.id).subscribe(
        () => {
          const backUrl = this.dataService.get<string>('backUrl');
          if (backUrl) {
            this.router.navigateByUrl(backUrl);
          } else {
            this.router.navigateByUrl('/');
          }
        },
        (err: Error) => {
          this.isPending = false;
          this.serverErrorMsg = 'Unexpected errors when delete product';
          return this.dialog.alert(err.message);
        });
    }
  }
  save() {
    const {name, description, cost, count} = this.productForm.value;
    this.isPending = true;
    this.productService.save(name, description, cost, count).subscribe(
      (product: Product) => {
        this.product = product;
        this.router.navigate(['..'], {
          relativeTo: this.activatedRoute
        });
      },
      (error: Error) => {
        this.isPending = false;
        this.serverErrorMsg = error.message;
      }
    );
  }
  cancel() {
    if (!this.isAddNew) {
      this.isPending = true;
      this.router.navigate(['..'], {
        relativeTo: this.activatedRoute
      });
    } else {
      const backUrl = this.dataService.get<string>('backUrl');
      if (backUrl) {
        this.router.navigateByUrl(backUrl);
      } else {
        this.router.navigateByUrl('/');
      }
    }
  }

  update() {
    const {name, description, cost, count} = this.productForm.value;
    this.isPending = true;
    this.productService.update(new Product(name, description, cost, count, this.product.id)).subscribe(
      (product: Product) => {
        this.product = product;
        this.locationService.back();
      },
      (error: Error) => {
        this.serverErrorMsg = error.message;
        this.isPending = false;
      }
    );
  }

  addNewProduct() {
    this.router.navigateByUrl('products/add');
    const currentUrl = this.router.url;
    this.dataService.set<string>('backUrl', currentUrl);
  }

  getNameErrorMsg() {
    return getErrorMsg(this.name.errors, {
      'required': 'Required',
      'minlength': () => {
        const minLength = this.name.errors.minlength.requiredLength;
        return `Min length of "name" field ${minLength} symbols`;
      }
    });
  }
  getCostErrorMsg() {
    return this.getNumberErrorMsg(this.cost);
  }
  getCountErrorMsg() {
    return this.getNumberErrorMsg(this.count);
  }

  private getNumberErrorMsg(control: AbstractControl) {
    return getErrorMsg(control.errors, {
      'required': 'Required',
      'pattern': 'Must be digit',
      'min': 'Must be positive number'
    });
  }
}
