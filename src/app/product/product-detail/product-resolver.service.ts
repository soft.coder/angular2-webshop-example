import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Product} from './product';
import {EMPTY, Observable, throwError} from 'rxjs';
import {ProductService} from './product.service';
import {catchError, take} from 'rxjs/operators';
import {LogService} from '../../common/app-services/log.service';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product> {

  constructor(private productService: ProductService, private router: Router, private log: LogService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> {
    const id = parseInt(route.paramMap.get('id'), 10);
    if (!isNaN(id) && id > 0) {
      return this.productService.get(id).pipe(
        take(1),
        catchError(error => {
          this.log.error(`Not found product id: ${id}`, error);
          this.router.navigateByUrl('not-found');
          return throwError(error);
        })
      );
    } else {
      this.router.navigateByUrl('not-found');
      return EMPTY;
    }
  }
}
