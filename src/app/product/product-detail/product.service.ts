import { Injectable } from '@angular/core';
import {Product} from './product';
import {forkJoin, Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from '../../api-url.service';
import {Category} from '../../categories/classes/category';
import {CategoryProduct} from '../../categories/category-product';
import {switchMap} from 'rxjs/operators';

@Injectable()
export class ProductService {
  products: Array<Product>;

  constructor(private http: HttpClient, private url: ApiUrlService) {
  }
  getList(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url.products);
  }
  get(id: number): Observable<Product> {
    return forkJoin(
      this.http.get<Product>(`${this.url.products}/${id}`),
      this.http.get<CategoryProduct[]>(`${this.url.categoryProduct}?productId=^${id}$`)
    ).pipe(
      switchMap(([product, categoryProducts]) => {
        if (categoryProducts.length) {
          const categories$: Array<Observable<Category>> = categoryProducts.map(cp => {
            return this.http.get<Category>(`${this.url.categories}/${cp.categoryId}`);
          });
          return forkJoin(categories$).pipe(
            switchMap((categories: Category[]) => {
              product.categories = categories;
              return of(product);
            })
          );
        } else {
          return of(product);
        }

      })
    );
  }
  update(product: Product): Observable<Product> {
    return this.http.put<Product>(`${this.url.products}/${product.id}`, product);
  }
  save(name: string, description: string, cost: number, count: number): Observable<Product> {
    return this.http.post<Product>(`${this.url.products}`, new Product(name, description, cost, count));
  }
  delete(id: number) {
    return forkJoin(
      this.http.delete<null>(`${this.url.products}/${id}`),
      this.http.get<CategoryProduct[]>(`${this.url.categoryProduct}?productId=${id}`)
        .pipe(switchMap((categoryProduct: CategoryProduct[]) => {
          const deletes$ = categoryProduct.map(cp => {
            return this.http.delete<null>(`${this.url.categoryProduct}/${cp.id}`);
          });
          return forkJoin(deletes$);
        })
      )
    ).pipe(switchMap(() => of(null)));
  }
}
