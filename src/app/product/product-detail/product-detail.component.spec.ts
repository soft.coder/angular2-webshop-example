import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ProductDetailComponent} from './product-detail.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ProductService} from './product.service';
import {Product} from './product';
import {DialogService} from '../../common/app-services/dialog.service';
import {DialogStubService} from '../../common/tests/dialog-stub.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedRouteStubService} from '../../common/tests/router/activated-route-stub.service';
import {RouterStubService} from '../../common/tests/router/router-stub.service';
import {DataService} from '../../common/app-services/data.service';
import {UppercaseFirstLetterStubPipe} from '../../common/tests/uppercase-first-letter-stub.pipe';
import {RouterLinkStubDirective} from '../../common/tests/router/router-link-stub.directive';
import {asyncData, asyncError, inputText, PageSelectors, TestPage} from '../../common/tests/common.spec';
import {Category} from '../../categories/classes/category';
import {Component, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {CurrencyStub} from '../../common/tests/currency-stub.pipe';
import SpyObj = jasmine.SpyObj;
import Spy = jasmine.Spy;

describe('ProductDetailComponent', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-test-zone-component',
    template: '<app-product-detail [product]="product" [isPreview]="isPreview"></app-product-detail>'
  })
  class TestZoneComponent {
    product: Product;
    isPreview: boolean;
  }
  interface ActivatedRouteData {
    add?: boolean;
    edit?: boolean;
    product?: Product;
  }
  let cTestZone: TestZoneComponent;
  let cProductDetail: ProductDetailComponent;
  let deProductDetail: DebugElement;
  let fixture: ComponentFixture<TestZoneComponent>;
  let activatedRoute: ActivatedRouteStubService<ActivatedRouteData>;
  let pageView: ProductDetailViewPage;
  let pageEdit: ProductDetailEditPage;
  const testCategories = [new Category('category1', 1), new Category('category2', 2)];
  const testProduct = new Product('name', 'description',
    100, 10, 1, testCategories);

  beforeEach(async(() => {
    const productServiceStub: Partial<ProductService> = {
      delete() {
        return asyncData(undefined);
      },
      save(name: string, description: string, cost: number, count: number) {
        return asyncData(new Product(name, description, cost, count, 1));
      },
      update(product: Product) {
        return asyncData(product);
      }
    };
    const locationServiceSpy = jasmine.createSpyObj(['back']);
    const dataServiceSpy = jasmine.createSpyObj(['set', 'get', 'delete']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      providers: [
        {provide: ProductService, useValue: productServiceStub},
        {provide: DialogService, useClass: DialogStubService},
        {provide: Location, useValue: locationServiceSpy },
        {provide: ActivatedRoute, useClass: ActivatedRouteStubService},
        {provide: Router, useClass: RouterStubService},
        {provide: DataService, useValue: dataServiceSpy}
      ],
      declarations: [ TestZoneComponent, ProductDetailComponent, UppercaseFirstLetterStubPipe, RouterLinkStubDirective, CurrencyStub ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestZoneComponent);
    cTestZone = fixture.componentInstance;
    deProductDetail = fixture.debugElement.query(By.directive(ProductDetailComponent));
    cProductDetail = deProductDetail.injector.get(ProductDetailComponent);
    pageView = new ProductDetailViewPage(deProductDetail);
    pageEdit = new ProductDetailEditPage(deProductDetail);
    activatedRoute = deProductDetail.injector.get(ActivatedRoute);
  });

  it('should create', () => {
    expect(cTestZone).toBeTruthy();
  });
  function shouldShowPage(data: ActivatedRouteData, pageShow: ProductDetailPage, pageHide: ProductDetailPage) {
    activatedRoute.data = asyncData(data);
    fixture.detectChanges();
    expect(pageShow.root).toBeNull();
    expect(pageHide.root).toBeNull();
    tick();
    fixture.detectChanges();
    expect(pageShow.root).not.toBeNull();
    expect(pageHide.root).toBeNull();
  }
  it('should show view page', fakeAsync(() => {
    const data: ActivatedRouteData = {product: testProduct};
    shouldShowPage(data, pageView, pageEdit);
  }));
  it('should show edit page', fakeAsync(() => {
    const data: ActivatedRouteData = {edit: true, product: testProduct};
    shouldShowPage(data, pageEdit, pageView);
  }));
  it('should show add page', fakeAsync(() => {
    const data: ActivatedRouteData = {add: true};
    shouldShowPage(data, pageEdit, pageView);
  }));
  describe('view mode', () => {
    beforeEach(fakeAsync(() => {
      const data: ActivatedRouteData = {product: testProduct};
      activatedRoute.data = asyncData(data);
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
    }));
    it('should binding', (() => {
      expect(pageView.root).toBeTruthy();
      expect(pageView.nameEl.textContent.trim()).toBe(testProduct.name);
      expect(pageView.costEl.textContent.trim()).toBe(testProduct.cost.toString());
      expect(pageView.countEl.textContent.trim()).toBe(testProduct.count.toString());
      expect(pageView.descriptionEl.textContent.trim()).toBe(testProduct.description);
      expect(pageView.categories).not.toBeNull();
      expect(pageView.categories.length).toBe(testCategories.length);
    }));
    it('should binding categories to clickable dom element with routerLink directive', () => {
      const deCategories = pageView.categories;
      deCategories.forEach(deCategory => {
        const elCategory: HTMLElement = deCategory.nativeElement;
        const testCategory = testCategories.find(tc => tc.name === elCategory.textContent.trim());
        expect(testCategory).toBeDefined('test category exists in DOM');
        const categoryUrl = `/categories/${testCategory.id}`;
        const routerLinkStub = deCategory.injector.get(RouterLinkStubDirective);
        const linkParamUrl = routerLinkStub.strLinkParam;
        expect(linkParamUrl).toBe(categoryUrl);
        expect(routerLinkStub.navigatedTo).toBeUndefined();
        deCategory.triggerEventHandler('click', null);
        fixture.detectChanges();
        const navigatedToUrl = routerLinkStub.strNavigatedTo;
        expect(navigatedToUrl).toBe(categoryUrl);
      });
    });
    describe('buttons', () => {
      let routerStub: RouterStubService;
      let dataServiceSpy: SpyObj<DataService>;
      beforeEach(() => {
        routerStub = TestBed.get(Router);
        dataServiceSpy = TestBed.get(DataService);
      });
      it('should add new product', () => {
        expect(pageView.add).not.toBeNull();
        expect(routerStub.navigatedUrl).toBeUndefined();
        expect(dataServiceSpy.set).not.toHaveBeenCalled();

        routerStub.url = 'current-url';
        pageView.clickAdd();
        expect(routerStub.navigatedUrl).toBe('products/add');
        expect(dataServiceSpy.set).toHaveBeenCalledWith('backUrl', routerStub.url);
      });
      it('should start edit current product', () => {
        const editUrl = 'edit';
        expect(pageView.editRouterLink.strLinkParam).toBe(editUrl);
        expect(pageView.editRouterLink.navigatedTo).toBeUndefined();
        pageView.clickEdit();
        expect(pageView.editRouterLink.strNavigatedTo).toBe(editUrl);
      });
      describe('delete', () => {
        let dialogServiceStub: DialogStubService, productServiceStub: Partial<ProductService>;
        let spyConfirm: Spy, spyAlert: Spy, spyServiceDelete: Spy, spyComponentDelete: Spy;
        beforeEach(() => {
          dialogServiceStub = deProductDetail.injector.get(DialogService) as DialogStubService;
          productServiceStub = deProductDetail.injector.get(ProductService) as Partial<ProductService>;
          spyConfirm = spyOn(dialogServiceStub, 'confirm').and.callThrough();
          spyAlert = spyOn(dialogServiceStub, 'alert').and.callThrough();
          spyServiceDelete = spyOn(productServiceStub, 'delete').and.callThrough();
          spyComponentDelete = spyOn(cProductDetail, 'delete').and.callThrough();
        });
        it('should have correct initial state', () => {
          expect(spyConfirm).not.toHaveBeenCalled();
          expect(spyAlert).not.toHaveBeenCalled();
          expect(spyServiceDelete).not.toHaveBeenCalled();
          expect(spyComponentDelete).not.toHaveBeenCalled();
          expect(cProductDetail.isPending).toBeFalsy();
          expect(cProductDetail.serverErrorMsg).toBeUndefined();
        });
        it('should clickable and request confirm', () => {
          dialogServiceStub.isConfirmed = false;
          pageView.clickDelete();
          expect(spyComponentDelete).toHaveBeenCalled();
          expect(spyConfirm).toHaveBeenCalled();
        });
        it('should prevent deleting when user click on cancel button in confirm dialog', () => {
          dialogServiceStub.isConfirmed = false;
          pageView.clickDelete();
          expect(cProductDetail.isPending).toBeFalsy();
          expect(spyServiceDelete).not.toHaveBeenCalled();
        });
        it('should handle response of server with error result', fakeAsync(() => {
          const error = new Error('error msg');
          spyServiceDelete.and.stub().and.returnValue(asyncError(error));
          dialogServiceStub.isConfirmed = true;
          pageView.clickDelete();
          expect(cProductDetail.isPending).toBeTruthy();
          expect(spyServiceDelete).toHaveBeenCalled();
          expect(cProductDetail.serverErrorMsg).toBeUndefined();
          tick();
          expect(cProductDetail.isPending).toBeFalsy();
          expect(cProductDetail.serverErrorMsg).toBeDefined();
          expect(spyAlert).toHaveBeenCalledWith(error.message);
        }));
        function setupForDeleteAction(backUrl?: string) {
          dialogServiceStub.isConfirmed = true;
          spyServiceDelete.and.stub().and.returnValue(asyncData(undefined));
          if (backUrl) {
            dataServiceSpy.get.and.stub().and.returnValue(backUrl);
          }
          pageView.clickDelete();
          expect(spyComponentDelete).toHaveBeenCalled();
          expect(spyConfirm).toHaveBeenCalled();
          expect(cProductDetail.isPending).toBeTruthy();
          expect(spyServiceDelete).toHaveBeenCalled();
          expect(cProductDetail.serverErrorMsg).toBeUndefined();
          tick();
          expect(cProductDetail.isPending).toBeTruthy();
          expect(cProductDetail.serverErrorMsg).toBeUndefined();
          expect(dataServiceSpy.get).toHaveBeenCalledWith('backUrl');
        }
        it('should go to main page, when backUrl empty', fakeAsync(() => {
          setupForDeleteAction();
          expect(routerStub.navigatedUrl).toBe('/');
        }));
        it('should go to backUrl', fakeAsync(() => {
          const backUrl = 'back-url';
          setupForDeleteAction(backUrl);
          expect(routerStub.navigatedUrl).toBe(backUrl);
        }));
      });
    });

  });
  describe('edit and add new product', () => {
    beforeEach(fakeAsync(() => {
      const data: ActivatedRouteData = {edit: true, product: testProduct};
      activatedRoute.data = asyncData(data);
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
    }));
    it('should have correct initial state', () => {
      expect(pageEdit.cancel).not.toBeNull();
      expect(cProductDetail.serverErrorMsg).toBeUndefined();
      for (const fieldKey in pageEdit.inputFields) {
        if (pageEdit.inputFields.hasOwnProperty(fieldKey)) {
          const formControl = pageEdit.inputFields[fieldKey].formControl;
          expect(formControl.pristine).toBeTruthy();
        }
      }
    });
    it('should binding', () => {
      const values = {
        name: 'changed-name',
        description: 'changed-description',
        cost: '123',
        count: '2'
      };
      const inputs = pageEdit.inputFields;
      for (const key in inputs) {
        if (inputs.hasOwnProperty(key)) {
          const {domElement, formControl} = inputs[key];
          inputText(domElement, values[key]);
          expect(formControl.value).toBe(values[key], `input#${key}`);
        }
      }
    });
    it('should handle required errors', () => {
      const requiredKeys = ['name', 'cost', 'count'];
      const inputs = pageEdit.inputFields;
      const errorKey = 'required', errorMsg = 'Required';
      for (const key of requiredKeys) {
        pageEdit.shouldNotShowError(key, errorKey, errorMsg);
        inputText(inputs[key].domElement, '');
        fixture.detectChanges();
        pageEdit.shouldShowError(key, errorKey, errorMsg);
      }
    });
    it('should handle positive number errors', () => {
      const positiveKeys = ['cost', 'count'];
      const inputs = pageEdit.inputFields;
      const errorKey = 'min', errorMsg = 'Must be positive number';
      for (const key of positiveKeys) {
        pageEdit.shouldNotShowError(key, errorKey, errorMsg);
        inputText(inputs[key].domElement, '-1');
        fixture.detectChanges();
        pageEdit.shouldShowError(key, errorKey, errorMsg);
      }
    });
    it('should have clickable cancel button', () => {
      const cancelSpy = spyOn(cProductDetail, 'cancel').and.stub();
      pageEdit.cancelClick();
      expect(cancelSpy).toHaveBeenCalled();
    });
    it('should handle min length error', () => {
      const errorKey = 'minlength', errorMsg = 'Min length', fieldKey = 'name';
      pageEdit.shouldNotShowError(fieldKey, errorKey, errorMsg);
      inputText(pageEdit.nameEl, 'a');
      fixture.detectChanges();
      pageEdit.shouldShowError(fieldKey, errorKey, errorMsg);
    });
    it('should handle must be digit erros', () => {
      const errorKey = 'pattern', errorMsg = 'Must be digit';
      const digitInputs = ['cost', 'count'];
      const inputs = pageEdit.inputFields;
      for (const key of digitInputs) {
        pageEdit.shouldNotShowError(key, errorKey, errorMsg);
        inputText(inputs[key].domElement, 'fds');
        fixture.detectChanges();
        pageEdit.shouldShowError(key, errorKey, errorMsg);
      }
    });
  });
  describe('edit mode', () => {
    beforeEach(fakeAsync(() => {
      const data: ActivatedRouteData = {edit: true, product: testProduct};
      activatedRoute.data = asyncData(data);
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
    }));
    it('should have correct initial state', () => {
      expect(pageEdit.nameEl.value).toBe(testProduct.name);
      expect(pageEdit.descriptionEl.value).toBe(testProduct.description);
      expect(pageEdit.costEl.value).toBe(testProduct.cost.toString());
      expect(pageEdit.countEl.value).toBe(testProduct.count.toString());
      expect(pageEdit.update).not.toBeNull();
      expect(pageEdit.cancel).not.toBeNull();
      expect(pageEdit.save).toBeNull();
    });
    describe('update button', () => {
      let productServiceStub, locationSpy, updateSpy;
      beforeEach(() => {
        productServiceStub = deProductDetail.injector.get(ProductService) as Partial<ProductService>;
        locationSpy = deProductDetail.injector.get(Location) as SpyObj<Location>;
        updateSpy = spyOn(productServiceStub, 'update').and.callThrough();
      });
      it('should have correct initial state', () => {
        expect(cProductDetail.isPending).toBeFalsy();
        expect(updateSpy).not.toHaveBeenCalled();
        expect(locationSpy.back).not.toHaveBeenCalled();
      });
      it('should disable update button when form invalid', () => {
        expect(cProductDetail.productForm.valid).toBeTruthy();
        inputText(pageEdit.nameEl, '');
        fixture.detectChanges();
        expect(cProductDetail.productForm.invalid).toBeTruthy();
        expect(pageEdit.updateEl.disabled).toBeTruthy();
      });
      it('should handle update action', fakeAsync(() => {
        pageEdit.updateClick();
        expect(cProductDetail.isPending).toBeTruthy();
        expect(updateSpy).toHaveBeenCalled();
        expect(locationSpy.back).not.toHaveBeenCalled();
        fixture.detectChanges();
        expect(pageEdit.fieldsetEl.disabled).toBeTruthy();
        tick();
        expect(locationSpy.back).toHaveBeenCalled();
      }));
      it('should handle server error msg', fakeAsync(() => {
        const error = new Error('test-server-error');
        updateSpy.and.stub().and.returnValue(asyncError(error));
        pageEdit.updateClick();
        expect(updateSpy).toHaveBeenCalled();
        fixture.detectChanges();
        tick();
        expect(locationSpy.back).not.toHaveBeenCalled();
        expect(cProductDetail.serverErrorMsg).toBe(error.message);
        expect(cProductDetail.isPending).toBeFalsy();
      }));
    });
    it('should handle cancel button', () => {
      // @ts-ignore
      const routerStub = deProductDetail.injector.get(Router) as RouterStubService;
      const activatedRouteStub = deProductDetail.injector.get(ActivatedRoute);
      expect(cProductDetail.isPending).toBeFalsy();
      expect(routerStub.navigatedCommands).toBeUndefined();
      pageEdit.cancelClick();
      expect(cProductDetail.isPending).toBeTruthy();
      expect(routerStub.navigatedCommands[0]).toBe('..');
      expect(routerStub.extras.relativeTo).toBe(activatedRouteStub);
    });
  });
  describe('add mode', () => {
    beforeEach(fakeAsync(() => {
      const data: ActivatedRouteData = {add: true};
      activatedRoute.data = asyncData(data);
      fixture.detectChanges();
      tick();
      fixture.detectChanges();
    }));
    it('should have correct initial state', () => {
      expect(pageEdit.nameEl.value).toBe('');
      expect(pageEdit.descriptionEl.value).toBe('');
      expect(pageEdit.costEl.value).toBe('0');
      expect(pageEdit.countEl.value).toBe('0');
      expect(pageEdit.update).toBeNull();
      expect(pageEdit.save).not.toBeNull();
    });
    describe('save button', () => {
      function inputForm(product: Product) {
        inputText(pageEdit.nameEl, product.name);
        inputText(pageEdit.descriptionEl, product.description);
        inputText(pageEdit.costEl, product.cost.toString());
        inputText(pageEdit.countEl, product.count.toString());
      }
      let saveSpy: Spy, productServiceStub: Partial<ProductService>;
      beforeEach(() => {
        productServiceStub = deProductDetail.injector.get(ProductService) as Partial<ProductService>;
        saveSpy = spyOn(productServiceStub, 'save').and.callThrough();
      });
      it('should disable when form invalid', () => {
        expect(cProductDetail.productForm.invalid).toBeTruthy();
        expect(pageEdit.saveEl.disabled).toBeTruthy();
      });
      it('should handle success response', fakeAsync(() => {
        // @ts-ignore
        const routerStub = deProductDetail.injector.get(Router) as RouterStubService;
        const activatedRouteStub  = deProductDetail.injector.get(ActivatedRoute);
        inputForm(testProduct);
        fixture.detectChanges();
        expect(cProductDetail.productForm.valid).toBeTruthy();
        expect(saveSpy).not.toHaveBeenCalled();
        pageEdit.saveClick();
        expect(cProductDetail.isPending).toBeTruthy();
        expect(saveSpy).toHaveBeenCalled();
        expect(routerStub.navigatedCommands).toBeUndefined();
        tick();
        expect(routerStub.navigatedCommands[0]).toBe('..');
        expect(routerStub.extras.relativeTo).toBe(activatedRouteStub);
      }));
      it('should handle error response', fakeAsync(() => {
        const serverError = new Error('test-server-error');
        saveSpy.and.stub().and.returnValue(asyncError(serverError));
        inputForm(testProduct);
        fixture.detectChanges();
        pageEdit.saveClick();
        tick();
        expect(cProductDetail.isPending).toBeFalsy();
        expect(cProductDetail.serverErrorMsg).toBe(serverError.message);
      }));
    });
    describe('cancel button', () => {
      let dataServiceSpy: SpyObj<DataService>, routerStub: RouterStubService;
      beforeEach(() => {
        dataServiceSpy = deProductDetail.injector.get(DataService) as SpyObj<DataService>;
        // @ts-ignore
        routerStub = deProductDetail.injector.get(Router) as RouterStubService;
      });
      it('should go to root when back url empty', () => {
        expect(dataServiceSpy.get).not.toHaveBeenCalled();
        expect(routerStub.navigatedUrl).toBeUndefined();
        pageEdit.cancelClick();
        expect(dataServiceSpy.get).toHaveBeenCalled();
        expect(routerStub.navigatedUrl).toBe('/');
      });
      it('should go to back url', () => {
        const backUrl = 'test-back-url';
        dataServiceSpy.get.and.returnValue(backUrl);
        expect(dataServiceSpy.get).not.toHaveBeenCalled();
        expect(routerStub.navigatedUrl).toBeUndefined();
        pageEdit.cancelClick();
        expect(dataServiceSpy.get).toHaveBeenCalled();
        expect(routerStub.navigatedUrl).toBe(backUrl);
      });
    });
  });
});
abstract class ProductDetailPage extends TestPage {
  abstract selectors: PageSelectors;
  get root() { return this.queryByCss(this.selectors.root); }
}

class ProductDetailViewPage extends ProductDetailPage {
  readonly selectors = {
    root: '.product-detail--view',
    name: '.name',
    cost: '.cost',
    count: '.count',
    category: '.category',
    description: '.description',
    add: '.btn-add',
    edit: '.a-edit',
    delete: '.btn-delete'
  };
  get nameEl() { return this.queryByCss(this.selectors.name).nativeElement as HTMLElement; }
  get costEl() { return this.queryByCss(this.selectors.cost).nativeElement as HTMLElement; }
  get countEl() { return this.queryByCss(this.selectors.count).nativeElement as HTMLElement; }
  get descriptionEl() { return this.queryByCss(this.selectors.description).nativeElement as HTMLElement; }
  get categories() { return this.queryAllByCss(this.selectors.category); }
  get add() { return this.queryByCss(this.selectors.add); }
  get edit() { return this.queryByCss(this.selectors.edit); }
  get editRouterLink() { return this.edit.injector.get(RouterLinkStubDirective); }
  get delete() { return this.queryByCss(this.selectors.delete); }
  clickAdd() { return this.add.triggerEventHandler('click', null); }
  clickEdit() { return this.edit.triggerEventHandler('click', null); }
  clickDelete() { return this.delete.triggerEventHandler('click', null); }
}
class ProductDetailEditPage extends ProductDetailPage {
  readonly selectors = {
    root: '.product-detail--edit',
    fieldset: 'fieldset',
    inputs: {
      name: '#name',
      description: '#description',
      cost: '#cost',
      count: '#count',
    },
    update: '.btn-update',
    save: '.btn-save',
    cancel: '.btn-cancel'
  };
  get nameEl() { return this.queryByCss(this.selectors.inputs.name).nativeElement as HTMLInputElement; }
  get descriptionEl() { return this.queryByCss(this.selectors.inputs.description).nativeElement as HTMLTextAreaElement; }
  get costEl() { return this.queryByCss(this.selectors.inputs.cost).nativeElement as HTMLInputElement; }
  get countEl() { return this.queryByCss(this.selectors.inputs.count).nativeElement as HTMLInputElement; }
  get update() { return this.queryByCss(this.selectors.update); }
  get updateEl() { return this.update.nativeElement as HTMLButtonElement; }
  get save() { return this.queryByCss(this.selectors.save); }
  get saveEl() { return this.save.nativeElement as HTMLButtonElement; }
  get cancel() { return this.queryByCss(this.selectors.cancel); }
  get fieldsetEl() { return this.queryByCss(this.selectors.fieldset).nativeElement as HTMLFieldSetElement; }
  constructor(protected debugElement: DebugElement) {
    super(debugElement);
    const component = debugElement.injector.get(ProductDetailComponent);
    this.initInputFields(this.selectors.inputs, component);
  }
  cancelClick(): void {
    return this.click(this.cancel);
  }
  updateClick(): void {
    return this.click(this.update);
  }
  saveClick(): void {
    return this.click(this.save);
  }
}
