import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {ProductResolverService} from './product-detail/product-resolver.service';

const routes: Routes = [
  {
    path: 'products',
    children: [
      {
        path: 'add',
        component: ProductDetailComponent,
        data: {add: true}
      },
      {
        path: ':id',
        component: ProductDetailComponent,
        resolve: {
          product: ProductResolverService
        }
      },
      {
        path: ':id/edit',
        component: ProductDetailComponent,
        data: {edit: true},
        resolve: {
          product: ProductResolverService
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
