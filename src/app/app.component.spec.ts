import {TestBed, async, ComponentFixture, fakeAsync, tick} from '@angular/core/testing';
import { AppComponent } from './app.component';
import {Component, DebugElement, Input} from '@angular/core';
import {UserService} from './common/app-services/user.service';
import {BreadcrumbsService} from './common/app-services/breadcrumbs.service';
import {Breadcrumb} from './common/app-widgets/breadcrumbs/breadcrumb';
import {CommonModule} from '@angular/common';
import {By} from '@angular/platform-browser';
import {RouterLinkStubDirective} from './common/tests/router/router-link-stub.directive';
import {RouterLinkActiveStubDirective} from './common/tests/router/router-link-active-stub.directive';
import {BreadcrumbsComponent} from './common/app-widgets/breadcrumbs/breadcrumbs.component';

describe('AppComponent', () => {
  let component: AppComponent, fixture: ComponentFixture<AppComponent>;
  let userService: Partial<UserService>, breadcrumbService: Partial<BreadcrumbsService> ;
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    template: '',
    selector: 'app-breadcrumbs'
  })
  class BreadcrumbsComponentStub {
    @Input()
    breadcrumbs: Breadcrumb;
  }

  beforeEach(async(() => {
    const userServiceStub: Partial<UserService> = {
      isLogin: true,
    };
    const breadcrumbsServiceStub: Partial<BreadcrumbsService> = {
      breadcrumbs: [new Breadcrumb('url', 'name')]
    };
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      template: '',
      // tslint:disable-next-line
      selector: 'router-outlet'
    })
    class RouterOutletStub { }
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [
        AppComponent,
        BreadcrumbsComponentStub,
        RouterOutletStub,
        RouterLinkStubDirective,
        RouterLinkActiveStubDirective
      ],
      providers: [
        {provide: UserService, useValue: userServiceStub},
        {provide: BreadcrumbsService, useValue: breadcrumbsServiceStub}
      ],
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    breadcrumbService = TestBed.get(BreadcrumbsService);
  });
  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));
  describe('class', () => {
    it('should return breadcrumbs of BreadcrumbService', () => {
      expect(component.breadcrumbs).toBe(breadcrumbService.breadcrumbs);
    });
    it('should return isLogin of UserService', () => {
      expect(component.isLogin).toBe(userService.isLogin);
      userService.isLogin = !userService.isLogin;
      fixture.detectChanges();
      expect(component.isLogin).toBe(userService.isLogin, 'component.isLogin not changed after changes in userService');
    });
  });
  describe('template', () => {
    describe('routerLinks', () => {
      const alwaysVisibleLinks = [
        'categories'
      ];
      const guestLinks = [
        'profile/login',
        'profile/register'
      ];
      function findLink(url) {
        const routerLinkDebugElements = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective));
        return routerLinkDebugElements.find((de: DebugElement) => {
            const routerLink = de.injector.get(RouterLinkStubDirective);
            if ( routerLink.linkParam && (routerLink.linkParam === url ||
              (routerLink.linkParam[0] === url && routerLink.linkParam.length === 1))) {
              return true;
            }
          }
        );
      }
      function isLinkExist(url, ) {
        const debugEl = findLink(url);
        const routerLink = debugEl ? debugEl.injector.get(RouterLinkStubDirective) : undefined;
        expect(debugEl).toBeDefined(`not found router link`);
        if (routerLink) {
          expect(routerLink.navigatedTo).toBeUndefined('not clicked yet');
          debugEl.triggerEventHandler('click', {});
          expect(routerLink.navigatedTo).toBe(routerLink.linkParam, 'link should be clickable');
        }

      }
      for (const linkUrl of alwaysVisibleLinks) {
        it(`should contain clickable router link to '${linkUrl}'`, () => {
          fixture.detectChanges();
          isLinkExist(linkUrl);
        });
      }
      for (const linkUrl of guestLinks) {
        it(`should contain clickable router link to '${linkUrl}' for guest user`, () => {
          userService.isLogin = false;
          fixture.detectChanges();
          isLinkExist(linkUrl);
        });
        it(`should not contain clickable router link to '${linkUrl}' for logged user`, () => {
          userService.isLogin = true;
          fixture.detectChanges();
          expect(findLink(linkUrl)).toBeUndefined();
        });

      }
    });
    it('should set breadcrumbs', () => {
      fixture.detectChanges();
      const breadcrumbDe = fixture.debugElement.query(By.directive(BreadcrumbsComponentStub));
      expect(breadcrumbDe).toBeDefined('not found breadcrumbsComponent by selector');
      const breadcrumbStub = breadcrumbDe.componentInstance;
      expect(breadcrumbStub).toBeDefined();
      if (breadcrumbStub) {
        expect(breadcrumbStub.breadcrumbs).toBe(breadcrumbService.breadcrumbs, 'not set breadcrumbs');
      }
    });
  });
});
