import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsernameValidatorFnService} from '../username-validator-fn.service';
import {passwordRepeatFnValidator} from '../password-repeat-validator-fn';
import {UserService} from '../../common/app-services/user.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  readonly usernameMinLength =  3;
  registerForm: FormGroup;
  public isPending: boolean;
  public serverErrorMsg: string;
  get username() {
    return this.registerForm.get('username');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get passwordRepeat() {
    return this.registerForm.get('passwordRepeat');
  }

  constructor(private fb: FormBuilder,
              private usernameValidator: UsernameValidatorFnService,
              private userService: UserService,
              private locationService: Location) {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(this.usernameMinLength)], usernameValidator.getValidatorFn()],
      password: ['', Validators.required],
      passwordRepeat: ['', Validators.required]
    }, {validators: passwordRepeatFnValidator, validator: passwordRepeatFnValidator});
  }

  ngOnInit() {
  }

  register(username: string, password: string) {
    this.isPending = true;
    this.userService.register(username, password).subscribe(
      () => {
        this.isPending = false;
        this.locationService.back();
      },
      (err: Error) => {
        this.isPending = false;
        this.serverErrorMsg = err.message;
      });
  }

  back() {
    this.locationService.back();
  }
  getUsernameError(control: AbstractControl) {
    if (control.errors) {
      if (control.errors['required']) {
        return 'Required';
      } else if ( control.errors['minlength']) {
        return `Min length of username ${control.errors.minlength.requiredLength} symbol`;
      } else if ( control.errors['userExists']) {
        return 'User exists';
      }
    }
  }
  getPasswordError(control: AbstractControl) {
    if (control.errors && control.errors['required']) {
      return 'Required';
    }
  }
  getPasswordRepeatError(control: AbstractControl) {
    if (control.errors && control.errors['required']) {
      return 'Required';
    } else if (this.registerForm.errors && this.registerForm.errors['passwordRepeat']) {
      return 'Password repeat mismatch';
    }
  }
}
