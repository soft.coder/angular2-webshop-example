import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {UserService} from '../../common/app-services/user.service';
import {Location} from '@angular/common';
import {DebugElement} from '@angular/core';
import SpyObj = jasmine.SpyObj;
import {By} from '@angular/platform-browser';
import {asyncData, asyncError, FieldData, generateText, inputText} from '../../common/tests/common.spec';
import {UsernameValidatorFnService} from '../username-validator-fn.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let userServiceSpy: SpyObj<UserService>, locationSpy: SpyObj<Location>, usernameValidator: SpyObj<UsernameValidatorFnService>;
  // let usernameInput: HTMLInputElement, passwordInput: HTMLInputElement, passwordRepeatInput: HTMLInputElement;
  let registerBtn: HTMLButtonElement, registerDe: DebugElement, backBtn: HTMLButtonElement, backDe: DebugElement;
  let fieldset: HTMLFieldSetElement;
  const inputFields: {[key: string]: FieldData<HTMLInputElement>} = {};
  const username = 'username', password = 'password', passwordRepeat = 'passwordRepeat';
  const inputsName = [username, password, passwordRepeat];
  const requiredInputsName = [username, password, passwordRepeat];
  function shouldShowError(name: string, errorKey: string, errorMsg: string, formGroupErrors?: {}) {
    const {domElement, formControl, invalidFeedback} = inputFields[name];
    const errors = formGroupErrors || formControl.errors;
    expect(errors && errors[errorKey]).toBeTruthy(`field have ${errorKey} error`);
    expect(domElement.classList.contains('is-invalid')).toBeTruthy(`field show "${errorKey}" error`);
    expect(invalidFeedback.classList.contains('show')).toBeTruthy(`.invalid-feedback has class show`);
    expect(invalidFeedback.textContent).toMatch(new RegExp(errorMsg, 'i'), `.invalid-feedback have "${errorMsg}" msg`);
  }
  function shouldNotShowError(name: string, errorKey: string, errorMsg: string, formGroupErrors?: {}) {
    const {formControl, invalidFeedback} = inputFields[name];
    const errors = formGroupErrors || formControl.errors;
    expect(errors && errors[errorKey]).toBeFalsy(`formControl dont have "${errorKey}" error`);
    expect(invalidFeedback.textContent).not
      .toMatch(new RegExp(errorMsg, 'i'), `.invalid-feedback field should have "${errorMsg}" msg`);
  }

  beforeEach(async(() => {
    const locationStub = jasmine.createSpyObj('Location', ['back']);
    const userServiceStub = jasmine.createSpyObj('UserService', ['register']);
    const usernameValidatorStub = jasmine.createSpyObj('UsernameValidatorFnService', ['getValidatorFn']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ RegisterComponent ],
      providers: [
        FormBuilder,
        {provide: UserService, useValue: userServiceStub},
        {provide: Location, useValue: locationStub},
        {provide: UsernameValidatorFnService, useValue: usernameValidatorStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    userServiceSpy = fixture.debugElement.injector.get(UserService) as SpyObj<UserService>;
    locationSpy = fixture.debugElement.injector.get(Location) as SpyObj<Location>;
    usernameValidator = fixture.debugElement.injector.get(UsernameValidatorFnService) as SpyObj<UsernameValidatorFnService>;
    usernameValidator.getValidatorFn.and.returnValue(() => asyncData(true));
    registerDe = fixture.debugElement.query(By.css('input[type="submit"]'));
    registerBtn = registerDe.nativeElement;
    backDe = fixture.debugElement.query(By.css('#back'));
    backBtn = backDe.nativeElement;
    fieldset = fixture.debugElement.query(By.css('fieldset')).nativeElement;
    for (const name of inputsName) {
      const debugElement = fixture.debugElement;
      const inputElement = debugElement.query(By.css(`#${name}`)).nativeElement as HTMLInputElement;
      const invalidFeedback = inputElement.parentElement.querySelector('.invalid-feedback') as HTMLInputElement;
      inputFields[name] = new FieldData(inputElement, component[name], debugElement, invalidFeedback) ;
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should initial empty, pristine, and hidden .invalid-feedback', () => {
    for (const name in inputFields) {
      if (inputFields.hasOwnProperty(name)) {
        const field = inputFields[name];
        expect(field.domElement.value).toBeFalsy(`empty ${name}`);
        expect(field.formControl.pristine).toBeTruthy(`pristine ${name}`);
        expect(field.invalidFeedback).toBeTruthy(`.invalid-feedback exists in dom for ${name}`);
      }
    }
  });
  it('should binding', () => {
    for (const name in inputFields) {
      if (inputFields.hasOwnProperty(name)) {
        const {domElement, formControl} = inputFields[name];
        const testValue = `test${name}`;
        inputText(domElement, testValue);
        expect(formControl.value).toBe(testValue, name);
      }
    }
  });
  describe('should show required error', () => {
    const errorKey = 'required', errorMsg = 'required';
    const shouldShowRequiredError = (name) => shouldShowError(name, errorKey, errorMsg);
    const shouldNotShowRequiredError = (name) => shouldNotShowError(name, errorKey, errorMsg);
    for (const name of requiredInputsName) {
      it(`for "${name}" input field`, () => {
        expect(inputFields.hasOwnProperty(name)).toBeTruthy('inputsFields array have that field');
        const {domElement, formControl, invalidFeedback} = inputFields[name];
        const classList = domElement.classList;
        expect(formControl.errors.required).toBeTruthy('initial formControl have required error');
        expect(invalidFeedback.classList.contains('show')).toBeTruthy('initial .invalid-feedback has class .show');
        expect(classList.contains('is-invalid')).toBeFalsy('initial error message is hidden');
        inputText(domElement, 'test');
        fixture.detectChanges();
        shouldNotShowRequiredError(name);
        inputText(domElement, '');
        fixture.detectChanges();
        shouldShowRequiredError(name);
        inputText(domElement, 'test1');
        fixture.detectChanges();
        shouldNotShowRequiredError(name);
      });
    }
  });
  it('should validate password repeat', () => {
    const errorKey = 'passwordRepeat', errorMsg = 'password repeat';
    inputText(inputFields[password].domElement, 'testPassword');
    inputText(inputFields[passwordRepeat].domElement, 'invalidPasswordRepeat');
    fixture.detectChanges();
    expect(component.registerForm.errors).toBeTruthy('expected form error');
    shouldShowError(passwordRepeat, errorKey, errorMsg, component.registerForm.errors);
    inputText(inputFields[passwordRepeat].domElement, 'testPassword');
    fixture.detectChanges();
    shouldNotShowError(passwordRepeat, errorKey, errorMsg, component.registerForm.errors);
  });
  it('should show minLength error for "username" field', () => {
    const errorKey = 'minlength', errorMsg = 'min length';
    const shouldShowMinLengthError = () => shouldShowError(username, errorKey, errorMsg);
    const shouldNotShowMinLengthError = () => shouldNotShowError(username, errorKey, errorMsg);
    const {formControl, domElement} = inputFields[username];
    shouldNotShowMinLengthError();
    inputText(domElement, 't');
    fixture.detectChanges();
    shouldShowMinLengthError();
    const requiredLength = formControl.errors.minlength.requiredLength;
    for (let i = 2; i < requiredLength; i++) {
      const text = generateText(i);
      inputText(domElement, text);
      shouldShowMinLengthError();
    }
    inputText(domElement, generateText(requiredLength));
    fixture.detectChanges();
    shouldNotShowMinLengthError();
  });
  it('should disable register button when omit filling required input field', () => {
    if (requiredInputsName.length) {
      expect(registerBtn.disabled).toBeTruthy('initially disabled register button');
    }
    for (let i = 0; i < requiredInputsName.length; i++) {
      const field = inputFields[requiredInputsName[i]];
      expect(field.formControl.errors.required).toBeTruthy(`formControl required error of ${inputsName[i]}`);
      inputText(field.domElement, 'testData');
      fixture.detectChanges();
      if (i + 1 < requiredInputsName.length) {
        const msg = requiredInputsName.slice(i + 1).join(', ') + ' not filled';
        expect(registerBtn.disabled).toBeTruthy(msg);
      } else {
        expect(registerBtn.disabled).toBeFalsy('all required data filled');
      }
    }
  });
  it('should handle back button', () => {
    expect(locationSpy.back).not.toHaveBeenCalled();
    backDe.triggerEventHandler('click', null);
    expect(locationSpy.back).toHaveBeenCalled();
  });
  it('should handle register button', fakeAsync( () => {
    function checkFormStatus(isPending) {
      const msg = isPending ? 'in process' : 'complete';
      expect(component.isPending).toBe(isPending, `component.isPending ${msg}`);
      fixture.detectChanges();
      expect(fieldset.disabled).toBe(isPending, `fieldset.disabled, ${msg}`);
    }
    expect(userServiceSpy.register).not.toHaveBeenCalled();
    const testUsername = 'testUsername', testPassword = 'testPassword';
    const usernameElement = inputFields[username].domElement;
    const passwordElement = inputFields[password].domElement;
    const passwordRepeatElement = inputFields[passwordRepeat].domElement;
    inputText(usernameElement, testUsername);
    inputText(passwordElement, testPassword);
    inputText(passwordRepeatElement, testPassword);
    fixture.detectChanges();
    const testErrorMsg = 'testErrorMsg';
    userServiceSpy.register.and.returnValue(asyncError(new Error(testErrorMsg)));
    registerDe.triggerEventHandler('click', null);
    checkFormStatus(true);
    tick();
    checkFormStatus(false);
    expect(locationSpy.back).not.toHaveBeenCalled();
    expect(component.serverErrorMsg).toBe(testErrorMsg);
    fixture.detectChanges();
    const serverError: HTMLElement = fixture.debugElement.query(By.css('.error-server')).nativeElement;
    expect(serverError.textContent).toBe(testErrorMsg);
    userServiceSpy.register.and.returnValue(asyncData(undefined));
    registerDe.triggerEventHandler('click', null);
    checkFormStatus(true);
    expect(locationSpy.back).not.toHaveBeenCalled();
    tick();
    checkFormStatus(false);
    expect(locationSpy.back).toHaveBeenCalled();
  }));

});
