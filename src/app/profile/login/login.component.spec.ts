import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {UserService} from '../../common/app-services/user.service';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {ReactiveFormsModule} from '@angular/forms';
import {Location} from '@angular/common';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {User} from '../user';
import SpyObj = jasmine.SpyObj;
import {asyncData, asyncError, newEvent} from '../../common/tests/common.spec';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userService: SpyObj<UserService>, locationService: SpyObj<Location>;
  let usernameInput: HTMLInputElement;
  let passwordInput: HTMLInputElement;
  let submitDe: DebugElement;
  let submitBtn: HTMLInputElement;
  let backDe: DebugElement;
  const credentials = {
    username: 'TestUser',
    password: 'TestPassword'
  };

  beforeEach(async(() => {
    const locationSpy = jasmine.createSpyObj('location', ['back']);
    const userServiceSpy = jasmine.createSpyObj('userService', ['login']);
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ LoginComponent ],
      providers: [
        {provide: UserService, useValue: userServiceSpy},
        {provide: Location, useValue: locationSpy}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService) as SpyObj<UserService>;
    locationService = TestBed.get(Location) as SpyObj<Location>;
    usernameInput = fixture.debugElement.query(By.css('#username')).nativeElement;
    passwordInput = fixture.debugElement.query(By.css('#password')).nativeElement;
    submitDe = fixture.debugElement.query(By.css('input[type="submit"]'));
    submitBtn = submitDe.nativeElement;
    backDe = fixture.debugElement.query(By.css('input.btn-back'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should binding', () => {
    expect(usernameInput.value).toBeFalsy('empty username input field');
    expect(passwordInput.value).toBeFalsy('empty password input field');
    usernameInput.value = 'testName';
    usernameInput.dispatchEvent(newEvent('input'));
    // fixture.detectChanges();
    expect(component.username.value).toBe(usernameInput.value, 'change username');
    passwordInput.value = 'testPassword';
    passwordInput.dispatchEvent(newEvent('input'));
    // fixture.detectChanges();
    expect(component.password.value).toBe(passwordInput.value, 'change password');
  });
  it('should submit button disabled', () => {
    expect(submitBtn.disabled).toBeTruthy('submit button disabled when username and password field is empty')
    usernameInput.value = 'testName';
    usernameInput.dispatchEvent(newEvent('input'));
    fixture.detectChanges();
    expect(submitBtn.disabled).toBeTruthy('submit button disabled when password field is empty');
    usernameInput.value = '';
    passwordInput.value = 'testPassword';
    usernameInput.dispatchEvent(newEvent('input'));
    passwordInput.dispatchEvent(newEvent('input'));
    fixture.detectChanges();
    expect(submitBtn.disabled).toBeTruthy('submit button disabled when username field is empty');
  });
  function setupCredentialsAndSubmit(username, password, returnValue) {
    usernameInput.value = username;
    passwordInput.value = password;
    usernameInput.dispatchEvent(newEvent('input'));
    passwordInput.dispatchEvent(newEvent('input'));
    userService.login.and.returnValue(returnValue);
    submitDe.triggerEventHandler('click', {});
  }
  it('should login', fakeAsync(() => {
    setupCredentialsAndSubmit(credentials.username, credentials.password, asyncData(true));
    expect(userService.login).toHaveBeenCalledWith(credentials.username, credentials.password);
    expect(component.isPending).toBeTruthy('pending');
    expect(userService.login).toHaveBeenCalled();
    expect(locationService.back).not.toHaveBeenCalled();
    tick();
    expect(locationService.back).toHaveBeenCalled();
  }));
  it('should not login', fakeAsync(() => {
    const username = 'invalidUsername', password = 'invalidPassword';
    setupCredentialsAndSubmit(username, password, asyncData(false));
    expect(userService.login).toHaveBeenCalledWith(username, password);
    expect(component.isPending).toBeTruthy('pending');
    tick();
    expect(component.isPending).toBeFalsy('pending complete');
    expect(component.serverErrorMsg).toBe(component.invalidUsernameOrPasswordMsg);
    expect(locationService.back).not.toHaveBeenCalled();
  }));
  it('should handle server error', fakeAsync(() => {
    const serverError = new Error('server error msg');
    const password = 'serverErrorPassword', username = 'serverErrorUsername';
    setupCredentialsAndSubmit(username, password, asyncError(serverError));
    expect(userService.login).toHaveBeenCalledWith(username, password);
    expect(component.isPending).toBeTruthy('pending');
    tick();
    expect(component.isPending).toBeFalsy('pending complete');
    expect(component.serverErrorMsg).toBe(serverError.message, 'expected server error');
    expect(locationService.back).not.toHaveBeenCalled();
  }));
  it('should back on previous page', () => {
    expect(locationService.back).not.toHaveBeenCalled();
    backDe.triggerEventHandler('click', null);
    expect(locationService.back).toHaveBeenCalled();
  });
});
