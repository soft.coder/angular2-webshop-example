import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../common/app-services/user.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  readonly invalidUsernameOrPasswordMsg = 'Invalid username or password';
  userForm: FormGroup;
  isPending: boolean;
  serverErrorMsg: string;
  get username() {
    return this.userForm.get('username');
  }
  get password() {
    return this.userForm.get('password');
  }

  constructor(private userService: UserService, private locationService: Location) {
    this.userForm = new FormGroup({
      username: new FormControl(),
      password: new FormControl()
    });
  }

  ngOnInit() {
  }

  login(username: string, password: string) {
    this.isPending = true;
    this.userService.login(username, password).subscribe(
      (isLogin) => {
        if (isLogin) {
          this.locationService.back();
        } else {
          this.isPending = false;
          this.serverErrorMsg = this.invalidUsernameOrPasswordMsg;
        }
      },
      (error: Error) => {
        this.isPending = false;
        this.serverErrorMsg = error.message;
      }
    );
  }

  back() {
    this.locationService.back();
  }
}
