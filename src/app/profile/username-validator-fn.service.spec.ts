import { TestBed, inject } from '@angular/core/testing';

import { UsernameValidatorFnService } from './username-validator-fn.service';
import {UserService} from '../common/app-services/user.service';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';

describe('UsernameValidatorFnService', () => {
  const existedUsername = 'TestUser';
  beforeEach(() => {
    const userServiceStub: Partial<UserService> = {
      userExists(name: string): Observable<boolean> {
        if (name === existedUsername) {
          return of(true);
        } else {
          return of(false);
        }
      }
    };
    TestBed.configureTestingModule({
      providers: [
        UsernameValidatorFnService,
        {provide: UserService, useValue: userServiceStub}
      ]
    });
  });

  it('should be created', inject([UsernameValidatorFnService], (service: UsernameValidatorFnService) => {
    expect(service).toBeTruthy();
  }));
});
