import { Injectable } from '@angular/core';
import {UserService} from '../common/app-services/user.service';
import {AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsernameValidatorFnService {

  constructor(private userService: UserService) { }

  getValidatorFn(): AsyncValidatorFn {
    return (control) => {
      return this.userService.userExists(control.value).pipe(
        map((val: boolean): ValidationErrors | null => {
          return val ? {userExists: true} : null;
        })
      );
    };
  }
}
