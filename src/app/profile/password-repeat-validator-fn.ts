import {ValidatorFn} from '@angular/forms';

export const passwordRepeatFnValidator: ValidatorFn = control => {
  const password = control.get('password');
  const passwordRepeat = control.get('passwordRepeat');
  return password.value === passwordRepeat.value ? null : {'passwordRepeat': true};
};
