import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiUrlService {
  public readonly categories = 'api/categories';
  public readonly products = 'api/products';
  public readonly categoryProduct = 'api/category-product';
  constructor() { }
}
