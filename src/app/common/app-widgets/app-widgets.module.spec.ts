import { AppWidgetsModule } from './app-widgets.module';

describe('AppWidgetsModule', () => {
  let appWidgetsModule: AppWidgetsModule;

  beforeEach(() => {
    appWidgetsModule = new AppWidgetsModule();
  });

  it('should create an instance', () => {
    expect(appWidgetsModule).toBeTruthy();
  });
});
