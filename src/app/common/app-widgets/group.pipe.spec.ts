import {GroupPipe} from './group.pipe';
import {errorExpectations} from '../tests/common.spec';

describe('GroupPipe', () => {
  function generateArray(count) {
    const array = new Array(count);
    for (let i = 0; i < count; i++) {
      array[i] = i + 1;
    }
    return array;
  }
  function checkGroups(countsValue: number[], countInGroup: number = 3) {
    let count;
    function errorMsg(msg) {
      return `count values: ${count}, count in group: ${countInGroup} - ${msg}`;
    }
    for (count of countsValue) {
      const values = generateArray(count);
      const countGroups = Math.ceil(count / countInGroup);
      const countInLastGroup = countGroups === 1 && count === countInGroup ? countInGroup : count % countInGroup;
      const groups = pipe.transform(values, countInGroup);
      expect(groups.length).toBe(countGroups, errorMsg('invalid count groups'));
      for (let i = 0; i < countGroups; i++) {
        if (i + 1 < countGroups) {
          expect(groups[i].length).toBe(countInGroup, errorMsg(`count elements in group:${i}`));
        } else {
          expect(groups[i].length).toBe(countInLastGroup, errorMsg(`count elements in group:${i}`));
        }
      }
    }
  }
  let pipe: GroupPipe;
  beforeEach(() => {
    pipe = new GroupPipe();
  });
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('undefined', () => {
    expect(pipe.transform(undefined)).toBe(undefined, 'undefined');
  });
  it('should errors', () => {
    const transform  = pipe.transform.bind(pipe);
    const invalidValuesArgs = [[1], ['1423'], [{}], [function() {}]];
    errorExpectations(pipe.valueTypeErrorMsg, TypeError, transform, invalidValuesArgs);
    const values = generateArray(3);
    const invalidTypeCountArgs = [[values, '123'], [values, {}], [values, []], [values, function() {}]];
    errorExpectations(pipe.countTypeErrorMsg, TypeError, transform, invalidTypeCountArgs);
    const invalidRangeCountArgs = [[values, 1], [values, 0], [values, -3], [values, NaN]];
    errorExpectations(pipe.countRangeErrorMsg, RangeError, transform, invalidRangeCountArgs);
  });
  it('transform', () => {
    checkGroups([1, 2, 3, 5, 7], 3);
    checkGroups([1, 2, 3, 4, 5, 7], 4);
  });
});
