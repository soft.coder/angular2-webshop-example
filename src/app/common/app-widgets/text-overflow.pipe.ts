import { Pipe, PipeTransform } from '@angular/core';
import {error} from 'selenium-webdriver';
import InvalidArgumentError = error.InvalidArgumentError;
import {tryParse} from 'selenium-webdriver/http';

@Pipe({
  name: 'textOverflow'
})
export class TextOverflowPipe implements PipeTransform {
  readonly valueTypeErrorMsg = 'textOverflowPipe: value must be type of number';
  readonly countTypeErrorMsg = 'textOverflowPipe: count must be type of number';
  readonly countRangeErrorMsg = 'textOverflowPipe: count must be larger 3';
  transform(value: any, count: any = 100): string {
    const end = '...';
    value = value === undefined ? '' : value;
    count = count === undefined ? 100 : count;
    if (typeof value !== 'string') {
      throw new TypeError(this.valueTypeErrorMsg);
    }
    if (typeof count !== 'number') {
      throw new TypeError(this.countTypeErrorMsg);
    }
    if (count <= end.length) {
      throw new RangeError(this.countRangeErrorMsg);
    }
    if (value && value.length > count) {
        return value.substring(0, count - end.length) + end;
    } else {
      return value;
    }
  }
}
