import { UppercaseFirstLetterPipe } from './uppercase-first-letter.pipe';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {By} from '@angular/platform-browser';
import {getTypeName} from '../tests/common.spec';

describe('UppercaseFirstLetterPipe', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-test-pipe',
    template: `<span>{{text | uppercaseFirstLetter}}</span>`
  })
  class TestPipe {
    text: any;
  }
  let fixture: ComponentFixture<TestPipe>, component: TestPipe, span: HTMLSpanElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestPipe, UppercaseFirstLetterPipe]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(TestPipe);
    component = fixture.componentInstance;
    span = fixture.debugElement.query(By.css('span')).nativeElement;
  });

  it('two word', () => {
    component.text = 'first letter';
    fixture.detectChanges();
    expect(span.textContent).toBe('First letter');
  });
  it('one word', () => {
    component.text = 'letter';
    fixture.detectChanges();
    expect(span.textContent).toBe('Letter');
  });
  it('empty string', () => {
    component.text = '';
    fixture.detectChanges();
    expect(span.textContent).toBe('');
  });
  it('undefined', () => {
    component.text = undefined;
    fixture.detectChanges();
    expect(span.textContent).toBe('');
  });
  it('throw TypeError for other types', () => {
    const pipe = new UppercaseFirstLetterPipe();
    const types = [[], {}, 1, null];
    for (const typeItem of types) {
      component.text = typeItem;
      try {
        fixture.detectChanges();
      } catch (error) {
        const typeInfo: string = getTypeName(typeItem);
        expect(error instanceof TypeError).toBeTruthy(`expected TypeError for ${typeInfo}`);
        expect(error.message).toBe(pipe.valueTypeErrorMsg, `expected another error msg for ${typeInfo}`);        continue;
      }
      fail('expected TypeError');
    }
  });
});
