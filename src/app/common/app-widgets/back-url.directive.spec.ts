import { BackUrlDirective } from './back-url.directive';
import {DataService} from '../app-services/data.service';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Component, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {errorExpectations, getTypeName} from '../tests/common.spec';
import {Data} from '@angular/router';
import SpyObj = jasmine.SpyObj;

describe('BackUrlDirective', () => {
  it('should create an instance', () => {
    const directive = new BackUrlDirective(new DataService());
    expect(directive).toBeTruthy();
  });
  describe('template', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-directive',
      template: '<a [appBackUrl]="url"></a>'
    })
    class TestDirective {
      url: any;
    }
    beforeEach(async(() => {
      const dataServiceSpy = jasmine.createSpyObj('dataService', ['set']);
      TestBed.configureTestingModule({

        declarations: [BackUrlDirective, TestDirective],
        providers: [
          {provide: DataService, useValue: dataServiceSpy}
        ]
      }).compileComponents();
    }));
    let fixture: ComponentFixture<TestDirective>, component: TestDirective;
    let debugElement: DebugElement, directive: BackUrlDirective
    beforeEach(() => {
      fixture = TestBed.createComponent(TestDirective);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement.query(By.directive(BackUrlDirective));
      directive = debugElement.injector.get(BackUrlDirective);
    });
    it('should binding', () => {
      const directiveClassInstance = new BackUrlDirective(undefined);
      const validValues = [undefined, 'test-url'];
      for (const value of validValues) {
        component.url = value;
        fixture.detectChanges();
        expect(directive.backUrl).toBe(component.url, getTypeName(value));
      }
      const invalidValues = [123, true, false];
      for (const value of invalidValues) {
        component.url = value;
        try {
          fixture.detectChanges();
        } catch (error) {
          expect(error instanceof TypeError).toBeTruthy(`TypeError for ${getTypeName(value)}`);
          expect(error.message).toBe(directiveClassInstance.backUrlTypeErrorMsg, 'error message');
          continue;
        }
        fail('expect TypeError');
      }
    });
    it('should handler click', () => {
      const dataService = debugElement.injector.get(DataService) as SpyObj<DataService>;
      fixture.detectChanges();
      debugElement.triggerEventHandler('click', undefined);
      expect(dataService.set.calls.any()).toBeFalsy('url undefined');
      const testUrl = 'test-url';
      component.url = testUrl;
      fixture.detectChanges();
      debugElement.triggerEventHandler('click', undefined);
      expect(dataService.set.calls.any()).toBeTruthy('url defined');
      expect(dataService.set.calls.mostRecent().args[1]).toBe(testUrl, 'saved correct url');
    });
  });
});
