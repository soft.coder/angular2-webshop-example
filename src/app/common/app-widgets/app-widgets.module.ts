import { NgModule } from '@angular/core';
import {TextOverflowPipe} from './text-overflow.pipe';
import {UppercaseFirstLetterPipe} from './uppercase-first-letter.pipe';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import { BackUrlDirective } from './back-url.directive';
import { GroupPipe } from './group.pipe';

@NgModule({
  imports: [
    RouterModule,
    CommonModule
  ],
  declarations: [
    BreadcrumbsComponent,
    TextOverflowPipe,
    UppercaseFirstLetterPipe,
    BackUrlDirective,
    GroupPipe
  ],
  exports: [
    BreadcrumbsComponent,
    TextOverflowPipe,
    UppercaseFirstLetterPipe,
    BackUrlDirective,
    GroupPipe
  ]
})
export class AppWidgetsModule { }
