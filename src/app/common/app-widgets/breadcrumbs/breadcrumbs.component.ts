import {Component, Input, OnInit} from '@angular/core';
import {Breadcrumb} from './breadcrumb';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.less'],
})
export class BreadcrumbsComponent implements OnInit {
  @Input()
  public breadcrumbs?: Breadcrumb[];

  constructor() { }

  ngOnInit() {
  }

}
