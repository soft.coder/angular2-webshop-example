import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreadcrumbsComponent } from './breadcrumbs.component';
import {RouterLinkStubDirective} from '../../tests/router/router-link-stub.directive';
import {RouterLinkActiveStubDirective} from '../../tests/router/router-link-active-stub.directive';
import {UppercaseFirstLetterStubPipe} from '../../tests/uppercase-first-letter-stub.pipe';
import {By} from '@angular/platform-browser';
import {Breadcrumb} from './breadcrumb';
import {Component} from '@angular/core';

describe('BreadcrumbsComponent', () => {
  let componentTestZone: ComponentTestZone;
  let componentBreadcrumbs: BreadcrumbsComponent;
  let fixtureTestZone: ComponentFixture<ComponentTestZone>;

  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-component-test-zone',
    template: '<app-breadcrumbs [breadcrumbs]="breadcrumbs"></app-breadcrumbs>'
  })
  class ComponentTestZone {
    breadcrumbs?: any;
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentTestZone, BreadcrumbsComponent,
        RouterLinkStubDirective, RouterLinkActiveStubDirective, UppercaseFirstLetterStubPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixtureTestZone = TestBed.createComponent(ComponentTestZone);
    componentTestZone = fixtureTestZone.componentInstance;
    const debugElement = fixtureTestZone.debugElement.query(By.directive(BreadcrumbsComponent));
    componentBreadcrumbs = debugElement.injector.get(BreadcrumbsComponent);
    fixtureTestZone.detectChanges();
  });

  it('should create', () => {
    expect(componentTestZone).toBeTruthy();
  });
  it('should handle undefined breadcrumbs', () => {
    expect(componentTestZone.breadcrumbs).toBeUndefined();
    expect(componentBreadcrumbs.breadcrumbs).toBeUndefined();
    expect(fixtureTestZone.debugElement.query(By.css('nav'))).toBeNull();
  });
  it('should set breadcrumbs', () => {
    const breadcrumbs = [
      new Breadcrumb('url-1', 'name-1'), new Breadcrumb('url-2', 'name-2')
    ];
    componentTestZone.breadcrumbs = breadcrumbs;
    fixtureTestZone.detectChanges();
    expect(componentBreadcrumbs.breadcrumbs.length).toBe(breadcrumbs.length, );
    const arrAnchor = fixtureTestZone.debugElement.queryAll(By.css('.breadcrumb-item > a'));
    expect(arrAnchor.length).toBe(breadcrumbs.length);
    breadcrumbs.forEach(b => {
      expect(arrAnchor.some(anchor => {
        const routerLink = anchor.injector.get(RouterLinkStubDirective);
        const htmlLi = anchor.nativeElement as HTMLLIElement;
        return htmlLi.textContent === b.name && routerLink.linkParam === b.url;
      })).toBeTruthy(`breadcrumb['${b.name}'] with url: '${b.url}' should have li element`);
    });
  });
});
