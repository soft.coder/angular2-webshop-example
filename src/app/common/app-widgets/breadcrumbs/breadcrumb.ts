export class Breadcrumb {
  constructor(
    public url: string,
    public name: string
  ) { }
}
