import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uppercaseFirstLetter'
})
export class UppercaseFirstLetterPipe implements PipeTransform {
  readonly valueTypeErrorMsg = 'upperCaseFirstLetterPipe: value must be type of string';
  transform(value: string | any, args?: any): any {
    value = value === undefined ? '' : value;
    if (typeof value !== 'string') {
      throw new TypeError(this.valueTypeErrorMsg);
    }
    if (value && value.length) {
      return value.substring(0, 1).toUpperCase() + value.substring(1);
    } else {
      return value;
    }
  }

}
