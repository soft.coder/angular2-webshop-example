import { TextOverflowPipe } from './text-overflow.pipe';
import {Component} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {loremIpsum} from 'lorem-ipsum/types/src';
import {Test} from 'tslint';
import {getTypeName} from '../tests/common.spec';

function generateText(length) {
  return (new Array(length + 1)).toString();
}

describe('TextOverflowPipe', () => {
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-test-pipe',
    template: `
      <span>{{text | textOverflow}}</span>
      <span>{{text | textOverflow:50}}</span>
      <span>{{text | textOverflow:4}}</span>
    `
  })
  class TestPipe {
    text: any;
  }
  // noinspection AngularMissingOrInvalidDeclarationInModule
  @Component({
    selector: 'app-test-pipe-errors',
    template: `
      <span>{{text | textOverflow:count}}</span>
    `
  })
  class TestPipeErrors {
    text: any;
    count: any = 1;
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextOverflowPipe, TestPipe, TestPipeErrors]
    }).compileComponents();
  }));
  describe('', () => {
    let fixture: ComponentFixture<TestPipe>, component: TestPipe, spans: HTMLSpanElement[];
    beforeEach(() => {
      fixture = TestBed.createComponent(TestPipe);
      component = fixture.componentInstance;
      spans = fixture.debugElement
        .queryAll(By.css('span'))
        .map(d => d.nativeElement);
    });
    it('create an instance', () => {
      const pipe = new TextOverflowPipe();
      expect(pipe).toBeTruthy();
    });
    it('undefined', () => {
      component.text = undefined;
      fixture.detectChanges();
      expect(spans[0].textContent).toBe('');
    });
    it('length > 100', () => {
      const text = generateText(110);
      component.text = text;
      fixture.detectChanges();
      expect(spans[0].textContent).toBe(text.substring(0, 100 - 3) + '...', 'default length: 100');
      expect(spans[1].textContent).toBe(text.substring(0, 50 - 3) + '...', 'custom length: 50');
      expect(spans[2].textContent).toBe(text.substring(0, 4 - 3) + '...', 'custom length: 4');
    });
    it('length === 80', () => {
      const text = generateText(80);
      component.text = text;
      fixture.detectChanges();
      expect(spans[0].textContent).toBe(text, 'text length less max');
      expect(spans[1].textContent).toBe(text.substring(0, 50 - 3) + '...', 'text length larger max: 50');
      expect(spans[2].textContent).toBe(text.substring(0, 4 - 3) + '...', 'text length larger max: 4');
    });
    it('throw TypeError for other types', () => {
      const pipe = new TextOverflowPipe();
      const types = [12345, {}, [1, 'a', '2', 3, 4], null];
      for (const typeItem of types) {
        component.text = typeItem;
        try {
          fixture.detectChanges();
        } catch (error) {
          const typeName = getTypeName(typeItem);
          expect(error instanceof TypeError).toBeTruthy(`expected TypeError for ${typeName}`);
          expect(error.message).toBe(pipe.valueTypeErrorMsg, `expected another error msg for ${typeName}`);
          continue;
        }
        fail('expected TypeError');
      }
    });
  });
  describe('filter exceptions', () => {
    let fixture: ComponentFixture<TestPipeErrors>, component: TestPipeErrors;
    beforeEach(() => {
      fixture = TestBed.createComponent(TestPipeErrors);
      component = fixture.componentInstance;
    });
    it('count range error', () => {
      const pipe = new TextOverflowPipe();
      component.text = 'text123';
      component.count = 3;
      let isThrowError = false;
      try {
        fixture.detectChanges();
      } catch (error) {
        isThrowError = true;
        expect(error instanceof RangeError).toBeTruthy();
        expect(error.message).toBe(pipe.countRangeErrorMsg, 'count param: range error');
      }
      expect(isThrowError).toBeTruthy('expected RangeError');
      component.count = 'some string';
      try {
        fixture.detectChanges();
      } catch (error) {
        expect(error instanceof TypeError).toBeTruthy();
        expect(error.message).toBe(pipe.countTypeErrorMsg, 'count param: range error');
        return;
      }
      fail('expected TypeError');
    });
  });
});
