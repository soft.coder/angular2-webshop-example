import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'group'
})
export class GroupPipe implements PipeTransform {
  readonly valueTypeErrorMsg = 'GroupPipe: value must be array';
  readonly countTypeErrorMsg = 'GroupPipe: count must be number';
  readonly countRangeErrorMsg = 'GroupPipe: count must be greater or equal than 2';

  transform(value: any, count: number = 3): Array<any[]> {
    const groups: Array<any[]> = [];
    if (value) {
      if (!(value instanceof Array)) {
        throw new TypeError(this.valueTypeErrorMsg);
      } else if (typeof count !== 'number') {
        throw new TypeError(this.countTypeErrorMsg);
      } else if (count < 2 || isNaN(count)) {
        throw new RangeError(this.countRangeErrorMsg);
      }
      for (let i = 0; i < Math.ceil(value.length / count); i++) {
        const start = i * count;
        groups.push(value.slice(start, start + count));
      }
      return groups;
    } else {
      return value;
    }
  }

}
