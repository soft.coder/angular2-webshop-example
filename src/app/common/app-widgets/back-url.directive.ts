import {Directive, HostListener, Input} from '@angular/core';
import {DataService} from '../app-services/data.service';

@Directive({
  selector: '[appBackUrl]'
})
export class BackUrlDirective {
  readonly backUrlTypeErrorMsg = 'BackUrlDirective: appBackUrl must be type of "string"';
  private _backUrl: string;
  @Input('appBackUrl')
  set backUrl(value) {
    if (typeof value === 'string' || value === undefined) {
      this._backUrl = value;
    } else {
      throw TypeError(this.backUrlTypeErrorMsg);
    }
  }
  get backUrl() {
    return this._backUrl;
  }
  @HostListener('click')
  click() {
    if (this.backUrl) {
      this.dataService.set<string>('backUrl', this.backUrl);
    }
  }

  constructor(private dataService: DataService) { }

}
