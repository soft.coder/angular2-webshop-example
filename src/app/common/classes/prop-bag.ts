export interface PropBag<T> {
  name: string;
  value: T;
}
