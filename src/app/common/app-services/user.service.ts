import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../profile/user';
import {map} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';
import {Observable} from 'rxjs';
import {AppServicesModule} from './app-services.module';

@Injectable({
  providedIn: AppServicesModule
})
export class UserService {
  public isLogin = false;
  public isAdmin = true;
  public currentUser: User;
  private readonly url = 'api/users';

  constructor(private http: HttpClient) { }
  public register(name: string, password: string): Observable<User> {
    return this.http.post<User>(this.url, new User(name, password)).pipe(
      tap((user: User) => this.currentUser = user));
  }
  public login(name: string, password: string) {
    return this.http.get<Array<User>>(`${this.url}?name=${name}`).pipe(
      map((users: Array<User>) => {
      if (users) {
        for (const user of users) {
          if (user.name === name && user.password === password) {
            this.isLogin = true;
            this.currentUser = user;
            break;
          }
        }
      }
      return this.isLogin;
     }));
  }
  public userExists(name: string) {
    return this.http.get<Array<User>>(`${this.url}?name=${name}`).pipe(
      map(users => {
        if (users) {
          let result = false;
          for (const user of users) {
            if (user.name === name) {
              result = true;
              break;
            }
          }
          return result;
        }
      }));
  }
}
