import { Injectable } from '@angular/core';
import {AppServicesModule} from './app-services.module';

@Injectable({
  providedIn: AppServicesModule
})
export class LogService {

  constructor() { }

  error(msg: string, ...args: Array<any>) {
    console.error(msg, ...args);
  }
  log(msg: string, ...args: Array<any>) {
    console.log(msg, ...args);
  }
}
