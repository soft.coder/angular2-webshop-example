import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {randomRange} from '../../utils';
import {Product} from '../../product/product-detail/product';
import {LoremIpsum} from 'lorem-ipsum';
import {User} from '../../profile/user';
import {AppServicesModule} from './app-services.module';
import {Category} from '../../categories/classes/category';
import {CategoryProduct} from '../../categories/category-product';
import {CategoryDB} from '../../categories/classes/category-db';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});

@Injectable({
  providedIn: AppServicesModule
})
export class InMemoryExampleService implements InMemoryDbService {
  private generateProducts(): Array<Product> {
    const count = randomRange(100, 50);
    const products = new Array<Product>();
    for (let i = 1; i < count; i++) {
      const productItem = new Product(
        `Product ${i}`,
        lorem.generateParagraphs(randomRange(5, 1)),
        randomRange(1000, 100),
        randomRange(25),
        i
      );
      products.push(productItem);
    }
    return products;
  }
  private generateUsers(): Array<User> {
    const count = randomRange(10, 25);
    const users = new Array<User>();
    for (let i = 1; i < count; i++) {
      const user = new User(
        lorem.generateWords(randomRange(1, 1), ),
        '123',
        i
      );
      users.push(user);
    }
    return users;
  }
  private generateCategories(): Array<CategoryDB> {
    const count = randomRange(10, 5);
    const categories = new Array<CategoryDB>();
    function generateParentId(parentId: number | null = null, deep = 0) {
      if (deep !== 0 && deep > randomRange(4, 0)) {
        return;
      }
      let countLevel: number;
      if (deep > 0) {
        countLevel = randomRange(4);
      } else {
        countLevel = randomRange(
          Math.ceil(availCategories.length / 3),
          Math.ceil(availCategories.length / 5)
        );
      }
      countLevel = countLevel <= availCategories.length ? countLevel : availCategories.length;
      for (let i = 0; i < countLevel && availCategories.length > 0; i++) {
        const index = randomRange(availCategories.length);
        const levelCategory: CategoryDB = availCategories.splice(index, 1)[0];
        levelCategory.parentId = parentId;
        generateParentId(levelCategory.id, deep + 1);
      }
    }
    for (let i = 1; i < count; i++) {
      categories.push(new CategoryDB(
        `category ${i}`,
        i,
      ));
    }
    const availCategories: CategoryDB[] = categories.map(c => c);
    generateParentId();
    const groupByParentId: {[key: string]: Array<CategoryDB>} = categories.reduce((group, category)  => {
      const propName = category.parentId ? category.parentId : undefined;
      if (!group[propName]) {
        group[propName] = [];
      }
      group[propName].push(category);
      return group;
    }, {});
    for (const parentId in groupByParentId) {
      if (groupByParentId.hasOwnProperty(parentId)) {
        groupByParentId[parentId].forEach((category, index, categoryGroup) => {
          if (index) {
            category.orderId = categoryGroup[index - 1].id;
          }
        });
      }
    }
    return categories;
  }
  private generateCategoryProductManyToMany(products: Product[], categories: CategoryDB[]): CategoryProduct[] {
    const countAttempt = categories.length * 100;
    const categoryProduct: CategoryProduct[] = [];
    let currentId = 1;
    for (let i = 0; i < products.length; i++) {
      let maxCat = Math.ceil(categories.length / 3);
      if (!maxCat) {
        maxCat = randomRange( 10 ) > 8 ? 0 : 1;
      }
      const countCategories = randomRange(maxCat);
      const productCategories: CategoryDB[] = [];
      for (let j = 0; j < countCategories; j++) {
        for (let k = 0; k < countAttempt; k++) {
          const category = categories[randomRange(categories.length - 1)];
          if (!productCategories[category.id]) {
            productCategories[category.id] = category;
            break;
          }
        }
      }
      productCategories.forEach((category: Category) => {
        categoryProduct.push(new CategoryProduct(currentId++, category.id, products[i].id));
      });
    }
    return categoryProduct;
  }

  constructor() { }

  createDb() {
    const products = this.generateProducts();
    const users = this.generateUsers();
    const categories: CategoryDB[] = this.generateCategories();
    const categoryProduct = this.generateCategoryProductManyToMany(products, categories);
    return { products, users, categories, 'category-product': categoryProduct};
  }


}
