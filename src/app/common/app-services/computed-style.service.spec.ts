import { TestBed, inject } from '@angular/core/testing';

import { ComputedStyleService } from './computed-style.service';

describe('ComputedStyleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComputedStyleService]
    });
  });

  it('should be created', inject([ComputedStyleService], (service: ComputedStyleService) => {
    expect(service).toBeTruthy();
  }));
});
