import { TestBed, inject } from '@angular/core/testing';

import { DragDataService } from './drag-data.service';

describe('DragDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DragDataService]
    });
  });

  it('should be created', inject([DragDataService], (service: DragDataService<string>) => {
    expect(service).toBeTruthy();
  }));
});
