import { Injectable } from '@angular/core';
import {timer} from 'rxjs';

@Injectable()
export class DragDataService <T> {
  private data: T;
  setData(data: T) {
    timer().subscribe(() => this.data = data);
  }
  getData(): T {
    return this.data;
  }

  constructor() { }
}
