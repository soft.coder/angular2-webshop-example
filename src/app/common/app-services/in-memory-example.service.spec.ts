import { TestBed, inject } from '@angular/core/testing';

import { InMemoryExampleService } from './in-memory-example.service';

describe('InMemoryExampleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InMemoryExampleService]
    });
  });

  it('should be created', inject([InMemoryExampleService], (service: InMemoryExampleService) => {
    expect(service).toBeTruthy();
  }));
});
