import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _data: Object = {};
  constructor() { }
  public set<T>(key: string, value: T): T {
    return this._data[key] = value;
  }
  public get<T>(key: string): T {
    return this._data[key];
  }
  public delete<T>(key: string): T {
    const deletedItem = this._data[key];
    delete this._data[key];
    return deletedItem;
  }
  public clear(): Object {
    const oldData = this._data;
    this._data = {};
    return oldData;
  }
}
