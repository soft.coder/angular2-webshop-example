import { Injectable } from '@angular/core';
import {Breadcrumb} from '../app-widgets/breadcrumbs/breadcrumb';
import {timer} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbsService {
  private _breadcrumbs: Breadcrumb[] = [];
  public set breadcrumbs(breadcrumbs: Breadcrumb[]) {
    timer().subscribe(() => this._breadcrumbs = breadcrumbs);
  }
  public get breadcrumbs() {
    return this._breadcrumbs;
  }
  constructor() { }
  push(breakcrumb: Breadcrumb) {
    this._breadcrumbs.push(breakcrumb);
  }
}
