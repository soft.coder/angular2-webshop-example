import { Injectable } from '@angular/core';
import {AppServicesModule} from './app-services.module';

@Injectable({
  providedIn: AppServicesModule
})
export class DialogService {

  constructor() { }
  alert(message: string) {
    return window.alert(message);
  }
  confirm(message: string): boolean {
    return window.confirm(message);
  }
}
