import {TestBed, inject, tick, fakeAsync} from '@angular/core/testing';

import { BreadcrumbsService } from './breadcrumbs.service';
import {Breadcrumb} from '../app-widgets/breadcrumbs/breadcrumb';

describe('BreadcrumbsService', () => {
  let breadcrumbsService: BreadcrumbsService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BreadcrumbsService]
    });
    breadcrumbsService = TestBed.get(BreadcrumbsService);
  });

  it('should be created', inject([BreadcrumbsService], (service: BreadcrumbsService) => {
    expect(service).toBeTruthy();
  }));
  it('should empty array', () => {
    expect(breadcrumbsService.breadcrumbs).toBeTruthy('array exist');
    expect(breadcrumbsService.breadcrumbs.length).toBe(0, 'array is empty');
  });
  it('should push item', () => {
    const breadcrumb = new Breadcrumb('test-url', 'test breadcrumb');
    breadcrumbsService.push(breadcrumb);
    expect(breadcrumbsService.breadcrumbs.length).toBe(1, 'one breadcrumb in array');
    expect(breadcrumbsService.breadcrumbs[0]).toBe(breadcrumb, 'pushed breadcrumb');
  });
  it('should set breadcrumb after tick', fakeAsync(() => {
    const initialBreadcrumbs = breadcrumbsService.breadcrumbs;
    const newBreadcrumbs = [new Breadcrumb('test-url', 'test-breadcrumb')];
    breadcrumbsService.breadcrumbs = newBreadcrumbs;
    expect(breadcrumbsService.breadcrumbs).toBe(initialBreadcrumbs, 'same breadcrumbs before tick');
    tick();
    expect(breadcrumbsService.breadcrumbs).toBe(newBreadcrumbs, 'new breadcrumbs after tick');
  }));
});
