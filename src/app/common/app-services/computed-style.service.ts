import {ElementRef, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComputedStyleService {

  constructor() { }
  get(element: ElementRef, propName: string, pseudoElement: string = null) {
    return window.getComputedStyle(element.nativeElement, pseudoElement).getPropertyValue(propName);
  }
  height(element: ElementRef) {
    return element.nativeElement.offsetHeight;
  }
  offsetTop(child: HTMLElement, parent: ElementRef) {
    return this.offset('Top', child, parent.nativeElement);
  }
  offsetLeft(child: HTMLElement, parent: ElementRef) {
    return this.offset('Left', child, parent.nativeElement);
  }
  private offset(direction: 'Top' | 'Left', child: HTMLElement, parent: HTMLElement) {
    const propName = `offset${direction}`;
    function accumulateParentOffset(current: HTMLElement) {
      if (current.offsetParent && current.offsetParent !== parent) {
        return current[propName] + accumulateParentOffset((<HTMLElement>current.offsetParent));
      } else {
        return current[propName];
      }
    }

    if (child !== parent) {
      const parentOffsets = accumulateParentOffset(child);
      if (window.getComputedStyle(parent, null).getPropertyValue('position') !== 'static') {
        return parentOffsets;
      } else {
        return parentOffsets - parent[propName];
      }
    } else {
      return 0;
    }
  }
}

