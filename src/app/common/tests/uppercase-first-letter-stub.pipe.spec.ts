import { UppercaseFirstLetterStubPipe } from './uppercase-first-letter-stub.pipe';
import {Component} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

describe('UppercaseFirstLetterStubPipe', () => {
  it('create an instance', () => {
    const pipe = new UppercaseFirstLetterStubPipe();
    expect(pipe).toBeTruthy();
  });
  describe('template', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-pipe',
      template: `<span>{{text | uppercaseFirstLetter}}</span>`
    })
    class TestPipe {
      text: any;
    }
    let fixture: ComponentFixture<TestPipe>, component: TestPipe, span: HTMLSpanElement;
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [TestPipe, UppercaseFirstLetterStubPipe]
      }).compileComponents();
    }));
    it('no transform', () => {
      fixture = TestBed.createComponent(TestPipe);
      component = fixture.componentInstance;
      span = fixture.debugElement.query(By.css('span')).nativeElement;
      component.text = 'text';
      fixture.detectChanges();
      expect(span.textContent).toBe(component.text);
    });
  });

});
