import {Pipe, PipeTransform} from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Pipe({
  name: 'currency'
})
export class CurrencyStub implements PipeTransform {
  transform(value: any): any {
    return value;
  }
}
