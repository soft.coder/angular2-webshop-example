import { Pipe, PipeTransform } from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Pipe({
  name: 'uppercaseFirstLetter'
})
export class UppercaseFirstLetterStubPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value;
  }

}
