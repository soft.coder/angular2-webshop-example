import { BackUrlStubDirective } from './back-url-stub.directive';
import {Component, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DataService} from '../app-services/data.service';
import {By} from '@angular/platform-browser';
import SpyObj = jasmine.SpyObj;

describe('BackUrlStubDirective', () => {
  it('should create an instance', () => {
    const directive = new BackUrlStubDirective();
    expect(directive).toBeTruthy();
  });
  describe('template', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-directive',
      template: '<a [appBackUrl]="url"></a>'
    })
    class TestDirective {
      url: any;
    }
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [BackUrlStubDirective, TestDirective]
      }).compileComponents();
    }));
    let fixture: ComponentFixture<TestDirective>, component: TestDirective;
    let debugElement: DebugElement, directive: BackUrlStubDirective;
    beforeEach(() => {
      fixture = TestBed.createComponent(TestDirective);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement.query(By.directive(BackUrlStubDirective));
      directive = debugElement.injector.get(BackUrlStubDirective);
    });
    it('should binding', () => {
      fixture.detectChanges();
      expect(directive.appBackUrl).toBe(component.url, 'undefined');
      component.url = 'test-url';
      fixture.detectChanges();
      expect(directive.appBackUrl).toBe(component.url, 'defined');
    });
    it('should handler click', () => {
      component.url = 'test-url';
      fixture.detectChanges();
      debugElement.triggerEventHandler('click', undefined);
      expect(directive.savedBackUrl).toBe(component.url);
    });
  });
});
