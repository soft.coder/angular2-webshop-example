import { RouterLinkActiveStubDirective } from './router-link-active-stub.directive';
import {Component, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

describe('RouterLinkActiveStubDirective', () => {
  it('should create an instance', () => {
    const directive = new RouterLinkActiveStubDirective();
    expect(directive).toBeTruthy();
  });
  describe('template', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-directive',
      template: '<a [routerLinkActive]="isActive"></a>'
    })
    class TestDirective {
      isActive: any;
    }
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [RouterLinkActiveStubDirective, TestDirective]
      }).compileComponents();
    }));
    let fixture: ComponentFixture<TestDirective>, component: TestDirective;
    let debugElement: DebugElement, directive: RouterLinkActiveStubDirective;
    beforeEach(() => {
      fixture = TestBed.createComponent<TestDirective>(TestDirective);
      component = fixture.componentInstance;
      debugElement = fixture.debugElement.query(By.directive(RouterLinkActiveStubDirective));
      directive = debugElement.injector.get(RouterLinkActiveStubDirective);
    });
    it('test binding', () => {
      const values = [undefined, true, false];
      for (const value of values) {
        component.isActive = value;
        fixture.detectChanges();
        expect(directive.routerLinkActive).toBe(component.isActive, value + '');
      }
    });
  });
});
