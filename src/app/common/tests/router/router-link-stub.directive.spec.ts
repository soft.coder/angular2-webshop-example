import { RouterLinkStubDirective } from './router-link-stub.directive';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserService} from '../../app-services/user.service';
import {BreadcrumbsService} from '../../app-services/breadcrumbs.service';
import {Component, DebugElement, Input} from '@angular/core';
import {Breadcrumb} from '../../app-widgets/breadcrumbs/breadcrumb';
import {CommonModule} from '@angular/common';
import {AppComponent} from '../../../app.component';
import {RouterLinkActiveStubDirective} from './router-link-active-stub.directive';
import {By} from '@angular/platform-browser';

describe('RouterLinkStubDirective', () => {

  describe('class', () => {
    it('should create an instance', () => {
      const directive = new RouterLinkStubDirective();
      expect(directive).toBeTruthy();
    });
  });
  describe('template', () => {
    let component: TestZone, fixture: ComponentFixture<TestZone>;
    let linkDebugElement: DebugElement, linkDirective: RouterLinkStubDirective;
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      template: `<a [routerLink]="linkUrl">link</a>`,
      selector: 'app-test-zone'
    })
    class TestZone {
      linkUrl = 'url';
    }
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [CommonModule],
        declarations: [
          TestZone,
          RouterLinkStubDirective,
        ],
      }).compileComponents();
    }));
    beforeEach(() => {
      fixture = TestBed.createComponent(TestZone);
      component = fixture.componentInstance;
      linkDebugElement = fixture.debugElement.query(By.directive(RouterLinkStubDirective));
      linkDirective = linkDebugElement.injector.get(RouterLinkStubDirective);
      fixture.detectChanges();
    });
    it('should contain correct link', () => {
      expect(linkDirective.linkParam).toBe(component.linkUrl);
    });
    it('should handle click', () => {
      expect(linkDirective.navigatedTo).toBeUndefined('not clicked yet');
      linkDebugElement.triggerEventHandler('click', {});
      expect(linkDirective.navigatedTo).toBe(linkDirective.linkParam);
    });

  });
});
