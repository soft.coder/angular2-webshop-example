import {Directive, HostListener, Input} from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLink]'
})
export class RouterLinkStubDirective {
  @Input('routerLink')
  linkParam: any;
  navigatedTo: any;

  get strLinkParam() {
    return this.urlToString(this.linkParam);
  }
  get strNavigatedTo() {
    return this.urlToString(this.navigatedTo);
  }
  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParam;
  }

  constructor() {

  }
  private urlToString(url: Array<string | number> | string) {
    if (url instanceof Array) {
      return url.join('/');
    } else if (typeof url === 'string' || url === null || url === undefined ) {
      return url;
    } else {
      throw new TypeError('url expected type of array or string');
    }
  }

}
