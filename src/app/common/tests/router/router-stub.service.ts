import { Injectable } from '@angular/core';
import {NavigationExtras} from '@angular/router';

@Injectable()
export class RouterStubService {
  url: string;
  navigatedCommands: any[];
  extras: NavigationExtras;
  navigatedUrl: string;

  constructor() { }
  navigate(commands: any[], extras?: NavigationExtras): Promise<boolean> {
    this.navigatedCommands = commands;
    this.extras = extras;
    return Promise.resolve(true);
  }

  navigateByUrl(url: string, extras?: NavigationExtras): Promise<boolean> {
    this.navigatedUrl = url;
    this.extras = extras;
    return Promise.resolve(true);
  }


}
