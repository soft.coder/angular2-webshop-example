import { TestBed } from '@angular/core/testing';

import { ActivatedRouteStubService } from './activated-route-stub.service';

describe('ActivatedRouteStubService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ActivatedRouteStubService]
  }));

  it('should be created', () => {
    const service: ActivatedRouteStubService<never> = TestBed.get(ActivatedRouteStubService);
    expect(service).toBeTruthy();
  });
});
