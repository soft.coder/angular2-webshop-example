import {Directive, Input} from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLinkActive]'
})
export class RouterLinkActiveStubDirective {
  @Input()
  routerLinkActive: string;

  constructor() { }

}
