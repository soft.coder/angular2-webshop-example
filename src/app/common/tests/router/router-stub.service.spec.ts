import {TestBed} from '@angular/core/testing';

import { RouterStubService } from './router-stub.service';
import {NavigationExtras} from '@angular/router';

describe('RouterStubService', () => {
  let service: RouterStubService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterStubService]
    });
    service = TestBed.get(RouterStubService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should navigate', (done) => {
    const commands = ['test-url', 1];
    const extras: NavigationExtras = {skipLocationChange: false};
    service.navigate(commands, extras).then(result => {
      expect(result).toBe(true, 'resolved as true');
      done();
    });
    expect(service.navigatedCommands).toBe(commands, 'set commands');
    expect(service.extras).toBe(extras, 'set extras');
  });
  it('should navigate by url', (done) => {
    const url = 'test-url';
    const extras: NavigationExtras = {skipLocationChange: false};
    service.navigateByUrl(url, extras).then(result => {
      expect(result).toBe(true, 'resolved as true');
      done();
    });
    expect(service.navigatedUrl).toBe(url, 'set url');
    expect(service.extras).toBe(extras, 'set extras');
  });
});
