import { Injectable } from '@angular/core';
import {EMPTY, Observable} from 'rxjs';

@Injectable()
export class ActivatedRouteStubService<T> {
  data: Observable<T> = EMPTY;
  constructor() { }
}
