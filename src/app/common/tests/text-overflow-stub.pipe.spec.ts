import { TextOverflowStubPipe } from './text-overflow-stub.pipe';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('TextOverflowStubPipe', () => {
  it('create an instance', () => {
    const pipe = new TextOverflowStubPipe();
    expect(pipe).toBeTruthy();
  });
  describe('template', () => {
    // noinspection AngularMissingOrInvalidDeclarationInModule
    @Component({
      selector: 'app-test-pipe',
      template: '<span>{{text | textOverflow}}</span>'
    })
    class TestPipe {
      text: any;
    }
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [TextOverflowStubPipe, TestPipe]
      }).compileComponents();
    }));
    it('no transform', () => {
      const fixture = TestBed.createComponent<TestPipe>(TestPipe);
      const component = fixture.componentInstance;
      const span: HTMLSpanElement = fixture.debugElement.query(By.css('span')).nativeElement;
      component.text = 'some text';
      fixture.detectChanges();
      expect(span.textContent).toBe(component.text);
    });
  });
});
