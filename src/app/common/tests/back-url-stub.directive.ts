import {Directive, HostListener, Input} from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Directive({
  selector: '[appBackUrl]'
})
export class BackUrlStubDirective {
  @Input()
  appBackUrl: string;
  savedBackUrl: string;
  @HostListener('click')
  onClick() {
    this.savedBackUrl = this.appBackUrl;
  }

  constructor() { }

}
