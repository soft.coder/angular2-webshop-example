import { Injectable } from '@angular/core';

@Injectable()
export class DialogStubService {
  alertMsg: string;
  confirmMsg: string;
  isConfirmed: boolean;
  constructor() { }
  alert(message: string) {
    this.alertMsg = message;
  }
  confirm(message: string): boolean {
    this.confirmMsg = message;
    return this.isConfirmed;
  }
}
