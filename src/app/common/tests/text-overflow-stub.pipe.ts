import { Pipe, PipeTransform } from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Pipe({
  name: 'textOverflow'
})
export class TextOverflowStubPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value;
  }

}
