import { TestBed } from '@angular/core/testing';

import { DialogStubService } from './dialog-stub.service';

describe('DialogStubService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [DialogStubService]
  }));

  it('should be created', () => {
    const service: DialogStubService = TestBed.get(DialogStubService);
    expect(service).toBeTruthy();
  });
});
