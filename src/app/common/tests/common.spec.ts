import {getFunctionName} from '../functions';
import {defer} from 'rxjs';
import {AbstractControl} from '@angular/forms';
import {DebugElement, Type} from '@angular/core';
import {By} from '@angular/platform-browser';

export function getTypeName(typeItem) {
  let typeInfo: string = typeof typeItem;
  if (typeInfo === 'object') {
    switch (typeItem) {
      case(typeItem instanceof Array):
        typeInfo = 'array';
        break;
      case(typeItem === null):
        typeInfo = 'null';
        break;
    }
  }
  return typeInfo;
}
export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}
export function asyncError<T>(errorObject: any) {
  return defer(() => Promise.reject(errorObject));
}

export function errorExpectations(msg: string, errorConstructor: Function, tryFn: Function, arrArgs: Array<Array<any>>) {
  const errorTypeName = getFunctionName(errorConstructor);
  for (const args of arrArgs) {
    try {
      tryFn(...args);
    } catch (error) {
      expect(getFunctionName(error.constructor)).toBe(errorTypeName);
      expect(error.message).toBe(msg);
      continue;
    }
    fail(`expected ${errorTypeName} args: ${JSON.stringify(args)}`);
  }

}
export function newEvent(eventName: string, bubbles = false, cancelable = false) {
  const evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
}
export function generateText(length) {
  if (length) {
    const tempArray = new Array(length);
    for (let i = 0; i < tempArray.length; i++) {
      tempArray[i] = `${i}`;
    }
    return tempArray.join('');
  } else {
    return '';
  }
}
export function inputText(element: HTMLInputElement | HTMLTextAreaElement, text: string) {
  element.value = text;
  element.dispatchEvent(newEvent('input'));
}

export class FieldData<T> {
  constructor(readonly domElement: T,
              readonly formControl?: AbstractControl,
              readonly debugElement?: DebugElement,
              readonly invalidFeedback?: HTMLElement) { }
}
export class FieldDataLazy<T extends HTMLElement> {
  get domElement() {
    return this.domElementFactory();
  }
  get formControl() {
    if (this.formControlFactory) {
      return this.formControlFactory();
    }
  }
  get debugElement() {
    if (this.debugElementFactory) {
      return this.debugElementFactory();
    }
  }
  get invalidFeedback() {
    if (this.invalidFeedbackFactory) {
      return this.invalidFeedbackFactory();
    }
  }
  constructor(private domElementFactory: () => T,
              private formControlFactory?: () => AbstractControl,
              private debugElementFactory?: () => DebugElement,
              private invalidFeedbackFactory?: () => HTMLElement) {
  }
}
export interface Selectors {
  [key: string]: string;
}
export interface PageSelectors {
  root: string;
  // @ts-ignore
  inputs?: Selectors;
  [key: string]: string | Selectors;
}

export class TestPage {
  inputFields: {[id: string]: FieldDataLazy<HTMLInputElement | HTMLTextAreaElement>} = {};
  constructor(protected debugElement: DebugElement) { }
  protected initInputFields(inputSelectors: Selectors, component) {
    Object.keys(inputSelectors).forEach(key => {
      this.inputFields[key] = new FieldDataLazy(
        () => this.queryByCss(inputSelectors[key]).nativeElement,
        () => component[key],
        () => this.queryByCss(inputSelectors[key]),
        () => this.queryByCss(`${inputSelectors[key]} ~ .invalid-feedback`).nativeElement
      );
    });
  }
  protected queryByDirective(directive: Type<any>) {
    return this.debugElement.query(By.directive(directive));
  }
  protected queryAllByDirective(directive: Type<any>) {
    return this.debugElement.queryAll(By.directive(directive));
  }
  protected queryByCss(selector: string) {
    return this.debugElement.query(By.css(selector));
  }
  protected queryAllByCss(selector: string) {
    return this.debugElement.queryAll(By.css(selector));
  }
  protected click(debugElement: DebugElement, eventObj: Event = null) {
    return debugElement.triggerEventHandler('click', eventObj);
  }

  public shouldShowError(fieldKey: string, errorKey: string, errorMsg: string, formGroupErrors?: {[errorKey: string]: any}) {
    const {domElement, formControl, invalidFeedback} = this.inputFields[fieldKey];
    const errors = formGroupErrors || formControl.errors;
    expect(errors && errors[errorKey]).toBeTruthy(`field "${fieldKey}" have "${errorKey}" error`);
    expect(domElement.classList.contains('is-invalid')).toBeTruthy(`field "${fieldKey}" show "${errorKey}" error`);
    expect(invalidFeedback.classList.contains('show')).toBeTruthy(`.invalid-feedback of field "${fieldKey}" has class show`);
    expect(invalidFeedback.textContent).toMatch(new RegExp(
      errorMsg, 'i'), `.invalid-feedback of field "${fieldKey}" have "${errorMsg}" msg`);
  }
  public shouldNotShowError(fieldKey: string, errorKey: string, errorMsg: string, formGroupErrors?: {[key: string]: any}) {
    const {formControl, invalidFeedback} = this.inputFields[fieldKey];
    const errors = formGroupErrors || formControl.errors;
    expect(errors && errors[errorKey]).toBeFalsy(`formControl dont have "${errorKey}" error`);
    expect(invalidFeedback.textContent).not
      .toMatch(new RegExp(errorMsg, 'i'), `.invalid-feedback of field "${fieldKey}" should have "${errorMsg}" msg`);
  }
}

