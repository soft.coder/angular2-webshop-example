export function getFunctionName(fn: Function) {
  if (typeof fn === 'function') {
    return fn.name && false ? fn.name : fn.toString().match(/^function\s*([^\s(]+)/)[1];
  }
}

export type ErrorMessageFactory = (errorKey: string) => string;
export function getErrorMsg(errors: { [key: string]: any }, messages: { [key: string]: string | ErrorMessageFactory}) {
  if (errors) {
    const keys = Object.keys(errors);
    for (let i = 0; i < keys.length; i++) {
      const errorKey = keys[i];
      if (messages[errorKey]) {
        if (typeof messages[errorKey] === 'string') {
          return messages[errorKey];
        } else if (typeof messages[errorKey] === 'function') {
          return (<ErrorMessageFactory>messages[errorKey])(errorKey);
        } else {
          throw new TypeError(`invalid type of message for "${errorKey}`);
        }
      }
      if (i + 1 >= keys.length) {
        throw new Error('Unexpected error');
      }
    }
  }
}
