import { TestBed, inject } from '@angular/core/testing';

import { ApiUrlService } from './api-url.service';

describe('ApiUrlService', () => {
  let apiUrlService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiUrlService]
    });
    apiUrlService = TestBed.get(ApiUrlService);
  });

  it('should be created', inject([ApiUrlService], (service: ApiUrlService) => {
    expect(service).toBeTruthy();
  }));
  describe('should contain url', () => {
    it('categories', () => {
      expect(apiUrlService.categories).toBeTruthy();
    });
    it('products', () => {
      expect(apiUrlService.products).toBeTruthy();
    });
    it('category-product', () => {
      expect(apiUrlService.categoryProduct).toBeTruthy();
    });
  });
});
