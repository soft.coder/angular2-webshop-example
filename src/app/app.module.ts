import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryExampleService} from './common/app-services/in-memory-example.service';
import {HttpClientModule} from '@angular/common/http';
import {ProfileModule} from './profile/profile.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { AppRoutingModule } from './app-routing.module';
import {AppServicesModule} from './common/app-services/app-services.module';
import {CategoriesModule} from './categories/categories.module';
import { ProductModule } from './product/product.module';
import {AppWidgetsModule} from './common/app-widgets/app-widgets.module';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryExampleService, {delay: 90}),
    AppServicesModule,
    AppWidgetsModule,
    ProfileModule,
    CategoriesModule,
    ProductModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
